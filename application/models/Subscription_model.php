<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Subscription_model extends CI_Model {

    public function __construct()
    {
        $this->load->database();
    }

    public function filter_record_count($id = FALSE, $user_id = FALSE, $plan_id = FALSE, $status = FALSE)
    {
        $this->db->reset_query();

        $sql = "SELECT COUNT(id) AS count FROM subscriptions WHERE ";

        $where = '';

        if($id !== FALSE)
        {

            $id = (int) $id;
            if($id != 0)
            {
                $where .= " AND (id = '". $id . "')";
            }
        }

        if($user_id !== FALSE)
        {

            $user_id = (int) $user_id;
            $where .= " AND (user_id = '". $user_id . "')";
        }

        if($plan_id !== FALSE)
        {

            $plan_id = (int) $plan_id;
            $where .= " AND (plan_id = '". $plan_id . "')";
        }

        if($status !== FALSE)
        {
            $date = date('Y-m-d H:i:s');

            if($status == 'expired')
            {
                $where .= " AND (DATE('$date') > DATE(end_time) )";
            }
            elseif($status == 'active')
            {
                $where .= " AND (DATE('$date') >= DATE(start_time) AND DATE('$date') <= DATE(end_time) )";
            }
            else
            {}
        }
        
        $where = ltrim($where, ' AND');
        $sql = $sql . $where;

        // echo $sql; die;

        $query = $this->db->query($sql);
        return $query->row_array()['count'];
    }

    public function filter($limit, $offset, $id = FALSE, $user_id = FALSE, $plan_id = FALSE, $status = FALSE)
    {
        $limit = (int) $limit;
        $offset = (int) $offset;

        $sql = "SELECT subscriptions.id, subscriptions.user_id, subscriptions.plan_id, subscriptions.payment_id, subscriptions.start_time, subscriptions.end_time, users.full_name, payments.transaction_ref, payments.paid, payments.currency, payments.gw FROM subscriptions LEFT OUTER JOIN users ON subscriptions.user_id = users.id LEFT OUTER JOIN subscriptions.payment_id = payments.id WHERE ";

        $where = '';

        if($id !== FALSE)
        {

            $id = (int) $id;
            if($id != 0)
            {
                $where .= " AND (subscriptions.id = '". $id . "')";
            }
        }

        if($user_id !== FALSE)
        {

            $user_id = (int) $user_id;
            $where .= " AND (subscriptions.user_id = '". $user_id . "')";
        }

        if($plan_id !== FALSE)
        {

            $plan_id = (int) $plan_id;
            $where .= " AND (subscriptions.plan_id = '". $plan_id . "')";
        }

        if($status !== FALSE)
        {
            $date = date('Y-m-d H:i:s');

            if($status == 'expired')
            {
                $where .= " AND (DATE('$date') > DATE(subscriptions.end_time) )";
            }
            elseif($status == 'active')
            {
                $where .= " AND (DATE('$date') >= DATE(subscriptions.start_time) AND DATE('$date') <= DATE(subscriptions.end_time) )";
            }
            else
            {}
        }
        
        $where = ltrim($where, ' AND');
        $sql = $sql . $where . " ORDER BY subscriptions.start_time DESC LIMIT $offset, $limit";

        // echo $sql; die;

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function record_count_today()
    {
        $sql = "SELECT COUNT(id) AS count FROM subscriptions WHERE DATE(start_time) = CURDATE()";
        $query = $this->db->query($sql);
        $row = $query->row_array();
        return $row['count'];
    }

    public function record_count_this_week()
    {
        $sql = "SELECT COUNT(id) AS count FROM subscriptions WHERE YEARWEEK(start_time) = YEARWEEK(CURRENT_TIMESTAMP)";
        $query = $this->db->query($sql);
        $row = $query->row_array();
        return $row['count'];
    }

    public function record_count() {
        return $this->db->count_all("subscriptions");
    }

    public function getRows($limit, $offset, $id = FALSE)
    {
        $limit = (int) $limit;
        $offset = (int) $offset;

        if($id === FALSE)
        {
            $this->db->order_by('date_created', 'DESC');
            $this->db->select('subscriptions.id, subscriptions.user_id, subscriptions.plan_id, subscriptions.payment_id, subscriptions.start_time, subscriptions.end_time, users.full_name, payments.transaction_ref, payments.paid, payments.currency, payments.gw');
            $this->db->from('subscriptions'); 
            $this->db->join('users', 'subscriptions.user_id = users.id', 'left');
            $this->db->join('payments', 'subscriptions.payment_id = payments.id', 'left');
            $this->db->limit($limit, $offset); // produces LIMIT $offset, $limit

            $query = $this->db->get();
            return $query->result_array();
        }

        $this->db->select('subscriptions.id, subscriptions.user_id, subscriptions.plan_id, subscriptions.payment_id, subscriptions.start_time, subscriptions.end_time, users.full_name, payments.transaction_ref, payments.paid, payments.currency, payments.gw');
        $this->db->from('subscriptions');
        $this->db->join('users', 'subscriptions.user_id = users.id', 'left');
        $this->db->join('payments', 'subscriptions.payment_id = payments.id', 'left');
        $this->db->where('id', $id); 
        $query = $this->db->limit(1);

        $query = $this->db->get();
        return $query->row_array();
    }

    public function add($user_id, $plan_id, $payment_id, $date = false)
    {
        $plans = plans();
        $plan_arr_key = getArrayKey($plans, "id", $plan_id);

        if($plan_arr_key === false)
        {
            return false;
        }

        $plan = $plans[$plan_arr_key];

        $start_time = date('Y-m-d H:i:s');
        if($date !== false)
        {
            $start_time = date('Y-m-d H:i:s', strtotime($date));
        }
        $end_time = date('Y-m-d H:i:s', strtotime('+' . $plan['duration'] . ' months', strtotime($start_time)));

        $data = array(
            'user_id' => trim($user_id),
            'plan_id' => trim($plan_id),
            'payment_id' => trim($payment_id),
            'start_time' => $start_time,
            'end_time' => $end_time
        );

        $this->db->insert('subscriptions', $data);

        return $this->db->insert_id();
    }

    public function getRowByRef($reference)
    {
        $this->db->order_by('date_created', 'DESC');
        $this->db->select('subscriptions.id, subscriptions.user_id, subscriptions.plan_id, subscriptions.payment_id, subscriptions.start_time, subscriptions.end_time, users.full_name, payments.transaction_ref, payments.paid, payments.currency, payments.gw');
        $this->db->from('subscriptions');
        $this->db->join('users', 'subscriptions.user_id = users.id', 'left');
        $this->db->join('payments', 'subscriptions.payment_id = payments.id', 'left');
        $this->db->where('payments.transaction_ref', $reference); 
        $query = $this->db->limit(1);
        $query = $this->db->get();

        return $query->row_array();
    }

    public function isActive($user_id)
    {
        $date = date('Y-m-d H:i:s');
        $this->db->select('subscriptions.id, subscriptions.user_id, subscriptions.plan_id, subscriptions.payment_id, subscriptions.start_time, subscriptions.end_time, users.full_name, payments.transaction_ref, payments.paid, payments.currency, payments.gw');
        $this->db->from('subscriptions');
        $this->db->join('users', 'subscriptions.user_id = users.id', 'left');
        $this->db->join('payments', 'subscriptions.payment_id = payments.id', 'left');
        $this->db->where('subscriptions.user_id', (int) $user_id); 
        $this->db->where("(DATE('$date') >= DATE(subscriptions.start_time) AND DATE('$date') <= DATE(subscriptions.end_time) )");
        $query = $this->db->limit(1);
        $query = $this->db->get();

        $result = $query->row_array();

        return $result ?: false;
    }
}