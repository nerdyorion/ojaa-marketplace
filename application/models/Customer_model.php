<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Customer_model extends CI_Model {

    public function __construct()
    {
        $this->load->database();
        $this->db->reset_query();
    }

    public function filter_record_count($id = FALSE, $title = FALSE)
    {
        $this->db->reset_query();

        $sql = "SELECT COUNT(customers.id) AS count FROM customers WHERE ";

        $where = '';

        if($id !== FALSE)
        {

            $id = (int) $id;
            if($id != 0)
            {
                $where .= " AND (customers.id = '". $id . "')";
            }
        }

        if($title !== FALSE)
        {
            $title_full = '%' . filter_var($title, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH) . '%';
            $where .= " AND ("; 

            $where .= "(customers.name LIKE '". $title_full . "') OR (customers.email LIKE '". $title_full . "')";

            $title_array = explode(' ', $title);
            if(count($title_array) > 1)
            {
                foreach ($title_array as $title_word) {
                    if(!empty($title_word))
                    {
                        $title_word = '%' . filter_var($title_word, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH) . '%';
                        $where .= " OR (customers.name LIKE '". $title_word . "') OR (customers.email LIKE '". $title_word . "')";
                    }
                }
            }

            $where .= ")";
        }
        
        $where = ltrim($where, ' AND');
        $sql = $sql . $where;

        // echo $sql; die;

        $query = $this->db->query($sql);
        return $query->row_array()['count'];
    }

    public function filter($limit, $offset, $id = FALSE, $title = FALSE)
    {
        $limit = (int) $limit;
        $offset = (int) $offset;

        $sql = "SELECT customers.id, customers.name, customers.email, customers.phone, customers.country, customers.address, customers.delivery, customers.date_created FROM customers WHERE ";

        $where = '';

        if($id !== FALSE)
        {

            $id = (int) $id;
            if($id != 0)
            {
                $where .= " AND (customers.id = '". $id . "')";
            }
        }

        if($title !== FALSE)
        {
            $title_full = '%' . filter_var($title, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH) . '%';
            $where .= " AND ("; 

            $where .= "(customers.name LIKE '". $title_full . "') OR (customers.email LIKE '". $title_full . "')";

            $title_array = explode(' ', $title);
            if(count($title_array) > 1)
            {
                foreach ($title_array as $title_word) {
                    if(!empty($title_word))
                    {
                        $title_word = '%' . filter_var($title_word, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH) . '%';
                        $where .= " OR (customers.name LIKE '". $title_word . "') OR (customers.email LIKE '". $title_word . "')";
                    }
                }
            }

            $where .= ")";
        }
        
        $where = ltrim($where, ' AND');
        $sql = $sql . $where . " ORDER BY customers.name ASC ASC LIMIT $offset, $limit";

        // echo $sql; die;

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function report_record_count($country, $name, $start, $end)
    {
        $this->db->reset_query();

        $sql = "SELECT COUNT(customers.id) AS count FROM customers WHERE ";

        $where = '';


        if($country !== FALSE)
        {

            $country = (int) $country;
            if($country != 0)
            {
                $where .= " AND (customers.country = '". $country . "')";
            }
        }

        if($name !== FALSE)
        {
            $title_full = '%' . filter_var($name, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH) . '%';
            $where .= " AND ("; 

            $where .= "(customers.name LIKE '". $title_full . "')";

            $title_array = explode(' ', $name);
            if(count($title_array) > 1)
            {
                foreach ($title_array as $title_word) {
                    if(!empty($title_word))
                    {
                        $title_word = '%' . filter_var($title_word, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH) . '%';
                        $where .= " OR (customers.name LIKE '". $title_word . "')";
                    }
                }
            }

            $where .= ")";
        }

        if($start !== FALSE && $end !== FALSE) // date_created
        {
            if(!empty($start) && !empty($end))
            {
                $start = filter_var($start, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);  // dd($start);
                $end = filter_var($end, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH); // dd($end);

                $start = date('Y-m-d', strtotime($start)); // convert to date
                $end = date('Y-m-d', strtotime($end)); // convert to date

                if($start == $end)
                {
                    $where .= " AND (DATE(customers.date_created) = '". $start . "')";
                }
                else
                {
                    $where .= " AND ((DATE(customers.date_created) >= '". $start . "') AND (DATE(customers.date_created) <= '". $end . "'))";
                }
            }
        }
        
        $where = ltrim($where, ' AND');
        $sql = $sql . $where;

        // echo $sql; die;

        $query = $this->db->query($sql);
        return $query->row_array()['count'];
    }

    public function report($limit, $offset, $country, $name, $start, $end)
    {
        $limit = (int) $limit;
        $offset = (int) $offset;

        $sql = "SELECT customers.id, customers.name, customers.email, customers.phone, customers.country, customers.address, customers.delivery, customers.date_created WHERE ";

        $where = '';

        if($country !== FALSE)
        {

            $country = (int) $country;
            if($country != 0)
            {
                $where .= " AND (customers.country = '". $country . "')";
            }
        }

        if($name !== FALSE)
        {
            $title_full = '%' . filter_var($name, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH) . '%';
            $where .= " AND ("; 

            $where .= "(customers.name LIKE '". $title_full . "')";

            $title_array = explode(' ', $name);
            if(count($title_array) > 1)
            {
                foreach ($title_array as $title_word) {
                    if(!empty($title_word))
                    {
                        $title_word = '%' . filter_var($title_word, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH) . '%';
                        $where .= " OR (customers.name LIKE '". $title_word . "')";
                    }
                }
            }

            $where .= ")";
        }

        if($start !== FALSE && $end !== FALSE) // date_created
        {
            if(!empty($start) && !empty($end))
            {
                $start = filter_var($start, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);  // dd($start);
                $end = filter_var($end, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH); // dd($end);

                $start = date('Y-m-d', strtotime($start)); // convert to date
                $end = date('Y-m-d', strtotime($end)); // convert to date

                if($start == $end)
                {
                    $where .= " AND (DATE(customers.date_created) = '". $start . "')";
                }
                else
                {
                    $where .= " AND ((DATE(customers.date_created) >= '". $start . "') AND (DATE(customers.date_created) <= '". $end . "'))";
                }
            }
        }
        
        $where = ltrim($where, ' AND');
        $sql = $sql . $where . " ORDER BY customers.name ASC ASC LIMIT $offset, $limit";

        // echo $sql; die;

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function record_count()
    {
        return $this->db->count_all("customers");
    }

    public function record_count_today()
    {
        $sql = "SELECT COUNT(id) AS count FROM customers WHERE DATE(date_created) = CURDATE()";
        $query = $this->db->query($sql);
        $row = $query->row_array();
        return $row['count'];
    }

    public function record_count_this_week()
    {
        $sql = "SELECT COUNT(id) AS count FROM customers WHERE YEARWEEK(date_created) = YEARWEEK(CURRENT_TIMESTAMP)";
        $query = $this->db->query($sql);
        $row = $query->row_array();
        return $row['count'];
    }

    public function getRows($limit, $offset, $id = FALSE)
    {
        $limit = (int) $limit;
        $offset = (int) $offset;

        if ($id === FALSE)
        {
            $query = $this->db->order_by('customers.name', 'ASC');
            $this->db->select('customers.id, customers.name, customers.email, customers.phone, customers.country, customers.address, customers.delivery, customers.date_created');
            $this->db->from('customers');
            $this->db->limit($limit, $offset); // produces LIMIT $offset, $limit

            $query = $this->db->get(); //echo $this->db->last_query(); die;
            return $query->result_array();
        }
        $this->db->select('customers.id, customers.name, customers.email, customers.phone, customers.country, customers.address, customers.delivery, customers.date_created');
        $this->db->from('customers');
        $this->db->where('customers.id', $id); 
        $query = $this->db->limit(1);
        $query = $this->db->get();    //echo $this->db->last_query(); die;
        return $query->row_array();
    }

    public function getRowByEmail($email)
    {
        $email = filter_var(strtolower(trim($email)), FILTER_SANITIZE_EMAIL);

        $this->db->select('customers.id, customers.name, customers.email, customers.phone, customers.country, customers.address, customers.delivery, customers.date_created');
        $this->db->from('customers'); 
        $this->db->where('customers.email', $email); 
        $query = $this->db->limit(1);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function emailExists($email)
    {
        $email = filter_var(strtolower(trim($email)), FILTER_SANITIZE_EMAIL);

        $this->db->select('id');
        $this->db->from('customers');
        $this->db->where('email', $email); 
        $query = $this->db->limit(1);
        $query = $this->db->get();
        $row = $query->row_array();
        if(empty($row))
        {
            // email does not exist
            return false;
        }
        else
        {
            // email exist
            return true;
        }
    }

    public function add()
    {
        $date = date("Y-m-d H:i:s");

        $data = array(
            'name' => ucfirst(trim($this->input->post('name'))),
            'email' => strtolower(trim($this->input->post('email'))),
            'phone' => trim($this->input->post('phone')),
            'country' => trim($this->input->post('country')),
            'address' => trim($this->input->post('address')),
            'delivery' => trim($this->input->post('delivery')),
            'date_created' => $date
        );

        $this->db->insert('customers', $data);
    }

    public function delete($id)
    {
        $this->db->delete('customers', array('id' => (int) $id));
    }

    public function update($id)
    {

        $data = array(
            'name' => ucfirst(trim($this->input->post('name'))),
            'phone' => trim($this->input->post('phone')),
            'country' => trim($this->input->post('country')),
            'address' => trim($this->input->post('address')),
            'delivery' => trim($this->input->post('delivery'))
        );
        $this->db->where('id', (int) $id);
        $this->db->update('customers', $data);
    }
}