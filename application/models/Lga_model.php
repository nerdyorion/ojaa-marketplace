<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Lga_model extends CI_Model {

    public function __construct()
    {
        $this->load->database();
    }

    public function filter_record_count($id = FALSE, $title = FALSE)
    {
        $this->db->reset_query();

        $sql = "SELECT COUNT(id) AS count FROM lgas WHERE ";

        $where = '';

        if($id !== FALSE)
        {

            $id = (int) $id;
            if($id != 0)
            {
                $where .= " AND (id = '". $id . "')";
            }
        }

        if($title !== FALSE)
        {
            $title_full = '%' . filter_var($title, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH) . '%';
            $where .= " AND ("; 

            // $where .= "(name LIKE '". $title_full . "') OR (code LIKE '". $title_full . "')";
            $where .= "(name LIKE '". $title_full . "')";

            $title_array = explode(' ', $title);
            if(count($title_array) > 1)
            {
                foreach ($title_array as $title_word) {
                    if(!empty($title_word))
                    {
                        $title_word = '%' . filter_var($title_word, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH) . '%';
                        // $where .= " OR (name LIKE '". $title_word . "') OR (code LIKE '". $title_word . "')";
                        $where .= " OR (name LIKE '". $title_word . "')";
                    }
                }
            }

            $where .= ")";
        }
        
        $where = ltrim($where, ' AND');
        $sql = $sql . $where;

        // echo $sql; die;

        $query = $this->db->query($sql);
        return $query->row_array()['count'];
    }

    public function filter($limit, $offset, $id = FALSE, $title = FALSE)
    {
        $limit = (int) $limit;
        $offset = (int) $offset;

        $sql = "SELECT lgas.id, lgas.name, states.name AS state_name, countries.name AS country_name FROM lgas LEFT OUTER JOIN states ON lgas.state_id = states.id LEFT OUTER JOIN countries ON states.country_id = countries.id WHERE ";

        $where = '';

        if($id !== FALSE)
        {

            $id = (int) $id;
            if($id != 0)
            {
                $where .= " AND (lgas.id = '". $id . "')";
            }
        }

        if($title !== FALSE)
        {
            $title_full = '%' . filter_var($title, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH) . '%';
            $where .= " AND ("; 

            // $where .= "(name LIKE '". $title_full . "') OR (code LIKE '". $title_full . "')";
            $where .= "(lgas.name LIKE '". $title_full . "')";

            $title_array = explode(' ', $title);
            if(count($title_array) > 1)
            {
                foreach ($title_array as $title_word) {
                    if(!empty($title_word))
                    {
                        $title_word = '%' . filter_var($title_word, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH) . '%';
                        // $where .= " OR (name LIKE '". $title_word . "') OR (code LIKE '". $title_word . "')";
                        $where .= "(lgas.name LIKE '". $title_full . "')";
                    }
                }
            }

            $where .= ")";
        }
        
        $where = ltrim($where, ' AND');
        $sql = $sql . $where . " ORDER BY lgas.name ASC LIMIT $offset, $limit";

        // echo $sql; die;

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function record_count() {
        return $this->db->count_all("lgas");
    }

    public function getRows($limit, $offset, $id = FALSE)
    {
        $limit = (int) $limit;
        $offset = (int) $offset;

        if($id === FALSE)
        {
            $this->db->order_by('lgas.name', 'ASC');
            $this->db->select('lgas.id, lgas.name, states.name AS state_name, countries.name AS country_name');
            $this->db->from('lgas'); 
            $this->db->join('states', 'lgas.state_id = states.id', 'left');
            $this->db->join('countries', 'states.country_id = countries.id', 'left');
            $this->db->limit($limit, $offset); // produces LIMIT $offset, $limit

            $query = $this->db->get();
            return $query->result_array();
        }

        $this->db->select('lgas.id, lgas.name, states.name AS state_name, countries.name AS country_name');
        $this->db->from('lgas');
        $this->db->join('states', 'lgas.state_id = states.id', 'left');
        $this->db->join('countries', 'states.country_id = countries.id', 'left');
        $this->db->where('lgas.id', $id); 
        $query = $this->db->limit(1);

        $query = $this->db->get();
        return $query->row_array();
    }

    public function getRowsDropDown($id = FALSE)
    {
        if ($id === FALSE)
        {
            $this->db->order_by('lgas.name', 'ASC');
            $this->db->select('lgas.id, lgas.name, states.name AS state_name, countries.name AS country_name');
            $this->db->from('lgas'); 
            $this->db->join('states', 'lgas.state_id = states.id', 'left');
            $this->db->join('countries', 'states.country_id = countries.id', 'left');
            $query = $this->db->get();
            
            return $query->result_array();
        }
        $this->db->select('lgas.name, states.name AS state_name, countries.name AS country_name');
        $this->db->from('lgas');
        $this->db->join('states', 'lgas.state_id = states.id', 'left');
        $this->db->join('countries', 'states.country_id = countries.id', 'left');
        $this->db->where('lgas.id', $id); 
        $query = $this->db->limit(1);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function getRowsByStateIDDropDown($state_id)
    {
        $this->db->order_by('name', 'ASC');
        $this->db->select('id, name');
        $this->db->from('lgas');
        $this->db->where('state_id', $state_id);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function add()
    {
        $created_by = (int) $this->session->userdata('user_id');

        $data = array(
            'country_id' => (int) trim($this->input->post('country_id')),
            'state_id' => (int) trim($this->input->post('state_id')),
            'name' => trim($this->input->post('name')),
            'created_by' => $created_by
        );

        $this->db->insert('lgas', $data);
    }

    public function delete($id)
    {
        $this->db->delete('lgas', array('id' => (int) $id));
    }

    public function update($id)
    {

        $data = array(
            'country_id' => (int) trim($this->input->post('country_id')),
            'state_id' => (int) trim($this->input->post('state_id')),
            'name' => trim($this->input->post('name'))
        );
        $this->db->where('id', (int) $id);
        $this->db->update('lgas', $data);
    }
}