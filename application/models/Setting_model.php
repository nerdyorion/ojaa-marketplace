<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Setting_model extends CI_Model {

    public function __construct()
    {
        $this->load->database();
        $this->db->reset_query();
    }

    public function getRow($id = 1)
    {
        $this->db->select('id, interest_rate, updated_by, date_updated');
        $this->db->from('settings');
        $this->db->where('id', $id); 
        $query = $this->db->limit(1);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function getInterestRate($id = 1)
    {
        $this->db->select('interest_rate');
        $this->db->from('settings');
        $this->db->where('id', $id); 
        $query = $this->db->limit(1);
        $query = $this->db->get();
        return $query->row_array()['interest_rate'];
    }

    public function update($id = 1)
    {
        $created_by = (int) $this->session->userdata('user_id');

        $data = array(
            'interest_rate' => (float) trim($this->input->post('interest_rate')),
            'updated_by' => $created_by,
            'date_updated' => date("Y-m-d H:i:s")
        );
        $this->db->where('id', (int) $id);
        $this->db->update('settings', $data);
    }
}