<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Country_model extends CI_Model {

    public function __construct()
    {
        $this->load->database();
    }

    public function filter_record_count($id = FALSE, $title = FALSE)
    {
        $this->db->reset_query();

        $sql = "SELECT COUNT(id) AS count FROM countries WHERE ";

        $where = '';

        if($id !== FALSE)
        {

            $id = (int) $id;
            if($id != 0)
            {
                $where .= " AND (id = '". $id . "')";
            }
        }

        if($title !== FALSE)
        {
            $title_full = '%' . filter_var($title, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH) . '%';
            $where .= " AND ("; 

            // $where .= "(name LIKE '". $title_full . "') OR (code LIKE '". $title_full . "')";
            $where .= "(name LIKE '". $title_full . "')";

            $title_array = explode(' ', $title);
            if(count($title_array) > 1)
            {
                foreach ($title_array as $title_word) {
                    if(!empty($title_word))
                    {
                        $title_word = '%' . filter_var($title_word, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH) . '%';
                        // $where .= " OR (name LIKE '". $title_word . "') OR (code LIKE '". $title_word . "')";
                        $where .= " OR (name LIKE '". $title_word . "')";
                    }
                }
            }

            $where .= ")";
        }
        
        $where = ltrim($where, ' AND');
        $sql = $sql . $where;

        // echo $sql; die;

        $query = $this->db->query($sql);
        return $query->row_array()['count'];
    }

    public function filter($limit, $offset, $id = FALSE, $title = FALSE)
    {
        $limit = (int) $limit;
        $offset = (int) $offset;

        $sql = "SELECT id, name, code FROM countries WHERE ";

        $where = '';

        if($id !== FALSE)
        {

            $id = (int) $id;
            if($id != 0)
            {
                $where .= " AND (id = '". $id . "')";
            }
        }

        if($title !== FALSE)
        {
            $title_full = '%' . filter_var($title, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH) . '%';
            $where .= " AND ("; 

            // $where .= "(name LIKE '". $title_full . "') OR (code LIKE '". $title_full . "')";
            $where .= "(name LIKE '". $title_full . "')";

            $title_array = explode(' ', $title);
            if(count($title_array) > 1)
            {
                foreach ($title_array as $title_word) {
                    if(!empty($title_word))
                    {
                        $title_word = '%' . filter_var($title_word, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH) . '%';
                        // $where .= " OR (name LIKE '". $title_word . "') OR (code LIKE '". $title_word . "')";
                        $where .= "(name LIKE '". $title_full . "')";
                    }
                }
            }

            $where .= ")";
        }
        
        $where = ltrim($where, ' AND');
        $sql = $sql . $where . " ORDER BY name ASC LIMIT $offset, $limit";

        // echo $sql; die;

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function record_count() {
        return $this->db->count_all("countries");
    }

    public function getRows($limit, $offset, $id = FALSE)
    {
        $limit = (int) $limit;
        $offset = (int) $offset;

        if($id === FALSE)
        {
            $this->db->order_by('name', 'ASC');
            $this->db->select('id, name, code');
            $this->db->from('countries'); 
            $this->db->limit($limit, $offset); // produces LIMIT $offset, $limit

            $query = $this->db->get();
            return $query->result_array();
        }

        $this->db->select('id, name, code');
        $this->db->from('countries');
        $this->db->where('id', $id); 
        $query = $this->db->limit(1);

        $query = $this->db->get();
        return $query->row_array();
    }

    public function getRowsDropDown($id = FALSE)
    {
        if ($id === FALSE)
        {
            $this->db->order_by('name', 'ASC');
            $this->db->select('id, name, code');
            $this->db->from('countries'); 
            $query = $this->db->get();
            
            return $query->result_array();
        }
        $this->db->select('name');
        $this->db->from('countries');
        $this->db->where('id', $id); 
        $query = $this->db->limit(1);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function getRowsDropDown_ThatHasPremiumResources($limit = FALSE)
    {
        if ($limit === FALSE)
        {
            $limit = (int) $limit;
            $this->db->limit($limit);
        }
        $this->db->distinct();
        $this->db->order_by('countries.name', 'ASC');
        $this->db->select('countries.id, countries.name, countries.code');
        $this->db->from('countries');
        $this->db->join('universities', 'countries.id = universities.country_id', 'left');
        $this->db->join('past_exam_solutions', 'universities.id = past_exam_solutions.university_id', 'left');
        $this->db->where('past_exam_solutions.id IS NOT NULL', NULL, FALSE);

        $query = $this->db->get();
        return $query->result_array();
    }

    public function add()
    {
        $created_by = (int) $this->session->userdata('user_id');

        $data = array(
            'name' => trim($this->input->post('name')),
            'created_by' => $created_by
        );

        $this->db->insert('countries', $data);
    }

    public function delete($id)
    {
        $this->db->delete('countries', array('id' => (int) $id));
    }

    public function update($id)
    {

        $data = array(
            'name' => trim($this->input->post('name'))
        );
        $this->db->where('id', (int) $id);
        $this->db->update('countries', $data);
    }
}