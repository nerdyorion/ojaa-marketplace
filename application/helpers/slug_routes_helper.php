<?php
defined('BASEPATH') OR exit('No direct script access allowed');


function add_to_routes_free_resources($slug)
{
	// Get current CodeIgniter instance
    $CI =& get_instance();

	$data = array();

	if (!empty($slug)) {
		$data[] = '';
		$data[] = '$route[\'Free-Resources/' . $slug . '\'] = \'Free_Resources/view/0/' . $slug . '\';';
		$output = implode("\n", $data);

		write_file(APPPATH . 'cache/routes_free_resources.php', $output, 'a');
	}
}

function save_routes_free_resources()
{
	// Get current CodeIgniter instance
    $CI =& get_instance();

	$routes = $CI->Free_Project_Proposal_model->get_all_slug();

	$data = array();

	if (!empty($routes )) {
		$data[] = '<?php if ( ! defined(\'BASEPATH\')) exit(\'No direct script access allowed\');';

		foreach ($routes as $route) {
			$data[] = '$route[\'Free-Resources/' . $route['slug'] . '\'] = \'Free_Resources/view/0/' . $route['slug'] . '\';';
		}
		$output = implode("\n", $data);

		write_file(APPPATH . 'cache/routes_free_resources.php', $output);
	}
}


function add_to_routes_past_exam_solution($slug)
{
	// Get current CodeIgniter instance
    $CI =& get_instance();

	$data = array();

	if (!empty($username)) {
		$data[] = '';
		$data[] = '$route[\'Premium-Resources/' . $slug . '\'] = \'Premium_Resources/view/0/' . $slug . '\';';
		$output = implode("\n", $data);

		write_file(APPPATH . 'cache/routes_past_exam_solution.php', $output, 'a');
	}
}

function save_routes_past_exam_solution()
{
	// Get current CodeIgniter instance
    $CI =& get_instance();

	$routes = $CI->Past_Exam_Solution_model->get_all_slug();

	$data = array();

	if (!empty($routes )) {
		$data[] = '<?php if ( ! defined(\'BASEPATH\')) exit(\'No direct script access allowed\');';

		foreach ($routes as $route) {
			$data[] = '$route[\'Premium-Resources/' . $route['slug'] . '\'] = \'Premium_Resources/view/0/' . $route['slug'] . '\';';
		}
		$output = implode("\n", $data);

		write_file(APPPATH . 'cache/routes_past_exam_solution.php', $output);
	}
}



/*
function add_to_routes($username)
{
	// Get current CodeIgniter instance
    $CI =& get_instance();

	$data = array();

	if (!empty($username)) {
		$data[] = '';
		$data[] = '$route[\'@' . $username . '\'] = \'Home/view/' . $username . '\';';
		$data[] = '$route[\'@' . $username . '/services\'] = \'Home/services/' . $username . '\';';
		$output = implode("\n", $data);

		write_file(APPPATH . 'cache/routes.php', $output, 'a');
	}
}

function save_routes()
{
	// Get current CodeIgniter instance
    $CI =& get_instance();

	$routes = $CI->User_model->get_all_slug();

	$data = array();

	if (!empty($routes )) {
		$data[] = '<?php if ( ! defined(\'BASEPATH\')) exit(\'No direct script access allowed\');';

		foreach ($routes as $route) {
			$data[] = '$route[\'@' . $route['slug'] . '\'] = \'Home/view/' . $route['username'] . '\';';
			$data[] = '$route[\'@' . $route['slug'] . '/services\'] = \'Home/services/' . $route['username'] . '\';';
			// $data[] = '$route[\'@' . $route['slug'] . '/portfolio\'] = \'Home/portfolio/' . $route['username'] . '\';';
		}
		$output = implode("\n", $data);

		write_file(APPPATH . 'cache/routes.php', $output);
	}
}
*/



/*
function save_routes()
{
	// Get current CodeIgniter instance
    $CI =& get_instance();

	// $routes = $this->routes_model->get_all_routes();
	$routes = $CI->Product_model->get_all_slug();

	$data = array();

	if (!empty($routes )) {
		$data[] = '<?php if ( ! defined(\'BASEPATH\')) exit(\'No direct script access allowed\');';

		foreach ($routes as $route) {
			// $data[] = '$route[\'' . $route['uri'] . '\'] = \'' . $route['controller'] . '/' . $route['action'] . '\';';
			$data[] = '$route[\'' . $route['slug'] . '\'] = \'Products/view/' . $route['id'] . '\';';
		}
		$output = implode("\n", $data);

		write_file(APPPATH . 'cache/routes.php', $output);
	}
}
*/

?>