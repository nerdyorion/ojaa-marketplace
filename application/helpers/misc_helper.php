<?php
defined('BASEPATH') OR exit('No direct script access allowed');


function drive_setups()
{
  $output = array(
    array('id' => 1, 'name' => 'Lefthand Drive'),
    array('id' => 2, 'name' => 'Righthand Drive'),
  );
  return $output;
}

function fuel_types()
{
  $output = array(
    array('id' => 1, 'name' => 'Petrol'),
    array('id' => 2, 'name' => 'Diesel'),
    array('id' => 3, 'name' => 'Electric'),
    array('id' => 4, 'name' => 'Hybrid')
  );
  return $output;
}

function transmissions()
{
  $output = array(
    array('id' => 1, 'name' => 'Manual'),
    array('id' => 2, 'name' => 'Automatic')
  );
  return $output;
}

function subscriptionIsActive() {
    // Get current CodeIgniter instance
    $CI =& get_instance(); 
    $CI->load->model('Subscription_model');
    // session is already autoloaded
    // var_dump($CI->session->userdata); die;
    // We need to use $CI->session instead of $this->session
    $user_id = $CI->session->userdata('user_id');

    if (!isset($user_id)) 
    {
      return false;
    }
    else
    {
      return $CI->Subscription_model->isActive($user_id);
    }
}

function countries()
{
  $json = '[ 
  {"name": "Afghanistan", "code": "AF"}, 
  {"name": "land Islands", "code": "AX"}, 
  {"name": "Albania", "code": "AL"}, 
  {"name": "Algeria", "code": "DZ"}, 
  {"name": "American Samoa", "code": "AS"}, 
  {"name": "AndorrA", "code": "AD"}, 
  {"name": "Angola", "code": "AO"}, 
  {"name": "Anguilla", "code": "AI"}, 
  {"name": "Antarctica", "code": "AQ"}, 
  {"name": "Antigua and Barbuda", "code": "AG"}, 
  {"name": "Argentina", "code": "AR"}, 
  {"name": "Armenia", "code": "AM"}, 
  {"name": "Aruba", "code": "AW"}, 
  {"name": "Australia", "code": "AU"}, 
  {"name": "Austria", "code": "AT"}, 
  {"name": "Azerbaijan", "code": "AZ"}, 
  {"name": "Bahamas", "code": "BS"}, 
  {"name": "Bahrain", "code": "BH"}, 
  {"name": "Bangladesh", "code": "BD"}, 
  {"name": "Barbados", "code": "BB"}, 
  {"name": "Belarus", "code": "BY"}, 
  {"name": "Belgium", "code": "BE"}, 
  {"name": "Belize", "code": "BZ"}, 
  {"name": "Benin", "code": "BJ"}, 
  {"name": "Bermuda", "code": "BM"}, 
  {"name": "Bhutan", "code": "BT"}, 
  {"name": "Bolivia", "code": "BO"}, 
  {"name": "Bosnia and Herzegovina", "code": "BA"}, 
  {"name": "Botswana", "code": "BW"}, 
  {"name": "Bouvet Island", "code": "BV"}, 
  {"name": "Brazil", "code": "BR"}, 
  {"name": "British Indian Ocean Territory", "code": "IO"}, 
  {"name": "Brunei Darussalam", "code": "BN"}, 
  {"name": "Bulgaria", "code": "BG"}, 
  {"name": "Burkina Faso", "code": "BF"}, 
  {"name": "Burundi", "code": "BI"}, 
  {"name": "Cambodia", "code": "KH"}, 
  {"name": "Cameroon", "code": "CM"}, 
  {"name": "Canada", "code": "CA"}, 
  {"name": "Cape Verde", "code": "CV"}, 
  {"name": "Cayman Islands", "code": "KY"}, 
  {"name": "Central African Republic", "code": "CF"}, 
  {"name": "Chad", "code": "TD"}, 
  {"name": "Chile", "code": "CL"}, 
  {"name": "China", "code": "CN"}, 
  {"name": "Christmas Island", "code": "CX"}, 
  {"name": "Cocos (Keeling) Islands", "code": "CC"}, 
  {"name": "Colombia", "code": "CO"}, 
  {"name": "Comoros", "code": "KM"}, 
  {"name": "Congo", "code": "CG"}, 
  {"name": "Congo, The Democratic Republic of the", "code": "CD"}, 
  {"name": "Cook Islands", "code": "CK"}, 
  {"name": "Costa Rica", "code": "CR"}, 
  {"name": "Cote D\"Ivoire", "code": "CI"}, 
  {"name": "Croatia", "code": "HR"}, 
  {"name": "Cuba", "code": "CU"}, 
  {"name": "Cyprus", "code": "CY"}, 
  {"name": "Czech Republic", "code": "CZ"}, 
  {"name": "Denmark", "code": "DK"}, 
  {"name": "Djibouti", "code": "DJ"}, 
  {"name": "Dominica", "code": "DM"}, 
  {"name": "Dominican Republic", "code": "DO"}, 
  {"name": "Ecuador", "code": "EC"}, 
  {"name": "Egypt", "code": "EG"}, 
  {"name": "El Salvador", "code": "SV"}, 
  {"name": "Equatorial Guinea", "code": "GQ"}, 
  {"name": "Eritrea", "code": "ER"}, 
  {"name": "Estonia", "code": "EE"}, 
  {"name": "Ethiopia", "code": "ET"}, 
  {"name": "Falkland Islands (Malvinas)", "code": "FK"}, 
  {"name": "Faroe Islands", "code": "FO"}, 
  {"name": "Fiji", "code": "FJ"}, 
  {"name": "Finland", "code": "FI"}, 
  {"name": "France", "code": "FR"}, 
  {"name": "French Guiana", "code": "GF"}, 
  {"name": "French Polynesia", "code": "PF"}, 
  {"name": "French Southern Territories", "code": "TF"}, 
  {"name": "Gabon", "code": "GA"}, 
  {"name": "Gambia", "code": "GM"}, 
  {"name": "Georgia", "code": "GE"}, 
  {"name": "Germany", "code": "DE"}, 
  {"name": "Ghana", "code": "GH"}, 
  {"name": "Gibraltar", "code": "GI"}, 
  {"name": "Greece", "code": "GR"}, 
  {"name": "Greenland", "code": "GL"}, 
  {"name": "Grenada", "code": "GD"}, 
  {"name": "Guadeloupe", "code": "GP"}, 
  {"name": "Guam", "code": "GU"}, 
  {"name": "Guatemala", "code": "GT"}, 
  {"name": "Guernsey", "code": "GG"}, 
  {"name": "Guinea", "code": "GN"}, 
  {"name": "Guinea-Bissau", "code": "GW"}, 
  {"name": "Guyana", "code": "GY"}, 
  {"name": "Haiti", "code": "HT"}, 
  {"name": "Heard Island and Mcdonald Islands", "code": "HM"}, 
  {"name": "Holy See (Vatican City State)", "code": "VA"}, 
  {"name": "Honduras", "code": "HN"}, 
  {"name": "Hong Kong", "code": "HK"}, 
  {"name": "Hungary", "code": "HU"}, 
  {"name": "Iceland", "code": "IS"}, 
  {"name": "India", "code": "IN"}, 
  {"name": "Indonesia", "code": "ID"}, 
  {"name": "Iran, Islamic Republic Of", "code": "IR"}, 
  {"name": "Iraq", "code": "IQ"}, 
  {"name": "Ireland", "code": "IE"}, 
  {"name": "Isle of Man", "code": "IM"}, 
  {"name": "Israel", "code": "IL"}, 
  {"name": "Italy", "code": "IT"}, 
  {"name": "Jamaica", "code": "JM"}, 
  {"name": "Japan", "code": "JP"}, 
  {"name": "Jersey", "code": "JE"}, 
  {"name": "Jordan", "code": "JO"}, 
  {"name": "Kazakhstan", "code": "KZ"}, 
  {"name": "Kenya", "code": "KE"}, 
  {"name": "Kiribati", "code": "KI"}, 
  {"name": "Korea, Democratic People\"S Republic of", "code": "KP"}, 
  {"name": "Korea, Republic of", "code": "KR"}, 
  {"name": "Kuwait", "code": "KW"}, 
  {"name": "Kyrgyzstan", "code": "KG"}, 
  {"name": "Lao People\"S Democratic Republic", "code": "LA"}, 
  {"name": "Latvia", "code": "LV"}, 
  {"name": "Lebanon", "code": "LB"}, 
  {"name": "Lesotho", "code": "LS"}, 
  {"name": "Liberia", "code": "LR"}, 
  {"name": "Libyan Arab Jamahiriya", "code": "LY"}, 
  {"name": "Liechtenstein", "code": "LI"}, 
  {"name": "Lithuania", "code": "LT"}, 
  {"name": "Luxembourg", "code": "LU"}, 
  {"name": "Macao", "code": "MO"}, 
  {"name": "Macedonia, The Former Yugoslav Republic of", "code": "MK"}, 
  {"name": "Madagascar", "code": "MG"}, 
  {"name": "Malawi", "code": "MW"}, 
  {"name": "Malaysia", "code": "MY"}, 
  {"name": "Maldives", "code": "MV"}, 
  {"name": "Mali", "code": "ML"}, 
  {"name": "Malta", "code": "MT"}, 
  {"name": "Marshall Islands", "code": "MH"}, 
  {"name": "Martinique", "code": "MQ"}, 
  {"name": "Mauritania", "code": "MR"}, 
  {"name": "Mauritius", "code": "MU"}, 
  {"name": "Mayotte", "code": "YT"}, 
  {"name": "Mexico", "code": "MX"}, 
  {"name": "Micronesia, Federated States of", "code": "FM"}, 
  {"name": "Moldova, Republic of", "code": "MD"}, 
  {"name": "Monaco", "code": "MC"}, 
  {"name": "Mongolia", "code": "MN"}, 
  {"name": "Montenegro", "code": "ME"},
  {"name": "Montserrat", "code": "MS"},
  {"name": "Morocco", "code": "MA"}, 
  {"name": "Mozambique", "code": "MZ"}, 
  {"name": "Myanmar", "code": "MM"}, 
  {"name": "Namibia", "code": "NA"}, 
  {"name": "Nauru", "code": "NR"}, 
  {"name": "Nepal", "code": "NP"}, 
  {"name": "Netherlands", "code": "NL"}, 
  {"name": "Netherlands Antilles", "code": "AN"}, 
  {"name": "New Caledonia", "code": "NC"}, 
  {"name": "New Zealand", "code": "NZ"}, 
  {"name": "Nicaragua", "code": "NI"}, 
  {"name": "Niger", "code": "NE"}, 
  {"name": "Nigeria", "code": "NG"}, 
  {"name": "Niue", "code": "NU"}, 
  {"name": "Norfolk Island", "code": "NF"}, 
  {"name": "Northern Mariana Islands", "code": "MP"}, 
  {"name": "Norway", "code": "NO"}, 
  {"name": "Oman", "code": "OM"}, 
  {"name": "Pakistan", "code": "PK"}, 
  {"name": "Palau", "code": "PW"}, 
  {"name": "Palestinian Territory, Occupied", "code": "PS"}, 
  {"name": "Panama", "code": "PA"}, 
  {"name": "Papua New Guinea", "code": "PG"}, 
  {"name": "Paraguay", "code": "PY"}, 
  {"name": "Peru", "code": "PE"}, 
  {"name": "Philippines", "code": "PH"}, 
  {"name": "Pitcairn", "code": "PN"}, 
  {"name": "Poland", "code": "PL"}, 
  {"name": "Portugal", "code": "PT"}, 
  {"name": "Puerto Rico", "code": "PR"}, 
  {"name": "Qatar", "code": "QA"}, 
  {"name": "Reunion", "code": "RE"}, 
  {"name": "Romania", "code": "RO"}, 
  {"name": "Russian Federation", "code": "RU"}, 
  {"name": "RWANDA", "code": "RW"}, 
  {"name": "Saint Helena", "code": "SH"}, 
  {"name": "Saint Kitts and Nevis", "code": "KN"}, 
  {"name": "Saint Lucia", "code": "LC"}, 
  {"name": "Saint Pierre and Miquelon", "code": "PM"}, 
  {"name": "Saint Vincent and the Grenadines", "code": "VC"}, 
  {"name": "Samoa", "code": "WS"}, 
  {"name": "San Marino", "code": "SM"}, 
  {"name": "Sao Tome and Principe", "code": "ST"}, 
  {"name": "Saudi Arabia", "code": "SA"}, 
  {"name": "Senegal", "code": "SN"}, 
  {"name": "Serbia", "code": "RS"}, 
  {"name": "Seychelles", "code": "SC"}, 
  {"name": "Sierra Leone", "code": "SL"}, 
  {"name": "Singapore", "code": "SG"}, 
  {"name": "Slovakia", "code": "SK"}, 
  {"name": "Slovenia", "code": "SI"}, 
  {"name": "Solomon Islands", "code": "SB"}, 
  {"name": "Somalia", "code": "SO"}, 
  {"name": "South Africa", "code": "ZA"}, 
  {"name": "South Georgia and the South Sandwich Islands", "code": "GS"}, 
  {"name": "Spain", "code": "ES"}, 
  {"name": "Sri Lanka", "code": "LK"}, 
  {"name": "Sudan", "code": "SD"}, 
  {"name": "Suriname", "code": "SR"}, 
  {"name": "Svalbard and Jan Mayen", "code": "SJ"}, 
  {"name": "Swaziland", "code": "SZ"}, 
  {"name": "Sweden", "code": "SE"}, 
  {"name": "Switzerland", "code": "CH"}, 
  {"name": "Syrian Arab Republic", "code": "SY"}, 
  {"name": "Taiwan, Province of China", "code": "TW"}, 
  {"name": "Tajikistan", "code": "TJ"}, 
  {"name": "Tanzania, United Republic of", "code": "TZ"}, 
  {"name": "Thailand", "code": "TH"}, 
  {"name": "Timor-Leste", "code": "TL"}, 
  {"name": "Togo", "code": "TG"}, 
  {"name": "Tokelau", "code": "TK"}, 
  {"name": "Tonga", "code": "TO"}, 
  {"name": "Trinidad and Tobago", "code": "TT"}, 
  {"name": "Tunisia", "code": "TN"}, 
  {"name": "Turkey", "code": "TR"}, 
  {"name": "Turkmenistan", "code": "TM"}, 
  {"name": "Turks and Caicos Islands", "code": "TC"}, 
  {"name": "Tuvalu", "code": "TV"}, 
  {"name": "Uganda", "code": "UG"}, 
  {"name": "Ukraine", "code": "UA"}, 
  {"name": "United Arab Emirates", "code": "AE"}, 
  {"name": "United Kingdom", "code": "GB"}, 
  {"name": "United States", "code": "US"}, 
  {"name": "United States Minor Outlying Islands", "code": "UM"}, 
  {"name": "Uruguay", "code": "UY"}, 
  {"name": "Uzbekistan", "code": "UZ"}, 
  {"name": "Vanuatu", "code": "VU"}, 
  {"name": "Venezuela", "code": "VE"}, 
  {"name": "Viet Nam", "code": "VN"}, 
  {"name": "Virgin Islands, British", "code": "VG"}, 
  {"name": "Virgin Islands, U.S.", "code": "VI"}, 
  {"name": "Wallis and Futuna", "code": "WF"}, 
  {"name": "Western Sahara", "code": "EH"}, 
  {"name": "Yemen", "code": "YE"}, 
  {"name": "Zambia", "code": "ZM"}, 
  {"name": "Zimbabwe", "code": "ZW"} 
  ]';

  return json_decode($json, true);
}

function getInterestRate() {
  // Get current CodeIgniter instance
  $CI =& get_instance();
  
  return $CI->Setting_model->getInterestRate();
}

// password (should be strong which includes at least one special character, One number and one Capital Letter)
function passwordStrong($password)
{
    if( 
        // ctype_alnum($password) // numbers & digits only 
        preg_match('`[A-Z]`',$password) // at least one upper case 
        && preg_match('`[a-z]`',$password) // at least one lower case 
        && preg_match('`[0-9]`',$password) // at least one digit 
        /**/ && preg_match('/[\'^!£$%&*()}{@#~?><>,|=_+¬-]/', $password) // at least one special character
        
        // && preg_match('/[^a-zA-Z0-9]+/', $password) // at least one special character
    )
    {
        return true; // strong
    }
    else
    { 
        return false; // not strong
    } 
}

if(!function_exists('log_transaction'))
{
  function log_transaction($input_data)
  {
    $CI =& get_instance();
    $handle = fopen($CI->config->item('home_dir_url') . "application/logs/transactions-".date('Y-m-d').".log", "a");
    fwrite($handle, $input_data . PHP_EOL);
    fclose($handle);
  }
}

if(!function_exists('log_transaction_raw'))
{
  function log_transaction_raw($input_data)
  {
    $CI =& get_instance();
    $handle = fopen($CI->config->item('home_dir_url') . "application/logs/transactions-raw-".date('Y-m-d').".log", "a");
    fwrite($handle, $input_data . PHP_EOL);
    fclose($handle);
  }
}

if(!function_exists('dateEmpty'))
{
  function dateEmpty($date)
  {
    if(is_null($date) || $date == "0000-00-00" || $date == "00:00:00" || $date == "0000-00-00 00:00:00" )
      return true;

    return false;
  }
}

function site_details()
{
  $CI =& get_instance();
  $contact = $CI->Site_Detail_model->getRows(0, 0, 1);

  return $contact;
}

if(!function_exists('slugify'))
{
  function slugify($url)
  {
    # Prep string with some basic normalization
    $url = strtolower($url);
    $url = strip_tags($url);
    $url = stripslashes($url);
    $url = html_entity_decode($url);
    # Remove quotes (can't, etc.)
    $url = str_replace('\'', '', $url);

    # Replace non-alpha numeric with hyphens
    $url = trim(preg_replace('/[^a-z0-9]+/', '-', $url), '-');

    return $url;
  }
}

if(!function_exists('dd'))
{
  function dd($var)
  {
    echo '<pre>';
    var_dump($var);
    echo '</pre>';
    die;
  }
}


function getPageMeta() {
  // Get current CodeIgniter instance
  $CI =& get_instance();
  $url = $CI->uri->segment(1);
  $url = empty($url) ? '/' : $url;
  
  return $CI->Page_model->getRowByURL($url);
}

if ( ! function_exists('ellipsize_no_strip'))
{
  /**
   * Ellipsize String
   *
   * This function will NOT strip tags from a string, but split it at its max_length and ellipsize
   *
   * @param string  string to ellipsize
   * @param int max length of string
   * @param mixed int (1|0) or float, .5, .2, etc for position to split
   * @param string  ellipsis ; Default '...'
   * @return  string  ellipsized string
   */
  function ellipsize_no_strip($str, $max_length, $position = 1, $ellipsis = '&hellip;')
  {
    // Strip tags
    // $str = trim(strip_tags($str));
    $str = trim($str);

    // Is the string long enough to ellipsize?
    if (mb_strlen($str) <= $max_length)
    {
      return $str;
    }

    $beg = mb_substr($str, 0, floor($max_length * $position));
    $position = ($position > 1) ? 1 : $position;

    if ($position === 1)
    {
      $end = mb_substr($str, 0, -($max_length - mb_strlen($beg)));
    }
    else
    {
      $end = mb_substr($str, -($max_length - mb_strlen($beg)));
    }

    return $beg.$ellipsis.$end;
  }
}

function highlight_word( $content, $word, $color ) {
    // $replace = '<span style="background-color: ' . $color . ';">' . $word . '</span>'; // create replacement
    $replace = '<mark>' . $word . '</mark>'; // create replacement
    $content = str_replace( $word, $replace, $content ); // replace content

    return $content; // return highlighted data
}

function highlight_words( $content, $words, $colors = array('#88ccff', '#cc88ff') ) {
    $color_index = 0; // index of color (assuming it's an array)

    // loop through words
    foreach( $words as $word ) {
        $content = highlight_word( $content, $word, $colors[$color_index] ); // highlight word
        $color_index = ( $color_index + 1 ) % count( $colors ); // get next color index
    }

    return $content; // return highlighted data
}

function base64url_encode($data) { 
  return rtrim(strtr(base64_encode($data), '+/', '-_'), '='); 
} 

function base64url_decode($data) { 
  return base64_decode(str_pad(strtr($data, '-_', '+/'), strlen($data) % 4, '=', STR_PAD_RIGHT)); 
}       

function getArrayKey($array, $field, $value)
{
   foreach($array as $key => $product)
   {
    // var_dump($value);
    // var_dump($product[$field]); die;
    if( $product[$field] == $value )
    {
      // var_dump($key); die;
      return $key;
    }
   }
   return false;
}

function time_elapsed_string($datetime, $full = false) {
    $now = new DateTime;
    $ago = new DateTime($datetime);
    $diff = $now->diff($ago);

    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;

    $string = array(
        'y' => 'year',
        'm' => 'month',
        'w' => 'week',
        'd' => 'day',
        'h' => 'hour',
        'i' => 'minute',
        's' => 'second',
    );
    foreach ($string as $k => &$v) {
        if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
        } else {
            unset($string[$k]);
        }
    }

    if (!$full) $string = array_slice($string, 0, 1);
    return $string ? implode(', ', $string) . ' ago' : 'just now';
}

function notifications() {
  // Get current CodeIgniter instance
  $CI =& get_instance();
  $user_id = $CI->session->userdata('user_id');
  $user_role_id = $CI->session->userdata('user_role_id');
  $notifications = array();


  if($user_role_id == 1 || $user_role_id == 2) // Super Admin and Admin
  {
    // get notifications where user_id = 0
    $notifications = $CI->Notification_model->getRowsUnRead(0);

    return $notifications;
  }
  elseif($user_role_id == 3 || $user_role_id == 4) // Artist and Agent
  {
    // get notifications where user_id = $user_id
    $notifications = $CI->Notification_model->getRowsUnRead($user_id);

    return $notifications;
  }
  else
  {
    return false;
  }
}

function add_notification($user_id, $message) {
  // Get current CodeIgniter instance
  $CI =& get_instance();
  // $user_id = $CI->session->userdata('user_id');
  // $user_role_id = $CI->session->userdata('user_role_id');
  $user_id = (int) $user_id;
  // $message = filter_var($message, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
  
  $CI->Notification_model->add($user_id, $message);
  return true;
}

function dashIfEmpty($var, $default = '-') {
  return empty($var) ? $default : $var;
}

function image_thumb($image_url)
{
  if(!empty($image_url))
  {
    $image_url_thumb = "";

    $arr = explode(".", $image_url);
    $ext = array_pop($arr);
    $image_url_thumb = implode('.', $arr) . '_thumb.' . $ext;

    return $image_url_thumb;
  }
  else
  {
    return $image_url;
  }
}


function sendmail($to, $to_name, $subject, $message)
{
  // Get current CodeIgniter instance
  $CI =& get_instance();

  $domain = 'mailserve.neegles.com';
  $app_name = $CI->config->item('app_name');

  $to = filter_var($to, FILTER_SANITIZE_EMAIL);
  $to_name = filter_var($to_name, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
  $subject = filter_var($subject, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);

  $CI->benchmark->mark('code_start');
  // $CI->load->library('email');

  $params = array(
    'from'     => $app_name . ' <ojaa@' . $domain . '>',
    'to'       => $to_name . ' <' . $to . '>',
    // 'bcc'      => array('NerdyOrion Notify <nerdyorion.notify@gmail.com>', 'NFD <' . $CI->config->item('info_email') . '>'),
    // 'bcc'      => array('NerdyOrion Notify <nerdyorion.notify@gmail.com>'),
    'bcc'      => array('NerdyOrion <nerdyorion@gmail.com>'),
    'subject'  => $subject,
    'text'     => strip_tags($message),
    'html'     => $message,
  );
  $url = 'https://api.mailgun.net/v3/' . $domain . '/messages';

  $ch = curl_init();
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch, CURLOPT_USERPWD, "api:key-0bb10e7562bb919fa3a4edec8baabb06");
  curl_setopt($ch, CURLOPT_TIMEOUT, 30);
  curl_setopt($ch, CURLOPT_POST, true);
  curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params));

  $output = curl_exec($ch);

  if($output === false)
  {
    $output = "URL: $url";
    $output = $output . "; Error Number: " . curl_errno($ch);
    $output = $output . "; Error String: " . curl_error($ch);
  }
  curl_close($ch);

  $CI->benchmark->mark('code_end');

  $time_taken = $CI->benchmark->elapsed_time('code_start', 'code_end');


  error_log($output . "; Duration: $time_taken");
}

function addToList($email, $name)
{
  // Get current CodeIgniter instance
  $CI =& get_instance();

  $domain = 'mailserve.neegles.com';
  $app_name = $CI->config->item('app_name');

  $email = filter_var($email, FILTER_SANITIZE_EMAIL);
  $name = filter_var($name, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);

  $CI->benchmark->mark('code_start');

  $params = array(
    'subscribed'       => TRUE,
    'address'     => $name . ' <' . $email . '>',
    'name'       => $name,
    'description'       => $app_name,
    'vars'       => json_encode(array('source' => $app_name)), 
    // 'vars'       => "{'source': $app_name}", 
    'upsert'       => TRUE, 
  );
  $url = 'https://api.mailgun.net/v3/lists/list1@' . $domain . '/members';

  $ch = curl_init();
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch, CURLOPT_USERPWD, "api:key-0bb10e7562bb919fa3a4edec8baabb06");
  curl_setopt($ch, CURLOPT_TIMEOUT, 30);
  curl_setopt($ch, CURLOPT_POST, true);
  curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params));

  $output = curl_exec($ch);

  if($output === false)
  {
    $output = "URL: $url";
    $output = $output . "; Error Number: " . curl_errno($ch);
    $output = $output . "; Error String: " . curl_error($ch);
  }
  curl_close($ch);

  $CI->benchmark->mark('code_end');

  $time_taken = $CI->benchmark->elapsed_time('code_start', 'code_end');


  error_log($output . "; Duration: $time_taken");
}

/*
function sendmail($to, $to_name, $subject, $message)
{
  // Get current CodeIgniter instance
  $CI =& get_instance();
  $app_name = $CI->config->item('app_name');

  $to = filter_var($to, FILTER_SANITIZE_EMAIL);
  $to_name = filter_var($to_name, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
  $subject = filter_var($subject, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);

  $CI->benchmark->mark('code_start');
  $CI->load->library('email');


  $config = array(
    'protocol' => 'smtp',
    'smtp_host' => 'mail.neegles.com', // smtp.mailgun.org
    'smtp_port' => 465, // 465 587
    'smtp_user' => 'martinscar@neegles.com',  // mailserve
    'smtp_pass' =>  'martinscar*123', // '2001d18ab718290911a7ce478ea3cd2f-a9919d1f-7d0d7c94', 
    'mailtype' => 'html',
    'charset' => 'utf-8', // utf-8 iso-8859-1
    // 'wordwrap' => TRUE
  );

  $CI->load->library('email', $config);
  $CI->email->set_newline("\r\n");

  // $CI->email->initialize($config);

  $CI->email->from('martinscar@neegles.com', $app_name);
  $CI->email->to($to, $to_name);
  $CI->email->bcc($CI->config->item('admin_bcc_email'));
  $CI->email->subject($subject);
  $CI->email->message($message);
  $CI->email->set_mailtype("html");

  $output = '';
  if($CI->email->send())
  {
    // great
    $output = 'mail sent to ' . $to;
  }
  else
  {
    $output = 'mail not sent to ' . $to;
    $output .= "; error: " . $CI->email->print_debugger() . PHP_EOL;
    // error_log($this->email->print_debugger());
    error_log($error);
  }

  $CI->benchmark->mark('code_end');

  $time_taken = $CI->benchmark->elapsed_time('code_start', 'code_end');


  error_log($output . "; Duration: $time_taken");
}*/

function getHeaderEnrichment()
{
  $telco = '';
  $msisdn = '';
  if(isset($_SERVER['HTTP_MSISDN']))
  {
    $telco = 'MTN'; // or GLO
    $msisdn = $_SERVER['HTTP_MSISDN'];
    return array('telco' => $telco, 'msisdn' => $msisdn);
  }
  elseif(isset($_SERVER['HTTP_X_UP_CALLING_LINE_ID']))
  {
    $telco = 'EMTS';
    $msisdn = $_SERVER['HTTP_X_UP_CALLING_LINE_ID'];
    return array('telco' => $telco, 'msisdn' => $msisdn);
  }
  else
  {
    return '';
  }
}


function format_msisdn($msisdn, $country_code = '234')
{
  // format msisdn
  $msisdn = trim($msisdn);

  if (substr($msisdn, 0, 1) == '0'){
    $msisdn = $country_code . substr($msisdn, 1);
  }
  elseif (substr($msisdn, 0, 1) == '+'){
    $msisdn = substr($msisdn, 1);
  }
  elseif (substr($msisdn, 0, 3) == '234'){
    // do nothing
  }
  else
  {
    // $msisdn is in the format 708900000
    $msisdn = '234' .  $msisdn;
  }
  return $msisdn;
}

function log_subscribe($text)
{
  // log
  $handle = fopen(APPPATH . "logs/subscriptions-".date('Y-m-d').".log", "a");
  fwrite($handle, $text);
}

function HTTPGet($url)
{
  $ch = curl_init();  

  curl_setopt($ch,CURLOPT_URL,$url);
  curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
  curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
  //curl_setopt($ch, CURLOPT_CONNECTTIMEOUT ,0); // 0 for wait infinitely, not a good practice
  curl_setopt($ch, CURLOPT_TIMEOUT, 30); //in seconds
  //  curl_setopt($ch,CURLOPT_HEADER, false); 

  $output=curl_exec($ch);

  if($output === false)
  {
    $output = "Error Number:".curl_errno($ch)."<br>";
    $output = $output . ". Error String:".curl_error($ch);
  }
  curl_close($ch);
  return $output;
}

function encryptURL($url)
{
  // $url = base_url(); // this is the sp landing page, Etisalat will redirect customer to this page.
  // $url = substr($url, 0, strlen($url) - 1); // remove trailing /
  // die($url);
  $key = "d"; // this is a unique key for sp,
  $encryptedUrl = openssl_encrypt($url,"DES-EDE3",$key);
  $destinationUrl = urlencode($encryptedUrl); // the final encrypted value must be urlencoded since the data will be pass over GET

  return $destinationUrl;
}

function getURL($action, $username)
{
    // Get current CodeIgniter instance
    $CI =& get_instance();

    $signup_link = $CI->config->base_url() . 'register/agent';
    $book_link = $CI->config->base_url() . 'book/@' . $username;

    switch (strtolower($action)) {
      case 'book':
        if(is_null($CI->session->userdata('user_role_id')) || $CI->session->userdata('user_role_id') != '4')
        {
          return $signup_link;
        }
        else
        {
          return $book_link;
        }
        break;
      
      default:
        return $signup_link;
        break;
    }
}

function showImage($image_url)
{
    // Get current CodeIgniter instance
    $CI =& get_instance();

    $default_image_url = 'assets/artist/images/users/default-user.jpg';

    if(file_exists($image_url))
    {
      return $image_url;
    }
    else
    {
      return $default_image_url;
    }
}


?>