
<div class="page-header">
  <div class="container">
    <div class="row">
      <div class="col-md-12 text-center">
        <h2>Loan Applications</h2>
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?php echo $this->config->base_url(); ?>">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Loan Applications</li>
          </ol>
        </nav>
      </div>
    </div>
  </div>
</div>
<section class="about-page-shortcode section-padding">
  <div class="container rowpadding-with-border">
    <div class="row">
      <?php $this->load->view('side-nav-menu'); echo "\n"; ?>
      <div class="col-lg-9">
        <div class="row">
          <div class="col-lg-12">
            <div class="about-content">
              <h2>Loan Applications</h2>

              <?php if($error_code == 0 && !empty($error)): ?>
                <div class="alert alert-success alert-dismissable fade show">
                  <a href="<?php echo current_url();?>#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  <strong>Success!</strong> <?php echo $error; ?>
                </div>
                <?php elseif($error_code == 1 && !empty($error)): ?>
                  <div class="alert alert-danger alert-dismissable fade show">
                    <a href="<?php echo current_url();?>#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>Error!</strong> <?php echo $error; ?>
                  </div>
                  <?php else: ?>
                  <?php endif; ?>


                  <p>At <?php echo $this->config->item('app_name'); ?>, wе’rе hеrе to рrоvіdе уоu wіth fіnаnсіаl ѕоlutіоnѕ fоr аll уоur lеndіng needs. Whether you are lооkіng fоr a loan, mortgage, or lіnе оf credit, our knоwlеdgеаblе and experienced lending tеаm wіll explain оur lеndіng орtіоnѕ to уоu іn a ѕtrаіghtfоrwаrd mаnnеr. </p>

                  <?php if(empty($rows)): ?>
                    <p>You have not applied for any loan yet! <br /></p>
                  <?php endif; ?>
                  <p><a href="New-Loan-Application" class="button btn btn-default ">&nbsp;<i class="fa fa-sign-in"></i> START A NEW LOAN APPLICATION&nbsp;</a></p>

                  <?php if(empty($rows)): ?>
                  </p>
                  <?php else: ?>
                    <div class="table-responsive">
                      <table class="table">
                        <thead>
                          <tr>
                            <th>#</th>
                            <th>Amount</th>
                            <th>Purpose</th>
                            <th>Duration</th>
                            <th>Date</th>
                            <th>Status</th>
                            <th>Actions</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php foreach($rows as $row): ?>
                           <tr>
                            <td><?php echo $sn++; ?></td>
                            <td class="price">
                              NGN<?php echo number_format($row['loan_amount'], 2); ?>

                              <?php if(!empty($row['stmt_of_acct_url']) && file_exists($upload_save_path . $row['stmt_of_acct_url'])): ?>
                              <br /><strong><a href="applications/download_statement/<?php echo $row['id']; ?>">View / Download Statement</a></strong>
                            <?php endif; ?>
                          </td>
                          <td class="description"><?php echo dashIfEmpty($row['loan_purpose']); ?></td>
                          <td class="description"><?php echo dashIfEmpty($row['loan_duration']); ?></td>
                          <td class="price"><?php echo dateEmpty($row['date_created']) ? "-" : date('M d, Y h:i A', strtotime($row['date_created'])); ?></td>

                          <td class="price">
                            <b>
                              <?php if($row['is_approved'] == 0 && $row['is_rejected'] == 0 && $row['loan_repayed'] == 0): ?>
                                <mark class="bg-dark text-white">PENDING</mark>
                              <?php elseif($row['is_approved'] == 1 && $row['is_disbursed'] == 0): ?>
                                <mark class="bg-warning text-white">APPROVED</mark>
                              <?php elseif($row['is_disbursed'] == 1): ?>
                                <mark class="bg-success text-white">DISBURSED</mark>
                              <?php elseif($row['is_rejected'] == 1): ?>
                                <mark class="bg-danger text-white">REJECTED</mark>
                              <?php elseif($row['loan_repayed'] == 1): ?>
                                <mark class="bg-info text-white">REPAYED</mark>
                              <?php else: ?>
                              <?php endif; ?>
                            </b>
                          </td>

                          <td class="price">
                            <?php if($row['is_approved'] == 1 && $row['is_disbursed'] == 1 && $row['loan_repayed'] == 0): ?>
                              <a href="Make-Loan-Payment/<?php echo $row['id']; ?>" data-toggle="tooltip" data-original-title="Make Payment" target="_blank"> <i class="fa fa-money text-inverse m-r-10"></i> Make Payment </a> 
                            <?php endif; ?>
                          </td>
                        </tr>
                      <?php endforeach; ?>
                    </tbody>
                  </table>
                </div>
              <?php endif; ?>
              <div class=" pull-right">

                <nav aria-label="...">
                  <ul class="pagination">
                    <?php echo $links; ?>
                  </ul>
                </nav>
              </div>



            </div>
          </div>
        </div>


      </div>
    </div>
  </div>
</section>
<!-- end of about section -->

<?php $this->load->view('footer'); echo "\n"; ?>

<script type="text/javascript">
  function validate()
  {

    var $btn = $('button[type="submit"]').button('loading');
    $(':input[type="submit"]').prop('disabled', true);
    $('button[type="submit"]').prop('disabled', true);
    return true;
  }
</script>

</body>

</html>