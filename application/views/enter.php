
    

<div class="fullwidth-box" style="background-color: #efefef;">
			<div class="container text-center">
				<h2 class="text-uppercase margin-bottom-60 letter-spacing-10 text-center opacity-30 text-size-8">How to Enter</h2>
				<div class="grid margin-bottom-lg columns-md-1 columns-sm-1">
					
					
				
					<div class="col">
						<div class="icon-left">
							
							<h3 class="margin-bottom-10">Step 1</h3>
							<p class="text-color2">Provide your full name, contact details and home address.</p>
						</div>
					</div>
					<div class="col">
						<div class="icon-left">
							
							<h3 class="margin-bottom-10">Step 2</h3>
							<p class="text-color2">Select your University.</p>
						</div>
					</div>
					<div class="col">
						<div class="icon-left">
							
							<h3 class="margin-bottom-10">Step 3</h3>
							<p class="text-color2">Upload three (3) images of dresses you have designed before.</p>
						</div>
					</div>
					<div class="col">
						<div class="icon-left">
							
							<h3 class="margin-bottom-10">Step 4</h3>
							<p class="text-color2">Share the uploaded images on your social media pages using the #MastersOfStyleD2D and #Dare2DreamDesigner</p>
						</div>
					</div>
					<div class="col">
						<div class="icon-left">
							
							<h3 class="margin-bottom-10">Step 5</h3>
							<p class="text-color2">Agree to terms & conditions of the competition</p>
						</div>
					</div>
					
						<div class="col">
						<div class="icon-left">
							
							<h3 class="margin-bottom-10">Step 6</h3>
							<p class="text-color2">Bring the 3 dresses to the Masters of Style Sewing booth when it comes to your campus for final vetting by Judges to qualify for the competition.</p>
						</div>
					</div>
					
					<div class="col">
						<div class="icon-left">
							
							<h3 class="margin-bottom-10" style="color: red;">Note that all participants that enter this competition must be able to sew.</h3>
							
						</div>
					</div>
					
					
					
					
				</div>
			</div>
		</div>


</div>
<!-- // END Content -->


<?php $this->load->view('footer'); echo "\n"; ?>

</body>
</html> 