<style type="text/css">
    .col-centered{
        float: none;
        margin: 0 auto;
    }
    .card-number {
        font-family: Source Code Pro,monospace;
        white-space: nowrap;
    }
</style>

<hr />
<div class="section">

    <div class="content-wrap" style="padding-top: 0px;">

        <div class="container">
            <div class="row">	

                <div class="col-md-6 col-lg-4 col-sm-8 col-centered">
                    <h3>Secure Payment</h3>
                    <div class="card">
                        <div class="card-header bg-success text-white">Pay with Stripe</div>
                        <div class="card-body bg-light">
                            <?php if (validation_errors()): ?>
                                <div class="alert alert-danger" role="alert">
                                    <strong>Oops!</strong>
                                    <?php echo validation_errors() ;?> 
                                </div>  
                            <?php endif ?>
                            <div id="payment-errors"></div>
                            <?php echo form_open('Process-Payment/stripe', 'id="paymentFrm"'); ?>
                                <input type="hidden" name="payment_type" value="<?php echo $payment_type; ?>" />

                                <div class="form-group">
                                    <input type="text" name="name" class="form-control" placeholder="Name" value="<?php echo $name; ?>" required readonly>
                                </div>  

                                <div class="form-group">
                                    <input type="email" name="email" class="form-control" placeholder="email@you.com" value="<?php echo $email; ?>" required readonly />
                                </div>

                                <div class="form-group">
                                    <input type="number" name="card_num" id="card_num" class="form-control" placeholder="Card Number" autocomplete="off" value="<?php echo set_value('card_num'); ?>" required>
                                </div>


                                <div class="row">

                                    <div class="col-sm-8">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <input type="text" name="exp_month" maxlength="2" class="form-control" id="card-expiry-month" autocomplete="off" placeholder="MM" value="<?php echo set_value('exp_month'); ?>" required>
                                                </div>
                                            </div>

                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <input type="text" name="exp_year" class="form-control" maxlength="4" id="card-expiry-year" autocomplete="off" placeholder="YYYY" required="" value="<?php echo set_value('exp_year'); ?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <input type="text" name="cvc" id="card-cvc" maxlength="3" class="form-control" autocomplete="off" placeholder="CVC" value="<?php echo set_value('cvc'); ?>" required>
                                        </div>
                                    </div>
                                </div>




                                <div class="form-group text-right">
                                    <button class="btn btn-secondary" type="reset">Reset</button>
                                    <button type="submit" id="payBtn" class="btn btn-success">Submit</button>
                                </div>
                            </form>     
                        </div>
                    </div>
                            <!-- <img src="assets/images/stripe-1.png" alt="Stripe Logo" /> -->
                     <img src="assets/images/stripe-2.jpg" style="width: 200px;" alt="Stripe Logo" />
                     <!-- <img src="assets/images/stripe-1.png" style="width: 200px;" alt="Stripe Logo" /> -->

                </div>

            </div>
        </div>
    </div>
</div> 

<?php $this->load->view('footer'); echo "\n"; ?>

<!-- Stripe JavaScript library -->
<script type="text/javascript" src="https://js.stripe.com/v2/"></script>    

<script type="text/javascript">
        //set your publishable key
        Stripe.setPublishableKey('<?php echo $this->config->item('stripe_publish_key_live'); ?>');
        
        //callback to handle the response from stripe
        function stripeResponseHandler(status, response) {
            if (response.error) {
                //enable the submit button
                $('#payBtn').removeAttr("disabled");
                //display the errors on the form
                // $('#payment-errors').attr('hidden', 'false');
                $('#payment-errors').addClass('alert alert-danger');
                $("#payment-errors").html(response.error.message);
            } else {
                var form$ = $("#paymentFrm");
                //get token id
                var token = response['id'];
                //insert the token into the form
                form$.append("<input type='hidden' name='stripeToken' value='" + token + "' />");
                //submit form to the server
                form$.get(0).submit();
            }
        }
        $(document).ready(function() {
            //on form submit
            $("#paymentFrm").submit(function(event) {
                //disable the submit button to prevent repeated clicks
                $('#payBtn').attr("disabled", "disabled");
                $('#payBtn').html('<span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span> Submit');
                
                //create single-use token to charge the user
                Stripe.createToken({
                    number: $('#card_num').val(),
                    cvc: $('#card-cvc').val(),
                    exp_month: $('#card-expiry-month').val(),
                    exp_year: $('#card-expiry-year').val()
                }, stripeResponseHandler);
                
                //submit from callback
                return false;
            });
        });
    </script>
</body>

</html>