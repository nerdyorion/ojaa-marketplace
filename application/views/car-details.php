
      <!-- Breadcromb Area Start -->
      <section class="gauto-breadcromb-area section_70">
         <div class="container">
            <div class="row">
               <div class="col-md-12">
                  <div class="breadcromb-box">
                     <h3>Car Details</h3>
                     <ul>
                        <li><i class="fa fa-home"></i></li>
                        <li><a href="<?php echo $this->config->base_url(); ?>">Home</a></li>
                        <li><i class="fa fa-angle-right"></i></li>
                        <li><a href="<?php echo $this->config->base_url(); ?>cars">Car Listing</a></li>
                        <li><i class="fa fa-angle-right"></i></li>
                        <li><?php echo $row['name']; ?></li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <!-- Breadcromb Area End -->
       
       
      <!-- Car Booking Area Start -->
      <section class="gauto-car-booking section_70">
         <div class="container">
            <div class="row">
               <div class="col-lg-6">
                  <div class="car-booking-image">
                     <img src="<?php echo $this->config->base_url(); ?>assets/img/cars/<?php echo $row['image_url']; ?>" alt="<?php echo $row['name']; ?>" />
                  </div>
               </div>
               <div class="col-lg-6">
                  <div class="car-booking-right">
                     <p class="rental-tag">rental</p>
                     <h3><?php echo $row['name']; ?></h3>
                     <div class="price-rating">
                        <div class="price-rent">
                           <h4>$<?php echo number_format($row['price_per_day']); ?><span>/ Day</span></h4>
                        </div>
                        <!-- <div class="car-rating">
                           <ul>
                              <li><i class="fa fa-star"></i></li>
                              <li><i class="fa fa-star"></i></li>
                              <li><i class="fa fa-star"></i></li>
                              <li><i class="fa fa-star"></i></li>
                              <li><i class="fa fa-star-half-o"></i></li>
                           </ul>
                           <p>(123 rating)</p>
                        </div> -->
                     </div>
                     <p><?php echo $row['description']; ?></p>
                     <div class="car-features clearfix">
                        <ul>
                           <li><i class="fa fa-car"></i> Model:<?php echo $row['model_year']; ?></li>
                           <li><i class="fa fa-cogs"></i> <?php echo $transmissions[getArrayKey($transmissions, "id", $row['transmission'])]['name']; ?></li>
                           <li><i class="fa fa-dashboard"></i> <?php echo number_format($row['kmph']); ?>kmph</li>
                           <?php if((int) $row['has_ac'] == 1): ?>
                              <li><i class="fa fa-empire"></i> AC</li>
                           <?php endif; ?>
                        </ul>
                        <ul>
                           <?php if((int) $row['has_gps'] == 1): ?>
                              <li><i class="fa fa-eye"></i> GPS Navigation</li>
                           <?php endif; ?>
                           <?php if(!is_null($row['drive_setup']) && getArrayKey($drive_setups, "id", $row['drive_setup']) !== false): ?>
                              <li><i class="fa fa-cogs"></i> <?php echo dashIfEmpty($drive_setups[getArrayKey($drive_setups, "id", $row['drive_setup'])]['name']) ?></li>
                           <?php endif; ?>
                           <?php if(!is_null($row['fuel_type']) && getArrayKey($fuel_types, "id", $row['fuel_type']) !== false): ?>
                              <li><i class="fa fa-dashboard"></i> <?php echo dashIfEmpty($fuel_types[getArrayKey($fuel_types, "id", $row['fuel_type'])]['name']) ?></li>
                           <?php endif; ?>
                           <?php if((int) $row['has_anti_lock_brakes'] == 1): ?>
                              <li><i class="fa fa-empire"></i> Anti-Lock Brakes</li>
                           <?php endif; ?>
                        </ul>
                        <ul>
                           <?php if((int) $row['has_remote_keyless'] == 1): ?>
                              <li><i class="fa fa-empire"></i> Remote Keyless</li>
                           <?php endif; ?>
                           <?php if((int) $row['has_v6_cylinder'] == 1): ?>
                              <li><i class="fa fa-lock"></i> V 6 Cylinder</li>
                           <?php endif; ?>
                           <?php if((int) $row['has_rear_seat_dvd'] == 1): ?>
                              <li><i class="fa fa-desktop"></i> Rear-Seat DVD</li>
                           <?php endif; ?>
                           <?php if(!is_null($row['color'])): ?>
                              <li><i class="fa fa-key"></i> <?php echo $row['color']; ?></li>
                           <?php endif; ?>
                        </ul>
                     </div>

                           <p>
                        <br />
                              <button type="button" class="gauto-theme-btn" onclick="window.location.href='rent/secure/<?php echo $row['id']; ?>'">Rent Car</button>
                           </p>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <!-- Car Booking Area End -->
       
       

    <?php $this->load->view('footer'); echo "\n"; ?>

</body>

</html>