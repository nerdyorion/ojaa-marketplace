
<div class="page-header">
  <div class="container">
    <div class="row">
      <div class="col-md-12 text-center">
        <h2>New Loan Application</h2>
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?php echo $this->config->base_url(); ?>">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page" id="bread-current">New Loan Application</li>
          </ol>
        </nav>
      </div>
    </div>
  </div>
</div>
<section class="about-page-shortcode section-padding">
  <div class="container rowpadding-with-border">
    <div class="row">
      <?php $this->load->view('side-nav-menu'); echo "\n"; ?>
      <div class="col-lg-9">
        <div class="row">
          <div class="col-lg-12">
            <div class="about-content">
              <h2 id="header_caption">Start a New Loan Application</h2>

              <?php if($error_code == 0 && !empty($error)): ?>
                <div class="alert alert-success alert-dismissable fade show">
                  <a href="<?php echo current_url();?>#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  <strong>Success!</strong> <?php echo $error; ?>
                </div>
                <?php elseif($error_code == 1 && !empty($error)): ?>
                  <div class="alert alert-danger alert-dismissable fade show">
                    <a href="<?php echo current_url();?>#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>Error!</strong> <?php echo $error; ?>
                  </div>
                  <?php else: ?>
                  <?php endif; ?>
                  <?php echo form_open_multipart('/New-Loan-Application/secure', 'class="form-horizontal" onsubmit="return validate();"'); ?>
                  <div class="row list-input" id="new-loan-box">
                    <div class="col-md-12 mr0">
                      <div class="single-get-touch">
                        <label class="mb-10" for="loan_amount">Loan amount* </label>
                        <input id="loan_amount" name="loan_amount" class="web form-control" type="text" placeholder="" required="required" value="<?php echo $this->session->flashdata('loan_amount'); ?>" />
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="single-get-touch">
                        <label class="mb-10" for="loan_purpose">Purpose of loan* </label>
                        <input id="loan_purpose" name="loan_purpose" class="web form-control" type="text" placeholder="" required="required" value="<?php echo $this->session->flashdata('loan_purpose'); ?>" />
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="single-get-touch">
                        <label class="mb-10" for="loan_duration">Duration* </label>
                        <div class="box">
                          <select class="wide fancyselect mb-30" id="loan_duration" name="loan_duration" required="required">
                            <option value="" selected="selected">-- Select --</option>
                            <?php foreach ($loan_duration as $rec): ?>
                              <option value="<?php echo $rec['value']; ?>" <?php echo $this->session->flashdata('loan_duration') == $rec['value'] ? 'selected="selected"' : ''; ?>><?php echo $rec['value']; ?></option>
                            <?php endforeach; ?>
                          </select>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="single-get-file">
                        <label class="mb-10" for="stmt_of_acct_url">Stamped Bank statement of salary account / business account (for business owners)* </label>
                        <!-- <input type="hidden" name="stmt_of_acct_url_old" value="" /> -->
                        <input id="stmt_of_acct_url" name="stmt_of_acct_url" class="web form-control"type="file" required="required" value="" />
                        <br />
                        <!-- <?php // if(!empty($row['stmt_of_acct_url']) && file_exists($upload_save_path . $row['stmt_of_acct_url'])): ?>
                          <strong><a href="profile/download_statement">View / Download Statement</a></strong>
                          <br />
                        <?php // endif; ?> -->
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="single-get-file">
                        <label class="mb-10" for="emp_letter_url">Employment Letter / Certificate of Incorporation or Memorandum (for business owners) / Article of association (for artisans)* </label>
                        <input id="emp_letter_url" name="emp_letter_url" class="web form-control"type="file" required="required" value="" />
                        <br />
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="single-get-check">
                        <label class="radiobox" for="terms"> <a href="Terms-And-Conditions" target="_blank">Accept &amp; Read our Terms and Condition</a>
                          <input type="radio" id="terms" name="terms">
                          <span class="checkmark"></span>
                        </label>
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="single-get-touch">
                        <button type="button" name="apply-now-btn" id="apply-now-btn" class="btn btn-default btn-sm" data-loading-text="<i class='fa fa-spinner fa-spin'></i> <span>Apply Now</span> <i class='fa fa-check'></i>">Apply Now <i class="fa fa-check"></i></button>
                      </div>
                    </div>
                  </div>

                  <!-- Confirm Application -->
                  <!-- <div class="row list-input d-none" id="confirm-loan-box"> -->
                  <div class="row list-input" id="confirm-loan-box" style="display: none;">


                    <div class="col-md-12 mr0">
                      <div class="card">
                        <ul class="list-group list-group-flush">
                          <li class="list-group-item"><strong>Loan amount:</strong> <span id="loan_amount_cfm"></span></li>
                          <li class="list-group-item"><strong>Purpose of loan:</strong> <span id="loan_purpose_cfm"></span></li>
                          <li class="list-group-item"><strong>Duration:</strong> <span id="loan_duration_cfm"></span></li>
                          <li class="list-group-item"><strong>Bank statement of salary account / business account:</strong> <span id="stmt_of_acct_url_cfm"></span></li>
                          <li class="list-group-item"><strong>Employment Letter / Certificate of Incorporation or Memorandum / Article of association:</strong> <span id="emp_letter_url_cfm"></span></li>
                          <!-- <?php // if(!empty($row['stmt_of_acct_url']) && file_exists($upload_save_path . $row['stmt_of_acct_url'])): ?>
                          <li class="list-group-item"><strong>Bank statement of account:</strong> <strong><a href="profile/download_statement">View / Download Statement</a></strong></li>
                          
                          <br />
                        <?php // endif; ?> -->
                      </ul>
                    </div>
                  </div>
                  <div class="col-md-12 mt-2">
                    <div class="single-get-touch">
                      <button type="button" name="edit-btn" class="btn btn-default btn-sm" id="edit-btn">Edit <i class="fa fa-edit"></i></button>
                      <button type="submit" name="submit" id="submit-btn" class="btn btn-default btn-sm">Complete <i class="fa fa-save"></i></button>
                    </div>
                  </div>
                </div>
                  <!-- // end Confirm Application -->

                </form>


              </div>
            </div>
          </div>


        </div>
      </div>
    </div>
  </section>
  <!-- end of about section -->

  <?php $this->load->view('footer'); echo "\n"; ?>

  <script type="text/javascript">
    const numberWithCommas = (x) => {
    var parts = x.toString().split(".");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return parts.join(".");
  }

    function validate()
    {
      if($('#loan_amount').val() == '')
      {
        document.getElementById("loan_amount").setCustomValidity("Please fill out this field.");
        // document.getElementById("loan_amount").focus();
      }
      else if($('#loan_purpose').val() == '')
      {
        document.getElementById("loan_amount").setCustomValidity('');

        document.getElementById("loan_purpose").setCustomValidity("Please fill out this field.");
        // document.getElementById("loan_purpose").focus();
      }
      else if($('#loan_duration').val() == '')
      {
        document.getElementById("loan_amount").setCustomValidity('');
        document.getElementById("loan_purpose").setCustomValidity('');

        document.getElementById("loan_duration").setCustomValidity("Please fill out this field.");
        // document.getElementById("loan_duration").focus();
      }
      else if($('#stmt_of_acct_url').val() == '')
      {
        document.getElementById("loan_amount").setCustomValidity('');
        document.getElementById("loan_purpose").setCustomValidity('');
        document.getElementById("loan_duration").setCustomValidity('');

        document.getElementById("stmt_of_acct_url").setCustomValidity("Please fill out this field.");
        // document.getElementById("stmt_of_acct_url").focus();
      }
      else if($('#emp_letter_url').val() == '')
      {
        document.getElementById("loan_amount").setCustomValidity('');
        document.getElementById("loan_purpose").setCustomValidity('');
        document.getElementById("loan_duration").setCustomValidity('');
        document.getElementById("stmt_of_acct_url").setCustomValidity('');

        document.getElementById("emp_letter_url").setCustomValidity("Please fill out this field.");
        // document.getElementById("emp_letter_url").focus();
      }
      else if(document.getElementById("terms").checked == false)
      {
        document.getElementById("loan_amount").setCustomValidity('');
        document.getElementById("loan_purpose").setCustomValidity('');
        document.getElementById("loan_duration").setCustomValidity('');
        document.getElementById("stmt_of_acct_url").setCustomValidity('');
        document.getElementById("emp_letter_url").setCustomValidity('');

        document.getElementById("terms").setCustomValidity("Please fill out this field.");
        alert('You MUST accept our Terms and Conditions in order to apply for a loan.');
        // return false;
      }
      else {
        document.getElementById("loan_amount").setCustomValidity('');
        document.getElementById("loan_purpose").setCustomValidity('');
        document.getElementById("loan_duration").setCustomValidity('');
        document.getElementById("stmt_of_acct_url").setCustomValidity('');
        document.getElementById("emp_letter_url").setCustomValidity('');
        document.getElementById("terms").setCustomValidity('');

        $('#new-loan-box').hide();
        $('#header_caption').text('Confirm Loan Application');

        // set values
        $('#loan_amount_cfm').text('NGN' + numberWithCommas($('#loan_amount').val()));
        $('#loan_purpose_cfm').text($('#loan_purpose').val());
        $('#loan_duration_cfm').text($('#loan_duration').val());
        $('#stmt_of_acct_url_cfm').text($('#stmt_of_acct_url').val());
        $('#emp_letter_url_cfm').text($('#emp_letter_url').val());

        $('#confirm-loan-box').show();

        $([document.documentElement, document.body]).animate({
          scrollTop: $("#bread-current").offset().top
        }, 1000);



      }
    }
    $(document).ready(function(){
      $(window).keydown(function(event){
        if(event.keyCode == 13) {
          event.preventDefault();
          return false;
        }
      });

      $('#apply-now-btn').click(function(){
        validate();
      });

      $('#edit-btn').click(function(){
        // $("#new-loan-box").animate({width:'toggle'},-350);
        // $("#new-loan-box").show("slide", { direction: "right" }, 1000);
        // $('#confirm-loan-box').addClass('d-none');
        $('#confirm-loan-box').hide();
        $('#header_caption').text('Start a New Loan Application');
        $('#new-loan-box').show();
      });

      $('#submit-btn').click(function(){
        var $btn = $('button[type="submit"]').button('loading');
        $(':input[type="submit"]').prop('disabled', true);
        $('button[type="submit"]').prop('disabled', true);
        return true;
      });



    });
  </script>

</body>

</html>