
<?php if(is_logged_in()): ?>
  <div class="col-lg-3">
    <div class="sidebar-widgets-wrap">
      <div class="sidebar-widget mb-40">
        <h5 class="mb-20">Menu</h5>
        <div class="widget-link navbar-collapse collpase">
          <ul class="nav navbar-nav navbar-left">
            <li> <a href="User-Home"> <i class="fa fa-angle-double-right"></i> Dashboard</a></li>

            <li> <a href="bookings"> <i class="fa fa-angle-double-right"></i> Bookings</a></li>
            <li> <a href="profile"> <i class="fa fa-angle-double-right"></i> Profile</a></li>
            <li> <a href="Change-Password"> <i class="fa fa-angle-double-right"></i> Change Password</a></li>
            <li> <a href="logout"> <i class="fa fa-angle-double-right"></i> Logout</a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <?php endif; ?>