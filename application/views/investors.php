
<div class="page-header">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h2>Investors</h2>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo $this->config->base_url(); ?>">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Investors</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- start our expert section -->
<section class="team-section section-padding">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="section-title">
                    <h2>Investors</h2>
                </div>
            </div>

            <div class="col-md-6">
                <div class="get-in-touch">
                    <p>
                        <strong>FUNDING</strong>
                        <ul>
                            <li>Invest in Xtraloans4u and earn up to 40% interest p.a.</li>
                            <li></li>
                        </ul>
                    </p>
                </div>
            </div>

            <div class="col-md-6">
                <div class="get-in-touch">
                    <p>
                        <strong>BENEFITS</strong>
                        <ul>
                            <li>Interest rates are competitive</li>
                            <li>Flexible loan periods and rollovers</li>
                            <li>Flexible interest payment period (upfront, monthly and at maturity)</li>
                            <li>Payments at maturity made within 24hours</li>
                        </ul>
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- end our expert section -->
    <?php $this->load->view('footer'); echo "\n"; ?>

</body>

</html>