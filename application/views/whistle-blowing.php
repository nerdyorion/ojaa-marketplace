
<div class="page-header">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h2>Whistle Blowing</h2>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index-2.html">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Whistle Blowing</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- end of page header -->
<!-- start faq question -->
<div class="faq-question-area section-padding">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="faq-single-title">Whistle Blowing Policy</h2>

                <p><strong><?php echo $this->config->item('app_name'); ?></strong> MFB promotes transparency, integrity and accountability in all its operations. This is why we have developed a Whistle Blowing policy to protect both you and <strong><?php echo $this->config->item('app_name'); ?></strong>. Prospects, customers and employees are encouraged to bring to the attention of <strong><?php echo $this->config->item('app_name'); ?></strong> any event, concerns, act or omission that they reasonably believe could impact negatively on the wellbeing of <strong><?php echo $this->config->item('app_name'); ?></strong>, any of its stakeholders and the general public by sending an email to report@xtraloans4u. All emails are treated with strict confidentiality and identities of senders protected.</p>

            </div>
        </div>
    </div>
</div>
<!-- end of faq question -->

<?php $this->load->view('footer'); echo "\n"; ?>

</body>

</html>