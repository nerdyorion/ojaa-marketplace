
      <!-- Breadcromb Area Start -->
      <section class="gauto-breadcromb-area section_70">
         <div class="container">
            <div class="row">
               <div class="col-md-12">
                  <div class="breadcromb-box">
                     <h3>Car Listing</h3>
                     <ul>
                        <li><i class="fa fa-home"></i></li>
                        <li><a href="<?php echo $this->config->base_url(); ?>">Home</a></li>
                        <li><i class="fa fa-angle-right"></i></li>
                        <li>Car Listing</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <!-- Breadcromb Area End -->
       
       
      <!-- Car Listing Area Start -->
      <section class="gauto-car-listing section_70">
         <div class="container">
            <div class="row">
               <div class="col-lg-4">
                  <div class="car-list-left">
                     <div class="sidebar-widget">
          <?php if($error_code == 0 && !empty($error)): ?>

            <div class="alert alert-success alert-dismissable fade show" role="alert">
              <strong>Success!</strong>
              <?php echo $error; ?>
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <?php elseif($error_code == 1 && !empty($error)): ?>
              <div class="alert alert-danger alert-dismissable fade show" role="alert">
                <strong>Error!</strong>
                <?php echo $error; ?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <?php else: ?>
              <?php endif; ?>
                        <?php echo form_open('cars/index', 'method="get"'); ?>
                           <p>
                              <select name="brand">
                                 <option value="">Brand</option>
                                 <?php foreach($brands_sidenav as $row): ?>
                                    <option value="cars?brand=<?php echo urlencode($row['name']); ?>" <?php echo isset($_GET['brand']) && !empty($_GET['brand']) && urldecode($_GET['brand']) == $row['name'] ? 'selected="selected"' : '' ?>><?php echo $row['name']; ?></option>
                                 <?php endforeach; ?>
                              </select>
                           </p>
                           <!-- <p>
                              <select>
                                 <option value="">AC</option>
                                 <option>AC Car</option>
                                 <option>Non-AC Car</option>
                              </select>
                           </p> -->
                           <p>
                              <input id="pickup_date" name="pickup_date" placeholder="Pickup Date" data-select="datepicker" type="text" value="<?php echo isset($_GET['pickup_date']) && !empty($_GET['pickup_date']) ? trim($_GET['pickup_date']) : '' ?>" autocomplete="off">
                           </p>
                           <p>
                              <input id="return_date" name="return_date" placeholder="Return Date" data-select="datepicker" type="text" value="<?php echo isset($_GET['return_date']) && !empty($_GET['return_date']) ? trim($_GET['return_date']) : '' ?>" autocomplete="off">
                           </p>
                           <p>
                              <button type="submit" class="gauto-theme-btn">Find Car</button>
                           </p>
                        </form>
                     </div>
                     <div class="sidebar-widget">
                        <ul class="service-menu">
                           <li <?php echo !isset($_GET['brand']) ? 'class="active"' : '' ?>>
                              <a href="cars">All Brands</a>
                              <!-- <a href="#">All Brands<span>(2376)</span></a> -->
                           </li>
                           <?php foreach($brands_sidenav as $row): ?>
                              <li <?php echo isset($_GET['brand']) && !empty($_GET['brand']) && urldecode($_GET['brand']) == $row['name'] ? 'class="active"' : '' ?> ><a href="cars?brand=<?php echo urlencode($row['name']); ?>"><?php echo $row['name']; ?></a></li>
                           <?php endforeach; ?>
                        </ul>
                     </div>
                     <div class="sidebar-widget">
                        <h3>Featured</h3>
                        <ul class="top-products">
                           <?php foreach($featured_sidenav as $row): ?>
                           <li>
                              <div class="recent-img">
                                 <a href="cars/view/<?php echo $row['id']; ?>">
                                 <img src="<?php echo $this->config->base_url(); ?>assets/img/cars/<?php echo image_thumb($row['image_url']); ?>" alt="<?php echo $row['name']; ?>">
                                 </a>
                              </div>
                              <div class="recent-text">
                                 <h4>
                                    <a href="cars/view/<?php echo $row['id']; ?>"><?php echo $row['name']; ?></a>
                                 </h4>
                                 <p>$<?php echo number_format($row['price_per_day']); ?><span>/ Day</span></p>
                              </div>
                           </li>
                           <?php endforeach; ?>
                        </ul>
                     </div>
                  </div>
               </div>
               <div class="col-lg-8">
                  <div class="car-listing-right">
                     <div class="property-page-heading">
                        <!-- <div class="propertu-page-head">
                           <ul>
                              <li class="active"><a href="#"><i class="fa fa-th-list"></i></a></li>
                              <li><a href="#"><i class="fa fa-th-large"></i></a></li>
                           </ul>
                        </div>
                        <div class="paging_status">
                           <p>1-10 of 25 results</p>
                        </div> -->
                        <div class="propertu-page-shortby">
                           <label><i class="fa fa-sort-amount-asc"></i>Sort By</label>
                           <select class="chosen-select-no-single" name="sort">
                              <option value="">Default</option>
                              <option value="1">Price (Low to High)</option>
                              <option value="2">Price (High to Low)</option>
                              <option value="3">Featured</option>
                           </select>
                        </div>
                     </div>
                     <div class="car-grid-list">

                     <?php $chunks = array_chunk($rows, 2); ?>

                     <?php foreach($chunks as $chunk): ?>

                        <div class="row">

                           <?php foreach($chunk as $key => $row): ?>

                           <div class="col-md-6">
                              <div class="single-offers">
                                 <div class="offer-image">
                                    <a href="cars/view/<?php echo $row['id']; ?>">
                                    <img src="<?php echo $this->config->base_url(); ?>assets/img/cars/<?php echo image_thumb($row['image_url']); ?>" alt="<?php echo $row['name']; ?>" />
                                    </a>
                                 </div>
                                 <div class="offer-text">
                                    <a href="cars/view/<?php echo $row['id']; ?>">
                                       <h3><?php echo $row['name']; ?></h3>
                                    </a>
                                    <h4>$<?php echo number_format($row['price_per_day']); ?><span>/ Day</span></h4>
                                    <ul>
                                       <li><i class="fa fa-car"></i>Model:<?php echo $row['model_year']; ?></li>
                                       <li><i class="fa fa-cogs"></i><?php echo $transmissions[getArrayKey($transmissions, "id", $row['transmission'])]['name']; ?></li>
                                       <li><i class="fa fa-dashboard"></i><?php echo number_format($row['kmph']); ?>kmph</li>
                                    </ul>
                                    <div class="offer-action">
                                       <a href="rent/secure/<?php echo $row['id']; ?>" class="offer-btn-1">Rent Car</a>
                                       <a href="cars/view/<?php echo $row['id']; ?>" class="offer-btn-2">Details</a>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <?php endforeach; ?>

                        </div>
                        <?php endforeach; ?>

                     </div>

                     <!-- <div class="pagination">
                       <p><?php echo $links; ?></p>
                     </div> -->

                     <div class="pagination-box-row">
                        <!-- <p>Page 1 of 6</p> -->
                        <ul class="pagination">

                           <?php echo $links; ?>
                           <!-- <li class="active"><a href="#">1</a></li>
                           <li><a href="#">2</a></li>
                           <li><a href="#">3</a></li>
                           <li>...</li>
                           <li><a href="#">6</a></li>
                           <li><a href="#"><i class="fa fa-angle-double-right"></i></a></li> -->
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <!-- Car Listing Area End -->
       
       

    <?php $this->load->view('footer'); echo "\n"; ?>

</body>

</html>