
    <div class="page-header">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h2>Team</h2>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?php echo $this->config->base_url(); ?>">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Team</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- end of page header -->
    <!-- start our expert section -->
    <section class="team-section section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <div class="section-title">
                        <h2>Our Experts</h2>
                        <p>The passages of Lorem Ipsum available but the majority have suffered alteration embarrased</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3 col-md-6">
                    <div class="single-team">
                        <div class="team-thumb">
                            <img src="assets/images/team-2.jpg" alt="">
                            <div class="team-hover">
                                <div class="display-table">
                                    <div class="display-cell">
                                        <div class="team-hover-content">
                                            <div class="team-link">
                                                <a href="#"><i class="fa fa-facebook-f"></i></a>
                                                <a href="#"><i class="fa fa-twitter"></i></a>
                                                <a href="#"><i class="fa fa-pinterest-p"></i></a>
                                            </div>
                                            <p>Tested and Trusted Professional in Finiancial Services</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="team-content text-center">
                            <h4>Stuart Binny</h4>
                            <p>Loan Consultant</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="single-team">
                        <div class="team-thumb">
                            <img src="assets/images/team-4.jpg" alt="">
                            <div class="team-hover">
                                <div class="display-table">
                                    <div class="display-cell">
                                        <div class="team-hover-content">
                                            <div class="team-link">
                                                <a href="#"><i class="fa fa-facebook-f"></i></a>
                                                <a href="#"><i class="fa fa-twitter"></i></a>
                                                <a href="#"><i class="fa fa-pinterest-p"></i></a>
                                            </div>
                                            <p>Tested and Trusted Professional in Finiancial Services</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="team-content text-center">
                            <h4>Stuart Binny</h4>
                            <p>Loan Consultant</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="single-team">
                        <div class="team-thumb">
                            <img src="assets/images/team-2.jpg" alt="">
                            <div class="team-hover">
                                <div class="display-table">
                                    <div class="display-cell">
                                        <div class="team-hover-content">
                                            <div class="team-link">
                                                <a href="#"><i class="fa fa-facebook-f"></i></a>
                                                <a href="#"><i class="fa fa-twitter"></i></a>
                                                <a href="#"><i class="fa fa-pinterest-p"></i></a>
                                            </div>
                                            <p>Tested and Trusted Professional in Finiancial Services</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="team-content text-center">
                            <h4>Stuart Binny</h4>
                            <p>Loan Consultant</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="single-team">
                        <div class="team-thumb">
                            <img src="assets/images/team-4.jpg" alt="">
                            <div class="team-hover">
                                <div class="display-table">
                                    <div class="display-cell">
                                        <div class="team-hover-content">
                                            <div class="team-link">
                                                <a href="#"><i class="fa fa-facebook-f"></i></a>
                                                <a href="#"><i class="fa fa-twitter"></i></a>
                                                <a href="#"><i class="fa fa-pinterest-p"></i></a>
                                            </div>
                                            <p>Tested and Trusted Professional in Finiancial Services</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="team-content text-center">
                            <h4>Stuart Binny</h4>
                            <p>Loan Consultant</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="single-team">
                        <div class="team-thumb">
                            <img src="assets/images/team-2.jpg" alt="">
                            <div class="team-hover">
                                <div class="display-table">
                                    <div class="display-cell">
                                        <div class="team-hover-content">
                                            <div class="team-link">
                                                <a href="#"><i class="fa fa-facebook-f"></i></a>
                                                <a href="#"><i class="fa fa-twitter"></i></a>
                                                <a href="#"><i class="fa fa-pinterest-p"></i></a>
                                            </div>
                                            <p>Tested and Trusted Professional in Finiancial Services</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="team-content text-center">
                            <h4>Martin Joe</h4>
                            <p>Branch Lead</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="single-team">
                        <div class="team-thumb">
                            <img src="assets/images/team-4.jpg" alt="">
                            <div class="team-hover">
                                <div class="display-table">
                                    <div class="display-cell">
                                        <div class="team-hover-content">
                                            <div class="team-link">
                                                <a href="#"><i class="fa fa-facebook-f"></i></a>
                                                <a href="#"><i class="fa fa-twitter"></i></a>
                                                <a href="#"><i class="fa fa-pinterest-p"></i></a>
                                            </div>
                                            <p>Tested and Trusted Professional in Finiancial Services</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="team-content text-center">
                            <h4>Maline</h4>
                            <p>Credit Card Operations</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="single-team">
                        <div class="team-thumb">
                            <img src="assets/images/team-2.jpg" alt="">
                            <div class="team-hover">
                                <div class="display-table">
                                    <div class="display-cell">
                                        <div class="team-hover-content">
                                            <div class="team-link">
                                                <a href="#"><i class="fa fa-facebook-f"></i></a>
                                                <a href="#"><i class="fa fa-twitter"></i></a>
                                                <a href="#"><i class="fa fa-pinterest-p"></i></a>
                                            </div>
                                            <p>Tested and Trusted Professional in Finiancial Services</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="team-content text-center">
                            <h4>Shine D’zoza</h4>
                            <p>Founder</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="single-team">
                        <div class="team-thumb">
                            <img src="assets/images/team-4.jpg" alt="">
                            <div class="team-hover">
                                <div class="display-table">
                                    <div class="display-cell">
                                        <div class="team-hover-content">
                                            <div class="team-link">
                                                <a href="#"><i class="fa fa-facebook-f"></i></a>
                                                <a href="#"><i class="fa fa-twitter"></i></a>
                                                <a href="#"><i class="fa fa-pinterest-p"></i></a>
                                            </div>
                                            <p>Tested and Trusted Professional in Finiancial Services</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="team-content text-center">
                            <h4>Alex Finch</h4>
                            <p>Home Loan Consult</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- end of our expert section -->

<?php $this->load->view('footer'); echo "\n"; ?>

</body>

</html>