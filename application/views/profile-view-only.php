
<div class="page-header">
  <div class="container">
    <div class="row">
      <div class="col-md-12 text-center">
        <h2>Profile</h2>
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?php echo $this->config->base_url(); ?>">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Profile</li>
          </ol>
        </nav>
      </div>
    </div>
  </div>
</div>
<section class="about-page-shortcode section-padding">
  <div class="container rowpadding-with-border">
    <div class="row">
      <?php $this->load->view('side-nav-menu'); echo "\n"; ?>
      <div class="col-lg-9">
        <div class="row">
          <div class="col-lg-12">
            <div class="about-content">
              <h2>Profile</h2>

              <?php if($error_code == 0 && !empty($error)): ?>
                <div class="alert alert-success alert-dismissable fade show">
                  <a href="<?php echo current_url();?>#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  <strong>Success!</strong> <?php echo $error; ?>
                </div>
                <?php elseif($error_code == 1 && !empty($error)): ?>
                  <div class="alert alert-danger alert-dismissable fade show">
                    <a href="<?php echo current_url();?>#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>Error!</strong> <?php echo $error; ?>
                  </div>
                  <?php else: ?>
                  <?php endif; ?>
                  <div class="row list-input">


                    <div class="col-md-12 mr0">
                      <div class="card">
                        <div class="card-header">
                          <strong><?php echo $row['full_name']; ?></strong>
                        </div>
                        <ul class="list-group list-group-flush">
                          <li class="list-group-item"><strong>Gender:</strong> <?php echo $row['gender']; ?></li>
                          <li class="list-group-item"><strong>Phone:</strong> <?php echo $row['phone']; ?></li>
                          <li class="list-group-item"><strong>Date of Birth:</strong> <?php echo date('M d, Y', strtotime($row['dob'])); ?></li>
                          <li class="list-group-item"><strong>Address:</strong> <?php echo $row['address']; ?></li>
                          <li class="list-group-item"><strong>State:</strong> <?php echo $row['state_name']; ?></li>
                          <li class="list-group-item"><strong>Profession:</strong> <?php echo $row['profession']; ?></li>
                          <li class="list-group-item"><strong>Work Sector:</strong> <?php echo $row['work_sector']; ?></li>
                          <li class="list-group-item"><strong>Place of work:</strong> <?php echo $row['place_of_work']; ?></li>
                          <li class="list-group-item"><strong>BVN:</strong> <?php echo $row['bvn']; ?></li>
                          <li class="list-group-item"><strong>Bank:</strong> <?php echo $row['bank_name']; ?></li>
                          <li class="list-group-item"><strong>Account name:</strong> <?php echo $row['bank_acct_name']; ?></li>
                          <li class="list-group-item"><strong>Account no.:</strong> <?php echo $row['bank_acct_no']; ?></li>
                          <?php if(!empty($current_card_str)): ?>
                            <li class="list-group-item"><strong>Card:</strong> <mark><?php echo $current_card_str; ?></mark></li>
                          <?php endif; ?>
                        </ul>
                      </div>
                    </div>
                    <div class="col-md-12 mt-2">
                      <div class="single-get-touch">
                        <button type="submit" name="submit" class="btn btn-default btn-sm" onclick="window.location='profile';">Edit Profile <i class="fa fa-edit"></i></button>
                      </div>
                    </div>
                  </div>


                </div>
              </div>
            </div>


          </div>
        </div>
      </div>
    </section>
    <!-- end of about section -->

    <?php $this->load->view('footer'); echo "\n"; ?>

    <script type="text/javascript">
      function validate()
      {

        var $btn = $('button[type="submit"]').button('loading');
        $(':input[type="submit"]').prop('disabled', true);
        $('button[type="submit"]').prop('disabled', true);
        return true;
      }
    </script>

  </body>

  </html>