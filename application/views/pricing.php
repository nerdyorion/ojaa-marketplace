
<!-- BANNER -->
<div class="section banner-page" data-background="<?php echo $this->config->base_url(); ?>assets/images/bg1-min.jpg">
    <div class="content-wrap pos-relative">
        <div class="container">
            <div class="col-12 col-md-12">
                <div class="d-flex bd-highlight mb-2">
                    <div class="title-page">Pricing</div>
                </div>
                <nav aria-label="breadcrumb">
                  <ol class="breadcrumb ">
                    <li class="breadcrumb-item"><a href="./">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Pricing</li>
                </ol>
            </nav>
        </div>
    </div>

</div>
</div>

<!-- CONTENT -->
<div class="section">
    <div class="content-wrap">
        <div class="container">
            
            <div class="row">
            <!-- <h4>Subscribe 10k for 6 months or 18k for one year for Electrical repair and installation in your house with option of rollover for one year if there is no electrical repair/fault in your house. We don't do NEPA jobs and electronic work; your subscription is strictly on electrical, it doesn't cover when you are moving from one apartment to another and it doesn't cover when you are renovating the apartment you are living</h4> -->
            <h4>Subscribe 10k for 6 months or 18k for one year for Electrical repair and installation in your house with option of rollover for one year if there is no electrical repair/fault in your house. Your subscription is strictly on electrical.</h4>
            <br />
            </div>

            <div class="row mt-3">

                <?php foreach($plans as $plan): ?>
                <!-- Item <?php echo $plan['id']; ?> -->
                    <div class="col-sm-12 col-md-12 col-lg-4">
                        <div class="rs-pricing-1 mb-2 <?php echo $plan['popular'] ? 'popular' : ''; ?>">
                            <h4 class="title"><?php echo $plan['name']; ?></h4>
                            <div class="price">
                                <?php if($plan['fee_ng'] == 0): ?>
                                    <span>/<?php echo $plan['duration']; ?>mo</span>
                                <?php else: ?>
                                    <span>N</span><?php echo number_format($plan['fee_ng']); ?> <span>/<?php echo $plan['duration']; ?>mo</span>
                                <?php endif; ?>
                            </div>
                            <div class="features">
                                <ul>
                                    <?php foreach($plan['features'] as $feature): ?>
                                        <li><?php echo $feature; ?></li>
                                    <?php endforeach; ?>
                                </ul>
                            </div>
                            <div class="action">
                                <a href="register?plan=<?php echo $plan['url']; ?>" class="btn <?php echo (int) $plan['id'] % 2 == 0 ? 'btn-primary' : 'btn-secondary' ; ?>">Subscribe Now</a>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>

            </div>

        </div>
    </div>
</div>


<?php $this->load->view('footer'); echo "\n"; ?>

</body>

</html>