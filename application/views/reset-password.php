
        <section class="breadcrumb_area">
            <img class="breadcrumb_shap" src="<?php echo $this->config->base_url(); ?>assets/img/breadcrumb/banner_bg.png" alt="">
            <div class="container">
                <div class="breadcrumb_content text-center">
                    <h1 class="f_p f_700 f_size_50 w_color l_height50 mb_20">Reset Password</h1>
                    <p class="f_400 w_color f_size_16 l_height26">Why I say old chap that is spiffing off his nut arse pear shaped plastered<br> Jeffrey bodge barney some dodgy.!!</p>
                </div>
            </div>
        </section>

        <section class="sign_in_area bg_color sec_pad">
            <div class="container">
                <div class="sign_info">
                    <div class="row">
                        <div class="col-lg-5">
                            <div class="sign_info_content">
                                <h3 class="f_p f_600 f_size_24 t_color3 mb_40">First time here?</h3>
                                <h2 class="f_p f_400 f_size_30 mb-30">Join now and get<br> <span class="f_700">Free Trial</span> for all <br> products</h2>
                                <ul class="list-unstyled mb-0">
                                    <li><i class="ti-check"></i> Premium Access to all Products</li>
                                    <li><i class="ti-check"></i> Free Testing Tools</li>
                                    <li><i class="ti-check"></i> Unlimited User Accounts</li>
                                </ul>
                                <a href="login" class="btn_three sign_btn_transparent">Sign Up</a>
                            </div>
                        </div>
                        <div class="col-lg-7">
                            <div class="login_info">
                                <h2 class="f_p f_600 f_size_24 t_color3 mb_40">Set your new password</h2>
                        <?php if($error_code == 0 && !empty($error)): ?>

                            <div class="alert alert-success alert-dismissable fade show" role="alert">
                                <strong>Success!</strong> <?php echo $error; ?>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <?php elseif($error_code == 1 && !empty($error)): ?>
                                <div class="alert alert-danger alert-dismissable fade show" role="alert">
                                    <strong>Error!</strong> <?php echo $error; ?>
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <?php else: ?>
                                <?php endif; ?>
                                <?php echo form_open('Reset-Password/token/' . $token, 'onsubmit="return validate();" class="login-form sign-in-form"'); ?>

              <div class="form-group text_box">
                                        <label class="f_p text_c f_400">Password</label>
                <input type="password" name="password" id="password" placeholder="******" required="required" aria-required="true" aria-invalid="false">
                <i class="fa fa-lock"></i>
              </div>
              <div class="form-group text_box">
                                        <label class="f_p text_c f_400">Confirm Password</label>
                <input type="password" name="passconf" id="passconf" placeholder="******" required="required" aria-required="true" aria-invalid="false">
                <i class="fa fa-lock"></i>
              </div>
                                    <div class="extra mb_20">
                                        <div class="forgotten-password">
                                            <a href="login">Login</a>
                                        </div>
                                    </div>
                                    <div class="d-flex justify-content-between align-items-center">
                                        <button type="submit" class="btn_three">Submit</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>


  <?php $this->load->view('footer'); echo "\n"; ?>


  <script>

    function validate()
    {
      $(':input[type="submit"]').prop('disabled', true);
      $('button[type="submit"]').prop('disabled', true);
      $('#submit').html('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>');
      return true;
    }

  </script>

</body>

</html>