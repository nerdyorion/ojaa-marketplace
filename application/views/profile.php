
<section class="about-page-shortcode section-padding mt-3" style="margin-bottom: 100px;">
  <div class="container rowpadding-with-border">
    <div class="row">
      <?php $this->load->view('side-nav-menu'); echo "\n"; ?>
      <div class="col-lg-9">
        <div class="row">
          <div class="col-lg-12">
            <div class="about-content">
              <h2>Update Profile</h2>
              <hr />

              <!-- <nav aria-label="breadcrumb">
                <ol class="breadcrumb" style="margin-left:0;">
                  <li class="breadcrumb-item"><a href="<?php echo $this->config->base_url(); ?>dashboard">Home</a></li>
                  <li class="breadcrumb-item active" aria-current="page" id="bread-current">Profile</li>
                </ol>
              </nav> -->

              <?php if((int) $row['profile_complete'] == 0): ?>
              <div class="alert alert-info alert-dismissable fade show" role="alert">
                <strong>Info!</strong> Please complete your profile to enable verification and start renting.
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <?php endif; ?>


              <?php if($error_code == 0 && !empty($error)): ?>

                <div class="alert alert-success alert-dismissable fade show" role="alert">
                  <strong>Success!</strong> <?php echo $error; ?>
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <?php elseif($error_code == 1 && !empty($error)): ?>
                  <div class="alert alert-danger alert-dismissable fade show" role="alert">
                    <strong>Error!</strong> <?php echo $error; ?>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <?php else: ?>
                  <?php endif; ?>
                  <?php echo form_open_multipart('/profile', 'class="form-horizontal" onsubmit="return validate();"'); ?>
                  <div class="row list-input">
                    <div class="col-md-4">
                      <div class="form-group">
                        <label class="mb-10" for="first_name">First name <span class="required">*</span> </label>
                        <input id="first_name" name="first_name" class="web form-control" type="text" placeholder="" required="required" value="<?php echo $row['first_name']; ?>" aria-required="true" aria-invalid="false" />
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label class="mb-10" for="last_name">Last name <span class="required">*</span> </label>
                        <input id="last_name" name="last_name" class="web form-control" type="text" placeholder="" required="required" value="<?php echo $row['last_name']; ?>" aria-required="true" aria-invalid="false" />
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label class="mb-10">Gender <span class="required">*</span> </label>
                        <div class="box">
                          <select class="form-control" id="gender" name="gender" required="required" aria-required="true" aria-invalid="false">
                            <option value="">-- Select --</option>
                            <option value="Male" <?php echo strtolower($row['gender']) == "male" ? 'selected="selected"' : ''; ?>>Male</option>
                            <option value="Female" <?php echo strtolower($row['gender']) == "female" ? 'selected="selected"' : ''; ?>>Female</option>
                          </select>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label class="mb-10">Country of Residence <span class="required">*</span> </label>
                        <div class="box">
                        <select class="web form-control" id="country_id" name="country_id" required="required" aria-required="true" aria-invalid="false">
                          <option value="" selected="selected">-- Select --</option>
                          <?php foreach ($countries as $country): ?>
                            <option value="<?php echo $country['id']; ?>" <?php echo $country['id'] == $row['country_id'] ? 'selected="selected"' : ''; ?>><?php echo $country['name']; ?></option>
                          <?php endforeach; ?>
                        </select>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label class="mb-10" for="dob">Date of Birth <span class="required">*</span> </label>
                        <input id="dob" name="dob" class="web form-control" type="date" value="<?php echo $row['dob']; ?>" required="required" aria-required="true" aria-invalid="false" />
                        <!-- <input id="dob" name="dob" class="web form-control" type="text" value="<?php echo $row['dob']; ?>" placeholder="" /> -->
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label class="mb-10" for="phone">Phone <span class="required">*</span> </label>
                        <input id="phone" name="phone" class="web form-control" type="text" placeholder="" value="<?php echo $row['phone']; ?>" required="required" aria-required="true" aria-invalid="false" />
                      </div>
                    </div>

                    <div class="col-md-3">
                      <div class="form-group">
                        <label class="mb-10" for="id_card_url">ID Card <span class="required">*</span> </label>
                        <input id="id_card_url" name="id_card_url" class="web form-control" type="file" <?php echo !empty($row['id_card_url']) && file_exists($upload_save_path . $row['id_card_url']) ? '' : 'required="required" aria-required="true"' ?> aria-invalid="false" />
                        <br />
                        <?php if(!empty($row['id_card_url']) && file_exists($upload_save_path . $row['id_card_url'])): ?>
                          <strong><a href="profile/id_card">View / Download</a></strong>
                        <?php endif; ?>
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="form-group">
                        <label class="mb-10" for="drivers_license_url">Driver's License <span class="required">*</span> </label>
                        <input id="drivers_license_url" name="drivers_license_url" class="web form-control" type="file" <?php echo !empty($row['drivers_license_url']) && file_exists($upload_save_path . $row['drivers_license_url']) ? '' : 'required="required" aria-required="true"' ?> aria-invalid="false" />
                        <br />
                        <?php if(!empty($row['drivers_license_url']) && file_exists($upload_save_path . $row['drivers_license_url'])): ?>
                          <strong><a href="profile/drivers_license">View / Download</a></strong>
                        <?php endif; ?>
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="form-group">
                        <label class="mb-10" for="proof_of_residence_url">Proof of Residence <span class="required">*</span> </label>
                        <input id="proof_of_residence_url" name="proof_of_residence_url" class="web form-control" type="file" <?php echo !empty($row['proof_of_residence_url']) && file_exists($upload_save_path . $row['proof_of_residence_url']) ? '' : 'required="required" aria-required="true"' ?> aria-invalid="false" />
                        <br />
                        <?php if(!empty($row['proof_of_residence_url']) && file_exists($upload_save_path . $row['proof_of_residence_url'])): ?>
                          <strong><a href="profile/proof_of_residence">View / Download</a></strong>
                        <?php endif; ?>
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="form-group">
                        <label class="mb-10" for="international_passport_url">Intl. Passport <span class="required">*</span> </label>
                        <input id="international_passport_url" name="international_passport_url" class="web form-control" type="file" <?php echo !empty($row['international_passport_url']) && file_exists($upload_save_path . $row['international_passport_url']) ? '' : 'required="required" aria-required="true"' ?> aria-invalid="false" />
                        <br />
                        <?php if(!empty($row['international_passport_url']) && file_exists($upload_save_path . $row['international_passport_url'])): ?>
                          <strong><a href="profile/international_passport">View / Download</a></strong>
                        <?php endif; ?>
                      </div>
                    </div>

                    <div class="col-md-12">
                      <div class="form-group">
                        <label class="mb-10" for="student_no">Student ID <span class="required">*</span> </label>
                        <input id="student_no" name="student_no" class="web form-control" type="text" placeholder="" value="<?php echo $row['student_no']; ?>" required="required" aria-required="true" aria-invalid="false" />
                      </div>
                    </div>

                    <div class="col-md-12">
                      <div class="form-group">
                        <label class="mb-10" for="address">Address </label>
                        <textarea class="web form-control" name="address" id="address" maxlength="8000" required="required"><?php echo $row['address']; ?></textarea>
                      </div>
                    </div>

                    <div class="col-md-12 mt-4">
                      <div class="form-group">
                        <button type="submit" name="submit" id="submitt" class="btn btn-default btn-sm">Update Profile <i class="fa fa-check"></i></button>
                      </div>
                    </div>
                  </div>
                </form>


              </div>
            </div>
          </div>


        </div>
      </div>
    </div>
  </section>
  <!-- end of about section -->

  <?php $this->load->view('footer'); echo "\n"; ?>

  <script type="text/javascript">
    function validate()
    {
      var $btn = $('button[type="submit"]').button('loading');
      $(':input[type="submit"]').prop('disabled', true);
      $('button[type="submit"]').prop('disabled', true);
      $('#submit').html('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>');
      return true;
    }
  </script>

</body>

</html>