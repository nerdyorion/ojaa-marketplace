
<body class="color-custom style-default layout-full-width no-content-padding header-classic sticky-header sticky-tb-color ab-show subheader-both-center menu-link-color menuo-right mobile-tb-hide mobile-side-slide mobile-mini-mr-ll">
    <div id="Wrapper">
        <div id="Header_wrapper">
            <header id="Header">
        <?php $this->load->view('menu'); echo "\n"; ?>
            </header>
            <div id="Subheader">
                <div class="container">
                    <div class="column one">
                        <h1 class="title">Manifesto</h1>
                        <ul class="breadcrumbs no-link">
                            <li>
                                <a href="./">Home</a><span><i class="icon-right-open"></i></span>
                            </li>
                            <li>
                                <a href="manifesto">Manifesto</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div id="Content">
            <div class="content_wrapper clearfix">
                <div class="sections_group">
                    <div class="entry-content">
                        <div class="section mcb-section" style="padding-top:80px; padding-bottom:40px;">
                            <div class="section_wrapper mcb-section-inner">
                                <div class="wrap mcb-wrap one valign-top clearfix">
                                    <div class="mcb-wrap-inner">
                                        <div class="column mcb-column one-third column_column">
                                            <div class="column_attr clearfix">
                                                <h3 class="themecolor">Our company</h3>
                                                <p class="big">
                                                    We are NFD party and we love what we do. We are located in <span class="tooltip tooltip-txt" data-tooltip="Mon-Fri 8:00am-6:00pm (GMT +1)">Nigeria</span> and reply always within 24 hours.
                                                </p>
                                                <p>
                                                    Donec ullamcorper nulla non metus auctor fringilla. Sed posuere consectetur est at lobortis. <span class="tooltip tooltip-txt" data-tooltip="Mor porta ac consecteturbi leo risus porta">Morbi leo risus</span> , porta ac consect etur, vestibulum at eros.
                                                </p>
                                            </div>
                                        </div>
                                        <div class="column mcb-column one-third column_column">
                                            <div class="column_attr clearfix">
                                                <h3 class="themecolor">Mission</h3>
                                                <p class="big">
                                                    Our mission is very clear - provide best and fully tested products and solutions for our customers.
                                                </p>
                                                <p>
                                                    Donec ullamcorper nulla non metus auctor fringilla. Sed posuere consectetur est at lobortis. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Donec ullamcorper nulla non metus.
                                                </p>
                                            </div>
                                        </div>
                                        <div class="column mcb-column one-third column_column">
                                            <div class="column_attr clearfix">
                                                <h3 class="themecolor">Passion</h3>
                                                <p class="big">
                                                    We love working with WordPress. Themes based on this web software gives unlimited possibilities.
                                                </p>
                                                <p>
                                                    Donec ullamcorper nulla non metus auctor fringilla. Sed posuere consectetur est at lobortis. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Donec ullamcorper nulla non metus.
                                                </p>
                                            </div>
                                        </div>
                                        <div class="column mcb-column one column_divider">
                                            <hr class="no_line" style="margin: 0 auto 30px">
                                        </div>
                                        <div class="column mcb-column one column_divider">
                                            <hr style="margin: 0 auto 80px">
                                        </div>
                                        <div class="column mcb-column one column_column">
                                            <div class="column_attr clearfix align_center">
                                                <h2 style="margin: 0;">We provide awesomness!</h2>
                                            </div>
                                        </div>
                                        <div class="column mcb-column one column_slider ">
                                            <div class="content_slider ">
                                                <ul class="content_slider_ul">
                                                    <li class="content_slider_li_1"><img width="890" height="470" src="assets/images/home_betheme_contentslider1.jpg" class="scale-with-grid wp-post-image" alt="">
                                                    </li>
                                                    <li class="content_slider_li_2"><img width="890" height="470" src="assets/images/home_betheme_contentslider2.jpg" class="scale-with-grid wp-post-image" alt="">
                                                    </li>
                                                    <li class="content_slider_li_3"><img width="890" height="470" src="assets/images/home_betheme_contentslider3.jpg" class="scale-with-grid wp-post-image" alt="">
                                                    </li>
                                                    <li class="content_slider_li_4"><img width="890" height="470" src="assets/images/home_betheme_contentslider4.jpg" class="scale-with-grid wp-post-image" alt="">
                                                    </li>
                                                </ul>
                                                <div class="slider_pager slider_pagination"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
<?php $this->load->view('footer'); echo "\n"; ?>


    <script>
        jQuery(window).load(function() {
            var retina = window.devicePixelRatio > 1 ? true : false;
            if (retina) {
                var retinaEl = jQuery("#logo img.logo-main");
                var retinaLogoW = retinaEl.width();
                var retinaLogoH = retinaEl.height();
                retinaEl.attr("src", "assets/images/logo-retina.png").width(retinaLogoW).height(retinaLogoH);
                var stickyEl = jQuery("#logo img.logo-sticky");
                var stickyLogoW = stickyEl.width();
                var stickyLogoH = stickyEl.height();
                stickyEl.attr("src", "assets/images/logo-retina.png").width(stickyLogoW).height(stickyLogoH);
                var mobileEl = jQuery("#logo img.logo-mobile");
                var mobileLogoW = mobileEl.width();
                var mobileLogoH = mobileEl.height();
                mobileEl.attr("src", "assets/images/logo-retina.png").width(mobileLogoW).height(mobileLogoH);
                var mobileStickyEl = jQuery("#logo img.logo-mobile-sticky");
                var mobileStickyLogoW = mobileStickyEl.width();
                var mobileStickyLogoH = mobileStickyEl.height();
                mobileStickyEl.attr("src", "assets/images/logo-retina.png").width(mobileStickyLogoW).height(mobileStickyLogoH);
            }
        });
    </script>

</body>

</html>