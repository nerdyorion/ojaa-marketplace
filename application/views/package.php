    <style type="text/css">
        .my-radio {
            margin-right: 20px;
            margin-top: 10px;
            cursor: pointer;
            font-size: 18px;
        }
    </style>
    
    <?php if($error_code == 0 && !empty($error)): ?>
        <div class="alert alert-success alert-dismissable fade show" role="alert">
            <strong>Success!</strong> <?php echo $error; ?>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    <?php elseif($error_code == 1 && !empty($error)): ?>
        <div class="alert alert-danger alert-dismissable fade show" role="alert">
            <strong>Error!</strong> <?php echo $error; ?>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    <?php else: ?>
    <?php endif; ?>

    <!-- BANNER -->

    <div class="section banner-page" data-background="assets/images/Blog-banner.jpg">

        <div class="content-wrap pos-relative">

            <div class="d-flex justify-content-center bd-highlight mb-3">

                <div class="title-page"><?php echo $package['type']; ?></div>

            </div>

            <div class="d-flex justify-content-center bd-highlight mb-3">

                

            </div>

        </div>

    </div>


    
    <!-- CONTENT -->

    <div class="section ">

        <div class="content-wrap">

            <div class="container">

                <div class="row">

                    

                    <div class="col-sm-12 col-md-12 col-lg-12">

                        <div class="single-news">
                            
                            <?php if( isset($package['animation']) && !empty($package['animation']) ): ?>
                            
                                <video autoplay src="assets/animations/<?php echo $package['animation']; ?>" id="player" controls data-plyr-config='{ "title": "<?php echo $package['type']; ?>" }'></video>
                                
                            <?php else: ?>

                                <img src="assets/images/<?php echo $package['image']; ?>" alt="" class="img-fluid rounded"> 
                                
                            <?php endif; ?>

                            <div class="spacer-30"></div>



                            <h2 class="title"><?php echo $package['type']; ?></h2> 

                            <!-- <div class="meta-date" style="color: #000; font-size: 13px;">
                                <p style="color: rgb(241, 194, 46); font-size: 23px;">
                                    <strong>
                                        $<?php // echo $package['fee_usd']; ?> 
                                        <small>(<del>N</del><?php // echo number_format($package['fee_ng']); ?>)</small>
                                    </strong>
                                </p>
                            </div> -->


                            <p><?php echo $package['text']; ?></p>
                        </div>
                        <br />

                        <button id="send" type="button" class="btn btn-primary" onclick="window.location.href='pricing';">Purchase</button>

                        <!-- 
                        <div class="leave-comment-box">
                            <h4 class="title-heading">Get Package Now!</h4>
                            <?php echo form_open('package/secure/' . $package['id'], 'onsubmit="return validate();" class="form-comment" id="package-form"'); ?>
                                <div class="row">
                                    <div class="col-xs-12 col-md-6">
                                        <div class="form-group">
                                            <input type="text" id="name" name="name" value="<?php echo $this->session->flashdata('name'); ?>" class="inputtext form-control" placeholder="Enter Name" required="required">
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-6">
                                        <div class="form-group">
                                            <input type="email" id="name" name="email" value="<?php echo $this->session->flashdata('email'); ?>" class="inputtext form-control" placeholder="Enter Email" required="required">
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-6">
                                        <div class="form-group">
                                            <input type="text" id="phone" name="phone" value="<?php echo $this->session->flashdata('phone'); ?>" class="inputtext form-control" placeholder="Enter Phone">
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-6">
                                        <div class="form-group">
                                            <select id="country" name="country" class="inputtext form-control">
                                                <option value="">-- Select Country --</option>
                                                <?php foreach($countries as $item): ?>
                                                    <option value="<?php echo $item['code'] ?>" <?php echo $this->session->flashdata('country') == $item['name'] ? 'selected="selected"' : ''; ?>><?php echo $item['name']; ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-6">
                                        <div class="form-group">
                                            <input type="text" id="address" name="address" value="<?php echo $this->session->flashdata('address'); ?>" class="inputtext form-control" placeholder="Enter Address">
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-6">
                                        <div class="form-group">
                                            <select id="delivery" name="delivery" class="inputtext form-control" required="required">
                                                <option value="">-- Delivery mode --</option>
                                                <option value="ecopy" <?php echo $this->session->flashdata('delivery') == 'ecopy' ? 'selected="selected"' : ''; ?>>Download E-Copy</option>
                                                <option value="hardcopy" <?php echo $this->session->flashdata('delivery') == 'hardcopy' ? 'selected="selected"' : ''; ?>>Deliver Hard Copy to Address</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-12">
                                        <div class="form-group">
                                            <label class="radio-inline my-radio"> <input type="radio" name="payment_gw" class="payment_gw" value="paystack" required="required"> Local (Naira Payments)</label>
                                            <label class="radio-inline my-radio"> <input type="radio" name="payment_gw" class="payment_gw" value="stripe" required="required"> International (Dollar Payments)</label>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-12">
                                            <p style="font-size: 23px;">
                                                <strong>
                                                    Fee: <span style="color: rgb(241, 194, 46);" id="fee">$<?php echo number_format($package['fee_usd']); ?></span>
                                                </strong>
                                            </p>
                                    </div>
                                    <div class="col-xs-12 col-md-12">
                                        <p class="small">
                                            * Shipping fee may vary for different locations
                                        </p>
                                    </div>
                                    <div class="col-xs-12 col-md-12">
                                        <div class="form-group">
                                            <button id="send" type="submit" class="btn btn-primary">Purchase</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                    
                        </div> 
                        -->
                        <!-- end leave comment -->

                                    <br>

                            

                            <div class="spacer-50"></div>

                            

                            

                            <!-- end accordion -->



                        </div>

                    </div>

                    


                    



                </div>

            </div>

        </div>

    </div>

    <?php $this->load->view('footer'); echo "\n"; ?>
    <script type="text/javascript">
        
        function validate()
        {
            $(':input[type="submit"]').prop('disabled', true);
            $('button[type="submit"]').prop('disabled', true);
            return true;
        }

        $(document).ready(function(){
            $("#package-form input[type='radio']").on('change', function() {
                var radioValue = $('input[name=payment_gw]:checked', '#package-form').val(); 
                
                if(radioValue == 'paystack'){
                    $("#fee").html("<del>N</del><?php echo number_format($package['fee_ng']); ?>");
                }
                else if(radioValue == 'paypal'){
                    $("#fee").html("$<?php echo $package['fee_usd']; ?>");
                }
            });
        });
    </script>
</body>

</html>