
        <section class="breadcrumb_area">
            <img class="breadcrumb_shap" src="<?php echo $this->config->base_url(); ?>assets/img/breadcrumb/banner_bg.png" alt="">
            <div class="container">
                <div class="breadcrumb_content text-center">
                    <h1 class="f_p f_700 f_size_50 w_color l_height50 mb_20">Terms &amp; Conditions</h1>
                    <p class="f_400 w_color f_size_16 l_height26">Why I say old chap that is spiffing off his nut arse pear shaped plastered<br> Jeffrey bodge barney some dodgy.!!</p>
                </div>
            </div>
        </section>
        <section class="bg_color sec_pad">
            <div class="container">
        <div class="row mt-5">
            <div class="col-md-12">

                <h3><?php echo $this->config->item('app_name'); ?> Website Access Agreement</h3>
                <p>Please read this Agreement carefully before accessing the <strong><?php echo $this->config->item('app_name'); ?></strong> Website (“the Website”). </p>

                <p>As a condition to and in consideration of receiving and accessing the Website, the User agrees to be bound by the terms of this Agreement. Use of or access to the Website shall constitute acceptance of and agreement to be bound by this Agreement.</p>

                <p>If you do not wish to be bound by this agreement, do not access the Website. If you have any questions about this Agreement, please contact us via email at <a href="mailto:<?php echo $this->config->item("info_email"); ?>"><?php echo $this->config->item("info_email"); ?></a>.</p>

                <p>THIS AGREEMENT (“Agreement”) is entered into by and between <strong><?php echo $this->config->item('app_name'); ?></strong> Limited ("<strong><?php echo $this->config->item('app_name'); ?></strong>") and any individual, corporation, association, agency, company, or other entity that accesses or uses the Website (the “User” or “you”).</p>

                <p>The Website, which is provided without charge to you, is a World Wide Web site on the Internet that is designed to allow <strong><?php echo $this->config->item('app_name'); ?></strong> to communicate with users and for you to interact with those users. The Website is owned and operated by <strong><?php echo $this->config->item('app_name'); ?></strong>. The Website contains or may contain information, communications, opinions, text, graphics, links, electronic art, animations, audio, video, software, photos, music, sounds and other material and data (collectively, “Content”) formatted, organized and collected in a variety of forms that are generally accessible to Users, including directories and databases, and areas of the Website that can be modified by Users, such as providing relevant information, uploading multimedia files, registering user profiles, and creating auto notify, personalized pages, and personalized project areas.</p>

                <p></p>

            </div>
        </div>
        <div class="row mt-5">
            <div class="col-md-12">

                <h3>User Rights and Responsibilities</h3>
                <p>Access to the Website You are responsible for all charges or other fees incurred in connecting to the internet and accessing the Website.</p>

                <p>User Conduct You agree to access and use the Website only for lawful purposes. You are solely responsible for the knowledge of and adherence to any and all laws, statutes, rules and regulations, pertaining to your use of the Website, including any Interactive Area; the use of any networks or other services connected to the Website, and the communications means by which you connect your modem, computer, or other equipment to the Website. By accessing the Website you agree that you will not:
                    <ul>
                        <li>Restrict or inhibit any other user from using and enjoying the Interactive Features;</li>
                        <li>Post or transmit any unlawful, threatening, abusive, libellous, defamatory, obscene, vulgar, pornographic, profane, or indecent information of any kind, including without limitation any transmissions constituting or encouraging conduct that would constitute a criminal offence, give rise to civil liability or otherwise violate any local, state, national, or international law</li>
                        <li>Post or transmit any information, software, or other material which violates or infringes in the rights of others, including material which is an invasion of privacy or publicity rights or which is protected by copyright, trademark or other proprietary right, or derivative works with respect thereto, without first obtaining permission from the owner or right holder.</li>
                        <li>Post or transmit any information, software or other material which contains a virus or other harmful component;</li>
                        <li>Alter, damage or delete any Content or other communications that are not your own Content or to otherwise interfere with the ability of others to access the Website;</li>
                        <li>Disrupt the normal flow of communication in an Interactive Area;</li>
                        <li>Claim a relationship with or to speak for any business, association, institution or other organization for which you are not authorized to claim such a relationship;</li>
                        <li>Violate any operating rule, policy or guideline of your Internet access provider or online service.</li>
                    </ul>
                </p>

                <p></p>

            </div>
        </div>
        <div class="row mt-5">
            <div class="col-md-12">

                <h3>Intellectual Property Rights</h3>

                <p><strong>Website Content</strong>
                    <ul>
                        <li>You acknowledge that Content on the Website is generally provided by <strong><?php echo $this->config->item('app_name'); ?></strong>, individual contributors of Content (“Contributors”), third party licensees, and/or other Users. You acknowledge that the Website permits access to Content that is protected by copyrights, trademarks, and other proprietary (including intellectual property) rights (“Intellectual Property Rights”), and that these Intellectual Property Rights are valid and protected in all media existing now or later developed and except as is explicitly provided below, your use of Content shall be governed by applicable copyright and other intellectual property laws. You acknowledge that <strong><?php echo $this->config->item('app_name'); ?></strong> exclusively owns a copyright in the “look and feel,” i.e., the selection, coordination, arrangement and presentation of such Content on the Website.</li>
                        <li>You may not modify, copy, reproduce, transmit, distribute, publish, create derivative works from, display or otherwise transfer or commercially exploit any of the Content, in whole or in part, provided, however, that you may (i) make a reasonable number of digital or other form of copies to permit your computer hardware and software to access and view the Content, (ii) print one copy of each piece of Content, (iii) make and distribute a reasonable number of copies of Content, in whole or in part, in hard copy or electronic form for internal use only. Any permitted copies of Content must reproduce in an unmodified form any notices contained in the Content, such as all Intellectual Property Right notices, and an original source attribution to “the Website” and its URL address. You acknowledge that the Website, its Contributors, and/or Users remain the owners of the Content and that you do not acquire any Intellectual Property Rights by downloading or printing Content.</li>
                    </ul>
                </p>

                <p><strong>Content Provided by User</strong>
                    <ul>
                        <li>You may upload to any Interactive Area or otherwise transmit, post, publish, reproduce or distribute, on or through the Website only Content that is not subject to any Intellectual Property Rights or Content in which any holder of Intellectual Property Rights has given express authorization for distribution over the Internet and on the Website, without restriction whatsoever. Any Content submitted with the consent of a copyright owner other than you should contain a phrase such as “Copyright owned by [name of owner]; Used by Permission.” By submitting Content to any Interactive Area, you automatically grant and/or warrant that the owner of such Content, whether it be You or a third party, has expressly granted to <strong><?php echo $this->config->item('app_name'); ?></strong> the royalty free, perpetual, irrevocable, nonexclusive, unrestricted, worldwide right and license to use, reproduce, modify, adapt, publish, translate, create derivative works from, sublicense, distribute, perform, and display such Content, in whole or in part, worldwide and/or to incorporate it in other works in any form, media, or technology now known or later developed for the full term of any Intellectual Property Rights that may exist in such Content. You also permit <strong><?php echo $this->config->item('app_name'); ?></strong> to sublicense to third parties the unrestricted right to exercise any of the foregoing rights granted with respect to such Content. You also permit any User to access, view, store and reproduce the Content for personal use.</li>
                    </ul>
                </p>

                <p><strong>Interactive Areas</strong>
                    <ul>
                        <li>
                            You acknowledge that the Website may include various interactive areas (“Interactive Areas”), including but not limited to classifieds. These Interactive Areas allow feedback to the Website and real-time interaction between users. You further understand that <strong><?php echo $this->config->item('app_name'); ?></strong> does not control the messages, information, or files delivered to such Interactive Areas and that the Website may offer you and other Users the capability of creating and managing an Interactive Area. However, neither <strong><?php echo $this->config->item('app_name'); ?></strong> nor its affiliates, their respective directors, officers, employees and agents are responsible for Content within any Interactive Area. Your use and/or management of an Interactive Area will be governed by this Agreement and any additional rules or operating procedures of any Interactive Area established by you or another User, as applicable. You recognize that <strong><?php echo $this->config->item('app_name'); ?></strong> cannot, and does not intend to, screen communications in advance.
                            <br /><br />
                            Moreover, because the Website encourages open and candid communication in the Interactive Areas, <strong><?php echo $this->config->item('app_name'); ?></strong> cannot determine in advance the accuracy or conformance to this Agreement of any Content transmitted in an Interactive Area. The Website is not responsible for screening, policing, editing, reviewing or monitoring any Content in an Interactive Area. Notwithstanding the above, you agree that <strong><?php echo $this->config->item('app_name'); ?></strong> has the right to monitor any Interactive Area, from time to time and to disclose any information as necessary to satisfy any law, regulation or other governmental request, to operate the Interactive Area, or to protect itself or other Users. If notified of content that is alleged not to conform to this Agreement, <strong><?php echo $this->config->item('app_name'); ?></strong> may investigate the allegation and determine in its sole discretion whether to remove or request the User to remove such Content. <strong><?php echo $this->config->item('app_name'); ?></strong> reserves the right to prohibit conduct, communication or Content within an Interactive Area, or to edit, refuse to post, or to remove any Content, in whole or in part, which it deems in its sole discretion to 
                            <ul>
                                <li>(i) violate the then standard provisions of this Agreement or any other standard, written Website policy in effect at that time,</li>
                                <li>(ii) be harmful to the rights of any User, <strong><?php echo $this->config->item('app_name'); ?></strong>, or other third parties,</li>
                                <li>(iii) violate applicable law, or</li>
                                <li>(iv) be otherwise objectionable.</li>
                            </ul> 
                        </li>
                    </ul>
                </p>

            </div>
        </div>
        <div class="row mt-5">
            <div class="col-md-12">

                <h3>Termination</h3>
                <p>The only right with respect to dissatisfaction with any policies, guidelines, or practices of <strong><?php echo $this->config->item('app_name'); ?></strong> in operating the Website, or any change in Content, is for you to discontinue accessing the Website. <strong><?php echo $this->config->item('app_name'); ?></strong> may terminate or temporarily suspend your access to all or any part of the Website, without notice, for conduct that <strong><?php echo $this->config->item('app_name'); ?></strong> believes is a violation of this Agreement or any policies or guidelines posted by <strong><?php echo $this->config->item('app_name'); ?></strong> or for other conduct which <strong><?php echo $this->config->item('app_name'); ?></strong> believes, in its sole discretion, are harmful to <strong><?php echo $this->config->item('app_name'); ?></strong> or other Users. <strong><?php echo $this->config->item('app_name'); ?></strong> may discontinue operating the Website and terminate this Agreement without notice at any time for any reason in its sole discretion. In the event of termination, you are no longer authorized to access the Website, including the Interactive Areas, and the restrictions imposed on you with respect to Content downloaded from the Website, as well as the disclaimers and limitations of liabilities set forth in this agreement, shall survive.</p>

                <p><strong>Links; Disclaimers of Warranties; Limitations of Liability</strong></p>

                <p>You understand that except for Content, products or services expressly available at the Website, neither <strong><?php echo $this->config->item('app_name'); ?></strong>, its subsidiary and parent companies, or affiliates, or their respective directors, officers, employees, and agents control, provide, or is responsible for any Content, goods or services available through sites on the Internet linked to or from the Website. All such Content, goods and services are made accessible on the Internet by independent third parties and are not part of the Website or controlled by <strong><?php echo $this->config->item('app_name'); ?></strong>. <strong><?php echo $this->config->item('app_name'); ?></strong> neither endorses nor is responsible for the accuracy, completeness, usefulness, quality or availability of any Content, goods or services available on any site linked to or from the Website, which are the sole responsibility of such independent third parties, and your use thereof is solely at your own risk. Neither <strong><?php echo $this->config->item('app_name'); ?></strong>, its subsidiary and parent companies, or affiliates, or their respective directors, officers, employees, and agents shall be held responsible or liable, directly or indirectly, for any loss or damage caused or alleged to have been caused by your use of or reliance on any Content, goods or services available on any site linked to or from the Website or your inability to access the Internet or any site linked to or from the Website.</p>

                <p><strong>Disclaimer of Warranties</strong></p>

                <p>Please use your best judgment in evaluating all information contained or opinions expressed on the Website. It is the policy of <strong><?php echo $this->config->item('app_name'); ?></strong> not to endorse or oppose any opinion expressed by a User or Content provided by a User, Contributor, or other independent party. You expressly agree that your use of the Website is at your sole risk. Neither <strong><?php echo $this->config->item('app_name'); ?></strong> nor any of its subsidiary and parent companies, or affiliates, or their respective directors, officers, employees, agents, contractors, affiliates, licensors or other suppliers providing content, data, information or services warrants that the Website or any internet site linked to or from the Website will be uninterrupted or error free, that defects will be corrected, or that this site, including the interactive areas, or the server that makes it available are free of viruses or other harmful components. nor do any of them make any warranty as to the results that may be obtained from the use of the website or any internet site linked to or from the Website or as to the timeliness, sequence, accuracy, authority, completeness, usefulness, non-infringement, reliability, availability, or substance of any content, information, service, or transaction provided through the Website or any site linked to or from the Website. The Website is provided on an “as is,” “as available” basis, without warranties of any kind, either express or implied, including, without limitation, those of merchantability and fitness for a particular purpose.</p>

                <p><strong>Limitation of Liability</strong></p>

                <p>Under no circumstances shall <strong><?php echo $this->config->item('app_name'); ?></strong>, its subsidiary and parent companies, or affiliates, or their respective directors, officers, employees, agents, contractors, or licensors, be liable for any direct or incidental, special or consequential damages under or arising from this agreement, the Website, or any Internet site linked to or from the Website, whether for breach of contract, tortuous behaviour, negligence, or under any other cause of action, including without limitation, any liability for damages caused or allegedly caused by any failure of performance, error, omission, interruption, electrical surge/damage/interference, deletion, defect, delay in operation or transmission, computer virus, communications line failure, breakdown of equipment, software error, infringement, unauthorized access to, or theft, destruction, alteration, or use of, records.</p>

                <p>Under no circumstances shall <strong><?php echo $this->config->item('app_name'); ?></strong>, its subsidiary and parent companies, or affiliates, or their respective directors, officers, employees, agents, contractors, or licensors, be liable to you or any other third party for any decision made or action taken by you in reliance on the content contained within the Website or the content contained within any Internet site linked to or from the Website. The content within the Website and the content within Internet sites linked to or from the Website may include technical or other inaccuracies or typographical errors. Changes are periodically added to the content herein; these changes will be incorporated in new versions of the Website and specifically are included in this section agreement. <strong><?php echo $this->config->item('app_name'); ?></strong> and its contributors may make improvements and/or changes in the content at any time and from time to time.</p>

                <p>You specifically acknowledge and agree that <strong><?php echo $this->config->item('app_name'); ?></strong> is not liable for any defamatory, offensive, fraudulent, or otherwise illegal conduct of any user. If you are dissatisfied with any Website site content, or with the access agreement of the Website, in whole or in part, your sole and exclusive remedy is to discontinue using the Website.</p>

                <p><strong>Indemnity</strong></p>

                <p>You agree to indemnify and hold <strong><?php echo $this->config->item('app_name'); ?></strong>, its parent or subsidiary companies and their affiliates, and their respective directors, officers, employees, and agents from any and all liabilities, claims and expenses, including reasonable attorneys’ fees, arising from breach of this Agreement, any other policy, your use or access of the Website or any Internet site linked to or from the Website, or in connection with the transmission of any Content on the Website.</p>

                <p><strong>Miscellaneous</strong></p>

                <p>This Agreement comprises the entire agreement between <strong><?php echo $this->config->item('app_name'); ?></strong> and you, and supersedes any prior agreements with respect to the subject matter herein. <strong><?php echo $this->config->item('app_name'); ?></strong> may revise this Agreement or any other policy at any time and from time to time, and such revision shall be effective two (2) days upon posting notice of such revision prominently on the Website. You agree to review this Agreement periodically to be aware of such revisions. If any such revision is unacceptable to you, you must discontinue accessing the Website.</p>

                <p>Your continued access to and use of the Website following notice of any such revision shall conclusively be deemed acceptance of all such revisions. The provisions of Sections 1.2, 2.1, 2.2, 5.1, 5.2, 5.3, 6 and 7 shall survive the termination or expiration of this Agreement. If any provision of this Agreement or any other policy be held invalid or unenforceable, that portion shall be construed in accordance with applicable law as nearly as possible to reflect the original intention of the parties and the remaining portions will continue in full force and effect. The failure of <strong><?php echo $this->config->item('app_name'); ?></strong> to insist upon or enforce strict performance of any provision of this Agreement shall not be construed as a waiver of any provision or right. This Agreement shall be governed by the laws of the state or province of domicile of <strong><?php echo $this->config->item('app_name'); ?></strong>, excluding its conflict of laws rules, and you and the Website each submit to the exclusive jurisdiction of the courts of that state or province.</p>

                <p>This Agreement is personal to you and you may not assign your rights or obligations thereunder to anyone. All logos, brand names, products, trademarks and service marks appearing herein may be the trademarks or service marks of their respective owners. References to any trademark, service mark and links to or from the Website have been done strictly for clarification and identification and does not constitute endorsement by <strong><?php echo $this->config->item('app_name'); ?></strong> of the products, services or information offered by the owner of such trademark, service mark or link or endorsement of <strong><?php echo $this->config->item('app_name'); ?></strong> by such trademark, service mark or link owner.</p>

                <p></p>

            </div>
        </div>
            </div>
        </section>

  <?php $this->load->view('footer'); echo "\n"; ?>

</body>

</html>