<style type="text/css">
    .text-white
    {
        color: #eee;
    }
</style>

<body class="color-custom style-default layout-full-width no-content-padding header-classic sticky-header sticky-tb-color ab-show subheader-both-center menu-link-color menuo-right mobile-tb-hide mobile-side-slide mobile-mini-mr-ll">
    <div id="Wrapper">
        <div id="Header_wrapper">
            <header id="Header">
        <?php $this->load->view('menu'); echo "\n"; ?>
            </header>
            <div id="Subheader">
                <div class="container">
                    <div class="column one">
                        <h1 class="title">Be an EXCO</h1>
                        <ul class="breadcrumbs no-link">
                            <li>
                                <a href="./">Home</a><span><i class="icon-right-open"></i></span>
                            </li>
                            <li>
                                <a href="Be-An-Exco">Be an EXCO</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div id="Content">
            <div class="content_wrapper clearfix">
                <div class="sections_group">
                    <div class="entry-content">
                        <div class="section mcb-section" style="padding-top:120px; padding-bottom:70px; background-color:#052154">
                            <div class="section_wrapper mcb-section-inner">
                                <div class="wrap mcb-wrap one valign-top clearfix">
                                    <div class="mcb-wrap-inner">
                                        <div class="column mcb-column one column_column">

                                            <?php if($error_code == 0 && !empty($error)): ?>
                                                <div class="alert alert_success">
                                                    <div class="alert_icon">
                                                        <i class="icon-check"></i>
                                                    </div>
                                                    <div class="alert_wrapper">
                                                        Success! <?php echo $error; ?>
                                                    </div>
                                                    <a href="<?php echo current_url();?>#" class="close"><i class="icon-cancel"></i></a>
                                                </div>
                                            <?php elseif($error_code == 1 && !empty($error)): ?>
                                                <div class="alert alert_error">
                                                    <div class="alert_icon">
                                                        <i class="icon-alert"></i>
                                                    </div>
                                                    <div class="alert_wrapper">
                                                        Error! <?php echo $error; ?>
                                                    </div>
                                                    <a href="<?php echo current_url();?>#" class="close"><i class="icon-cancel"></i></a>
                                                </div>
                                            <?php else: ?>
                                            <?php endif; ?>



                                            <div class="column_attr clearfix">
                                                <div id="contactWrapper">
                                                    <?php echo form_open('Be-An-Exco/secure', 'onsubmit="return validate();", id="contactform"'); ?>
                                                        <!-- One Second (1/2) Column -->
                                                        <div class="column one" style="clear: both;">
                                                            <label class="text-white">Application Fee: <b>N2,000</b>
                                                                <br />
                                                                <a href="https://chat.whatsapp.com/7l4lgVuv7MiJE0Sx6U32lg">Click to Join WhatsApp group</a>
                                                            </label>
                                                        </div>

                                                        <!-- One Second (1/2) Column -->
                                                        <div class="column one-second" style="clear: both;">
                                                            <label for="position_id" class="text-white">Choose Position *</label>
                                                            <select name="position_id" id="position_id" aria-required="true" aria-invalid="false" required="required" style="padding: 8px;">
                                                                <option value="" selected="selected">-- Select --</option>
                                                                <?php foreach ($state_executive_positions as $row): ?>
                                                                  <option value="<?php echo $row['id']; ?>" <?php echo $this->session->flashdata('position_id') == $row['id'] ? 'selected="selected"' : ''; ?>><?php echo $row['title']; ?></option>
                                                              <?php endforeach; ?>
                                                            </select>
                                                        </div>

                                                        <!-- One Second (1/2) Column -->
                                                        <div class="column one-second">
                                                            <label for="email" class="text-white">Your email *</label>
                                                            <input placeholder="Your e-mail" type="email" name="email" id="email" value="<?php echo $this->session->flashdata('email'); ?>" maxlength="255" aria-required="true" aria-invalid="false" required="required" />
                                                        </div>

                                                        <div class="column one" style="padding-top: 20px;">
                                                            <input type="submit" value="Apply" id="submit" />
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<?php $this->load->view('footer'); echo "\n"; ?>


    <script>
        jQuery(window).load(function() {
            var retina = window.devicePixelRatio > 1 ? true : false;
            if (retina) {
                var retinaEl = jQuery("#logo img.logo-main");
                var retinaLogoW = retinaEl.width();
                var retinaLogoH = retinaEl.height();
                retinaEl.attr("src", "assets/images/logo-retina.png").width(retinaLogoW).height(retinaLogoH);
                var stickyEl = jQuery("#logo img.logo-sticky");
                var stickyLogoW = stickyEl.width();
                var stickyLogoH = stickyEl.height();
                stickyEl.attr("src", "assets/images/logo-retina.png").width(stickyLogoW).height(stickyLogoH);
                var mobileEl = jQuery("#logo img.logo-mobile");
                var mobileLogoW = mobileEl.width();
                var mobileLogoH = mobileEl.height();
                mobileEl.attr("src", "assets/images/logo-retina.png").width(mobileLogoW).height(mobileLogoH);
                var mobileStickyEl = jQuery("#logo img.logo-mobile-sticky");
                var mobileStickyLogoW = mobileStickyEl.width();
                var mobileStickyLogoH = mobileStickyEl.height();
                mobileStickyEl.attr("src", "assets/images/logo-retina.png").width(mobileStickyLogoW).height(mobileStickyLogoH);
            }
        });


        function validate()
        {
            $(':input[type="submit"]').prop('disabled', true);
            $('button[type="submit"]').prop('disabled', true);
            return true;
        }


        $(document).ready(function() {
          $("#origin_state_id").change(function() {
            var state_id = $(this).val();
            if(state_id != "" && state_id != null) {
              $.ajax({
                url:"register/getLgasByStateID/" + state_id,
            // data:{c_id:state_id},
            type:'GET',
            success:function(response) {
              var resp = $.trim(response);
              $("#origin_lga_id").html(resp);
          }
      });
          } else {
              $("#origin_lga_id").html("<option value=''>-- Select --</option>");
          }
      });

        //   $('#searchIcon').click(function () {
        //     $('#searchForm').submit();
        // });
      });
    </script>

</body>

</html>