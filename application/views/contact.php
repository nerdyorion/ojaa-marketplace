

      <!-- Breadcromb Area Start -->
      <section class="gauto-breadcromb-area section_70">
         <div class="container">
            <div class="row">
               <div class="col-md-12">
                  <div class="breadcromb-box">
                     <h3>Contact Us</h3>
                     <ul>
                        <li><i class="fa fa-home"></i></li>
                        <li><a href="<?php echo $this->config->base_url(); ?>">Home</a></li>
                        <li><i class="fa fa-angle-right"></i></li>
                        <li>Contact Us</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <!-- Breadcromb Area End -->
       
       
      <!-- Contact Area Start -->
      <section class="gauto-contact-area section_70">
         <div class="container">
            <div class="row">
               <div class="col-lg-7">
                  <div class="contact-left">
                     <h3>Get in touch</h3>
              <?php if($error_code == 0 && !empty($error)): ?>

                <div class="alert alert-success alert-dismissable fade show" role="alert">
                  <strong>Success!</strong> <?php echo $error; ?>
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <?php elseif($error_code == 1 && !empty($error)): ?>
                  <div class="alert alert-danger alert-dismissable fade show" role="alert">
                    <strong>Error!</strong> <?php echo $error; ?>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <?php else: ?>
                  <?php endif; ?>
                     <?php echo form_open('contact/secure', ''); ?>
                        <div class="row">
                           <div class="col-md-6">
                              <div class="single-contact-field">
                                 <input type="text" placeholder="Your Name" name="name" required="required" value="<?php $this->session->flashdata('name'); ?>">
                              </div>
                           </div>
                           <div class="col-md-6">
                              <div class="single-contact-field">
                                 <input type="email" placeholder="Email Address" name="email" required="required" value="<?php $this->session->flashdata('email'); ?>">
                              </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-md-6">
                              <div class="single-contact-field">
                                 <input type="text" placeholder="10 + 19 = " name="captcha" required="required">
                              </div>
                           </div>
                           <div class="col-md-6">
                              <div class="single-contact-field">
                                 <input type="tel" placeholder="Phone Number" name="phone" value="<?php $this->session->flashdata('phone'); ?>">
                              </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-md-12">
                              <div class="single-contact-field">
                                 <textarea placeholder="Write here your message" name="message" required="required"><?php $this->session->flashdata('message'); ?></textarea>
                              </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-md-12">
                              <div class="single-contact-field">
                                 <button type="submit" class="gauto-theme-btn"><i class="fa fa-paper-plane"></i> Send Message</button>
                              </div>
                           </div>
                        </div>
                     </form>
                  </div>
               </div>
               <div class="col-lg-5">
                  <div class="contact-right">
                     <h3>Contact information</h3>
                     <div class="contact-details">
                        <p><i class="fa fa-map-marker"></i> <?php echo $this->config->item("info_address"); ?> </p>
                        <div class="single-contact-btn">
                           <h4>Email Us</h4>
                           <a href="mailto:<?php echo $this->config->item("info_email"); ?>"><?php echo $this->config->item("info_email"); ?></a>
                        </div>
                        <div class="single-contact-btn">
                           <h4>Call Us</h4>
                           <a href="tel:<?php echo $this->config->item("info_phone"); ?>"><?php echo $this->config->item("info_phone"); ?></a>
                        </div>
                        <div class="social-links-contact">
                           <h4>Follow Us:</h4>
                           <ul>
                              <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                              <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                              <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                              <li><a href="#"><i class="fa fa-skype"></i></a></li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <!-- Contact Area End -->


<?php $this->load->view('footer'); echo "\n"; ?>


</body>

</html>