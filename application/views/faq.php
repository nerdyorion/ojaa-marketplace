
<div class="page-header">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h2>FAQ's</h2>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index-2.html">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">faq</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- end of page header -->
<!-- start faq question -->
<div class="faq-question-area section-padding">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="faq-single-title">General Question</h2>
                <div id="accordion" class="second-accordion">
                    <div class="card">
                        <div class="card-header" id="headingOne">
                            <h5 class="mb-0">
                                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    What do I need to be eligible for your loans? (employed)
                                </button>
                            </h5>
                        </div>
                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                                <ul>
                                  <li>You shall be between 18 and 60 years old.</li>
                                  <li>You must have a current or savings account with a commercial Bank in Nigeria.</li>
                                  <li>Applicant must live and or work in Lagos State.</li>
                                  <li>You must have a verifiable monthly income.</li>
                                  <li>You must have a clean and verifiable credit history</li>
                                  <li>You must not have a history of returned cheques in your account.</li>
                                  <li>One recent passport photograph (within the last 6 months)</li>
                                  <li>Salary bank statement for the last 6 months (must be stamped by the bank). Your bank can send it directly to us at info@xtraloans4u.com</li>
                                  <li>NUBAN post-dated cheque leaves for the duration of loan or completed Direct Debit Form</li>
                                  <li>Company / Staff Identification card</li>
                                  <li>Letter of Employment, Confirmation, Promotion, Redeployment, Salary Increase or Introduction Letter from Employer’s HR to <strong>Xtraloans4u</strong></li>
                                  <li>Utility Bill (PHCN, Water bill, LAWMA bill OR Rent Agreement) not older than 3 months</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingTwo">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                  How do I apply?
                              </button>
                          </h5>
                      </div>
                      <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                        <div class="card-body">
                            <ul>
                              <li>Online – www.xtraloans4u.com</li>
                              <li>Phone - 07030052585, 08029560984</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingThree">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                              What is the minimum and maximum amount I can borrow?
                          </button>
                      </h5>
                  </div>
                  <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                    <div class="card-body">
                        You can take loans ranging from NGN 500,000.00 to NGN 1,000,000.00
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header" id="heading-general-1">
                    <h5 class="mb-0">
                        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collaps-general-1" aria-expanded="false" aria-controls="collaps-general-1">
                          Do I have to be in paid employment to get your loans?
                      </button>
                  </h5>
              </div>
              <div id="collaps-general-1" class="collapse" aria-labelledby="heading-general-1" data-parent="#accordion">
                <div class="card-body">
                    Yes, you must be gainfully employed to benefit from our loan products.
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-header" id="heading-general-2">
                <h5 class="mb-0">
                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collaps-general-2" aria-expanded="false" aria-controls="collaps-general-2">
                     Does <strong>Xtraloans4u</strong> require a credit check?
                 </button>
             </h5>
         </div>
         <div id="collaps-general-2" class="collapse" aria-labelledby="heading-general-2" data-parent="#accordion">
            <div class="card-body">
                Yes, this forms a major part of our loan criteria that we use to verify information provided in your loan application form. <strong>Xtraloans4u</strong> will obtain it directly, our customers do not need to get this for us.
            </div>
        </div>
    </div>
</div>
<h2 class="faq-single-title">Eligibility & Documentation</h2>
<div id="accordion-2" class="second-accordion">
    <div class="card">
        <div class="card-header" id="eligibility-heading-1">
            <h5 class="mb-0">
                <button class="btn btn-link" data-toggle="collapse" data-target="#eligibility-collaps-1" aria-expanded="true" aria-controls="eligibility-collaps-1">
                  Do I need to appear physically to apply for a loan?
              </button>
          </h5>
      </div>
      <div id="eligibility-collaps-1" class="collapse show" aria-labelledby="eligibility-heading-1" data-parent="#accordion-2">
        <div class="card-body">
            No, you do not need to appear physically to make an application for a loan. You can make an application on our website and by calling 07030052585, 08029560984 to speak to our sales team.
        </div>
    </div>
</div>
<div class="card">
    <div class="card-header" id="eligibility-heading-2">
        <h5 class="mb-0">
            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#eligibility-collaps-2" aria-expanded="false" aria-controls="eligibility-collaps-2">
              Will the details of my loan be disclosed to a third party
          </button>
      </h5>
  </div>
  <div id="eligibility-collaps-2" class="collapse" aria-labelledby="eligibility-heading-2" data-parent="#accordion-2">
    <div class="card-body">
        In a bid to collect our loans disbursed, we reserve the right to contact your employers or next of kin about details relating to your loans with us.
    </div>
</div>
</div>
<div class="card">
    <div class="card-header" id="eligibility-heading-6">
        <h5 class="mb-0">
            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#eligibility-collaps-6" aria-expanded="false" aria-controls="eligibility-collaps-6">
              Can I make a part payment before my loan repayment date?
          </button>
      </h5>
  </div>
  <div id="eligibility-collaps-6" class="collapse" aria-labelledby="eligibility-heading-6" data-parent="#accordion-2">
    <div class="card-body">
        Yes, a customer can part liquidate and have the facility restructured as agreed in the loan contract
    </div>
</div>
</div>
<div class="card">
    <div class="card-header" id="heading-general-5">
        <h5 class="mb-0">
            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collaps-general-5" aria-expanded="false" aria-controls="collaps-general-5">
             Can I pay down my outstanding loan before the end of the loan period?
         </button>
     </h5>
 </div>
 <div id="collaps-general-5" class="collapse" aria-labelledby="heading-general-5" data-parent="#accordion-2">
    <div class="card-body">
        Yes, you would pay your outstanding principal and interest up until the time when you are liquidating.
    </div>
</div>
</div>
<div class="card">
    <div class="card-header" id="heading-general-6">
        <h5 class="mb-0">
            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collaps-general-6" aria-expanded="false" aria-controls="collaps-general-5">
             How do I get confirmation that my loan has been approved?
         </button>
     </h5>
 </div>
 <div id="collaps-general-6" class="collapse" aria-labelledby="heading-general-6" data-parent="#accordion-2">
    <div class="card-body">
        You will get an SMS once your loan has been approved based on your document verification. An email will also be sent to you with details of your Loan and account number. Please make sure you provide all your correct contact information to make this possible for us. If you have received an SMS, the funds would have been credited into your account.
    </div>
</div>
</div>
</div>
<!-- <h2 class="faq-single-title">About <?php echo $this->config->item('app_name'); ?></h2>
<div id="accordion-3" class="second-accordion">
    <div class="card">
        <div class="card-header" id="about-heading-1">
            <h5 class="mb-0">
                <button class="btn btn-link" data-toggle="collapse" data-target="#about-collaps-1" aria-expanded="true" aria-controls="about-collaps-1">
                  Where are you located?
              </button>
          </h5>
      </div>
      <div id="about-collaps-1" class="collapse show" aria-labelledby="about-heading-1" data-parent="#accordion-3">
        <div class="card-body">
            The passages of Lorem Ipsum available but the majority ave suffer alteration embarrased the point of using Lorem sum is that it has a more-or-less normal distribution.
        </div>
    </div>
</div>
<div class="card">
    <div class="card-header" id="about-heading-2">
        <h5 class="mb-0">
            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#about-collaps-2" aria-expanded="false" aria-controls="about-collaps-2">
              What kinds of business financing do you offer?
          </button>
      </h5>
  </div>
  <div id="about-collaps-2" class="collapse" aria-labelledby="about-heading-2" data-parent="#accordion-3">
    <div class="card-body">
        The passages of Lorem Ipsum available but the majority ave suffer alteration embarrased the point of using Lorem sum is that it has a more-or-less normal distribution.
    </div>
</div>
</div>
<div class="card">
    <div class="card-header" id="about-heading-6">
        <h5 class="mb-0">
            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#about-collaps-6" aria-expanded="false" aria-controls="about-collaps-6">
              Do you offering refinancing ?
          </button>
      </h5>
  </div>
  <div id="about-collaps-6" class="collapse" aria-labelledby="about-heading-6" data-parent="#accordion-3">
    <div class="card-body">
        The passages of Lorem Ipsum available but the majority ave suffer alteration embarrased the point of using Lorem sum is that it has a more-or-less normal distribution.
    </div>
</div>
</div>
<div class="card">
    <div class="card-header" id="about-heading-5">
        <h5 class="mb-0">
            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#about-collaps-5" aria-expanded="false" aria-controls="about-collaps-5">
             Can you help me if I am not an advisor?
         </button>
     </h5>
 </div>
 <div id="about-collaps-5" class="collapse" aria-labelledby="about-heading-5" data-parent="#accordion-3">
    <div class="card-body">
        The passages of Lorem Ipsum available but the majority ave suffer alteration embarrased the point of using Lorem sum is that it has a more-or-less normal distribution.
    </div>
</div>
</div>
</div> -->
<h2 class="faq-single-title">After Approved</h2>
<div id="accordion-4" class="second-accordion">
    <div class="card">
        <div class="card-header" id="after-heading-1">
            <h5 class="mb-0">
                <button class="btn btn-link" data-toggle="collapse" data-target="#after-collaps-1" aria-expanded="true" aria-controls="after-collaps-1">
                  How long do I have to wait after repaying a loan in full before I can reapply for a new one?
              </button>
          </h5>
      </div>
      <div id="after-collaps-1" class="collapse show" aria-labelledby="after-heading-1" data-parent="#accordion-4">
        <div class="card-body">
            You can get a new loan as soon as you repay your existing loan.
        </div>
    </div>
</div>
<div class="card">
    <div class="card-header" id="after-heading-2">
        <h5 class="mb-0">
            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#after-collaps-2" aria-expanded="false" aria-controls="after-collaps-2">
              How do I make a payment on my loan?
          </button>
      </h5>
  </div>
  <div id="after-collaps-2" class="collapse" aria-labelledby="after-heading-2" data-parent="#accordion-4">
    <div class="card-body">
      <ul>
        <li>
          <b>PAYSTACK</b>
          <ul>
            <li>You can make quick and convenient repayments through the PAYSTACK platform. <a href="https://paystack.com/pay/">Follow this link to pay</a></li>
          </ul>
        </li>
        <li>
          <b>CHEQUES</b>
          <ul>
            <li>The cheques deposited as part of your loan documentation will be presented to your Bank on your due date. Please always ensure you have enough funds on your due date.</li>
          </ul>
        </li>
        <li>
          <b>ONLINE</b>
          <ul>
            <li>Make online transfers to any of our accounts:</li>
            <li>Please state your name and Account Number clearly as the depositor and ensure you notify us by e-mail to info@xtraloans4u.com 72 hours before your due date for your cheques to be retrieved.</li>
          </ul>
        </li>
      </ul>
    </div>
</div>
</div>
<div class="card">
    <div class="card-header" id="after-heading-6">
        <h5 class="mb-0">
            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#after-collaps-6" aria-expanded="false" aria-controls="after-collaps-6">
              I changed my job/ I travelled out of the country. can I change my repayment date?
          </button>
      </h5>
  </div>
  <div id="after-collaps-6" class="collapse" aria-labelledby="after-heading-6" data-parent="#accordion-4">
    <div class="card-body">
        Yes, you can change your repayment date, but it will be subject to review of proof of change of job and salary dates. Additional charges may be applicable.
    </div>
</div>
</div>
<div class="card">
    <div class="card-header" id="after-heading-5">
        <h5 class="mb-0">
            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#after-collaps-5" aria-expanded="false" aria-controls="after-collaps-5">
             What is the penalty for defaulting on a loan?
         </button>
     </h5>
 </div>
 <div id="after-collaps-5" class="collapse" aria-labelledby="after-heading-5" data-parent="#accordion-4">
    <div class="card-body">
        A Penalty charge is applied to your account and accrues on a daily basis until it is settled. It is also important to note that continuous default on a loan also adversely affects your credit rating with the Credit Bureau agencies and your ability to apply for a loan in the future from <strong>Xtraloans4u</strong> or any other Financial institution. Also, a defaulter may be reported to his or her employer.
    </div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<!-- end of faq question -->

<?php $this->load->view('footer'); echo "\n"; ?>

</body>

</html>