
    <div class="page-header">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h2>Loan Calculator</h2>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?php echo $this->config->base_url(); ?>">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Loan Calculator</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <section class="loan-calculator-page section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12 single-nav">
                    <h3>Loan
                        <!-- <span>@ 10%</span> -->
                    </h3>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8 loan-slider-box">
                    <div class="single-loan-slider">
                        <h4>Loan Amount</h4>
                        <div id="pricipal-slide"></div>
                        <div>
                            <span>NGN</span>
                            <h6 id="pricipal"></h6>
                        </div>
                    </div>
                    <div class="single-loan-slider">
                        <h4>Loan Duration</h4>
                        <div id="totalyear-slide"></div>
                        <div>
                            <h6 id="totalyear"></h6>
                            <span id="totalyear-label">Months</span>
                        </div>
                    </div>
                    <div class="single-loan-slider" style="display: none">
                        <h4>Interest Rate</h4>
                        <div id="intrest-slide"></div>
                        <div>
                            <h6 id="intrest"></h6>
                            <span>%</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 text-center loan-emi">
                    <div class="total-calculation">
                        <div class="single-total">
                            <h5>Loan EMI</h5>
                            <h2 class="emi-price" id="emi">0</h2>
                        </div>
                        <div class="single-total">
                            <h5>Total Interest Payable</h5>
                            <h2 id="tbl_emi">0</h2>
                        </div>
                        <div class="single-total">
                            <h5>Total Payment
                                <br>(Principal + Interest)
                            </h5>
                            <h2 id="tbl_la">0</h2>
                        </div>
                        <button class="btn applybtn btn-default btn-sm">APPLY NOW</button>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php $this->load->view('footer'); echo "\n"; ?>
<script type="text/javascript">
    $("#intrest-slide").slider({ disabled: true });
</script>
</body>

</html>