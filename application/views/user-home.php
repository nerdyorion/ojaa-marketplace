<style>
p#countdown {
  text-align: center;
  font-size: 2em;
  margin-top: 0px;
}
</style>
<section class="about-page-shortcode section-padding mt-3" style="margin-bottom: 100px;">
  <div class="container rowpadding-with-border">
    <div class="row">
      <?php $this->load->view('side-nav-menu'); echo "\n"; ?>
      <div class="col-lg-9">
        <div class="row">
          <div class="col-lg-12">
            <div class="about-content">
              <h2>My Account</h2>
              <hr />

                            <?php if($error_code == 0 && !empty($error)): ?>

                                <div class="alert alert-success alert-dismissable fade show" role="alert">
                                    <strong>Success!</strong> <?php echo $error; ?>
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <?php elseif($error_code == 1 && !empty($error)): ?>
                                    <div class="alert alert-danger alert-dismissable fade show" role="alert">
                                        <strong>Error!</strong> <?php echo $error; ?>
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <?php else: ?>
                                    <?php endif; ?>
                                    
              <!-- <p>Welcome, <?php // echo isset($_SESSION['user_full_name']) ? explode(" ", $_SESSION['user_full_name'])[0] : "User"?></p> -->
              <p>Welcome <?php echo isset($_SESSION['user_full_name']) ? $_SESSION['user_full_name'] : "User"?>,</p>

            </div>
          </div>
        </div>


      </div>
    </div>
  </div>
</section>
<!-- end of about section -->

<?php $this->load->view('footer'); echo "\n"; ?>
<script type="text/javascript">
</script>

</body>

</html>