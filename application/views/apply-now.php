
<div class="page-header">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h2>Apply Now</h2>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo $this->config->base_url(); ?>">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Apply Now</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- end of page header -->
<section class="section-padding contact-us-padding">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12 col-lg-10">
                <?php if($error_code == 0 && !empty($error)): ?>
                    <div class="alert alert-success alert-dismissable fade show">
                        <a href="<?php echo current_url();?>#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>Success!</strong> <?php echo $error; ?>
                    </div>
                    <?php elseif($error_code == 1 && !empty($error)): ?>
                        <div class="alert alert-danger alert-dismissable fade show">
                            <a href="<?php echo current_url();?>#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong>Error!</strong> <?php echo $error; ?>
                        </div>
                        <?php else: ?>
                        <?php endif; ?>
                        <?php echo form_open('Apply-Now/secure', 'onsubmit="return validate();" id="applicationform"'); ?>
                        <div class="row list-input">
                            <div class="col-md-6 mr0">
                                <div class="single-get-touch">
                                    <label for="first_name">First name</label>
                                    <input type="text" name="first_name" id="first_name" placeholder="" value="<?php echo $this->session->flashdata('first_name'); ?>" required="required">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="single-get-touch">
                                    <label for="last_name">Last name</label>
                                    <input type="text" name="last_name" id="last_name" placeholder="" value="<?php echo $this->session->flashdata('last_name'); ?>" required="required">
                                </div>
                            </div>
                            <div class="col-md-12 mr0">
                                <div class="single-get-touch">
                                    <label for="email">Email</label>
                                    <input type="email" name="email" id="email" placeholder="" value="<?php echo $this->session->flashdata('email'); ?>" required="required">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="single-get-touch">
                                    <label for="password">Password</label>
                                    <input type="password" name="password" id="password" placeholder="" required="required">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="single-get-touch">
                                    <label for="confirm_password">Confirm Password</label>
                                    <input type="password" name="confirm_password" id="confirm_password" placeholder="" required="required">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="single-get-check">
                                    <label class="radiobox"> Male
                                        <input type="radio" name="gender" value="Male" <?php echo $this->session->flashdata('gender') == 'Male' ? 'checked="checked"' : ''; ?> required="required">
                                        <span class="checkmark"></span>
                                    </label>
                                    <label class="radiobox">Female
                                        <input type="radio" name="gender" value="Female" <?php echo $this->session->flashdata('gender') == 'Female' ? 'checked="checked"' : ''; ?> required="required">
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="single-get-touch">
                                    <p>Submit the word you see below:</p>
                                    <?php echo $captcha_image; ?>
                                    <input type="text" name="captcha" id="captcha" value=""  placeholder="Enter Captcha*" class="form-control" required="required" />
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="single-get-check">
                                    <label class="radiobox" for="terms"> <a href="Terms-And-Conditions" target="_blank">Read our Terms and Condition</a>
                                        <input type="radio" id="terms" name="terms">
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="single-get-touch">
                                    <button type="submit" name="submit" class="btn btn-default btn-sm">Apply Now</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

    <?php $this->load->view('footer'); echo "\n"; ?>

    <script type="text/javascript">

        function validate()
        {
            var password = document.getElementById("password").value;
            var confirm_password = document.getElementById("confirm_password").value;
            if(password != confirm_password ){
                $('#confirm_password').parent().parent().addClass('has-error');

                document.getElementById("confirm_password").focus();
                alert('Passwords do not match.');
                return false;
            }
            else if(document.getElementById("terms").checked == false)
            {
                alert('You MUST accept our Terms and Conditions in order to apply for a loan.');
                return false;
            }
            else {
                $(':input[type="submit"]').prop('disabled', true);
                $('button[type="submit"]').prop('disabled', true);
                return true;
            }
        }
    </script>
</body>

</html>