
<section class="breadcrumb_area">
    <img class="breadcrumb_shap" src="<?php echo $this->config->base_url(); ?>assets/img/breadcrumb/banner_bg.png" alt="">
    <div class="container">
        <div class="breadcrumb_content text-center">
            <h1 class="f_p f_700 f_size_50 w_color l_height50 mb_20">Sign Up</h1>
            <p class="f_400 w_color f_size_16 l_height26">Why I say old chap that is spiffing off his nut arse pear shaped plastered<br> Jeffrey bodge barney some dodgy.!!</p>
        </div>
    </div>
</section>
<section class="sign_in_area bg_color sec_pad">
    <div class="container">
        <div class="sign_info">
            <div class="row">
                <div class="col-lg-5">
                    <div class="sign_info_content">
                        <h3 class="f_p f_600 f_size_24 t_color3 mb_40">Already have an account?</h3>
                        <h2 class="f_p f_400 f_size_30 mb-30">Login now and<br> start using our <br><span class="f_700">amazing</span> products</h2>
                        <ul class="list-unstyled mb-0">
                            <li><i class="ti-check"></i> Premium Access to all Products</li>
                            <li><i class="ti-check"></i> Free Testing Tools</li>
                            <li><i class="ti-check"></i> Unlimited User Accounts</li>
                        </ul>
                        <a href="login" class="btn_three sign_btn_transparent">Sign In</a>
                    </div>
                </div>
                <div class="col-lg-7">
                    <div class="login_info">
                        <h2 class="f_p f_600 f_size_24 t_color3 mb_40">Sign Up</h2>
                        <?php if($error_code == 0 && !empty($error)): ?>

                            <div class="alert alert-success alert-dismissable fade show" role="alert">
                                <strong>Success!</strong> <?php echo $error; ?>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <?php elseif($error_code == 1 && !empty($error)): ?>
                                <div class="alert alert-danger alert-dismissable fade show" role="alert">
                                    <strong>Error!</strong> <?php echo $error; ?>
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <?php else: ?>
                                <?php endif; ?>
                                <?php echo form_open('register/secure', 'onsubmit="return validate();" class="login-form sign-in-form"'); ?>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group text_box">
                                            <label class="f_p text_c f_400">First name</label>
                                            <input placeholder="First name" type="text" name="first_name" id="first_name" value="<?php echo $this->session->flashdata('first_name'); ?>" maxlength="255" aria-required="true" aria-invalid="false" required="required" />
                                            <i class="fa fa-user"></i>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group text_box">
                                            <label class="f_p text_c f_400">Last name</label>
                                            <input placeholder="Last name" type="text" name="last_name" id="last_name" value="<?php echo $this->session->flashdata('last_name'); ?>" maxlength="255" aria-required="true" aria-invalid="false" required="required" />
                                            <i class="fa fa-user"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group text_box">
                                    <label class="f_p text_c f_400">Email Address</label>
                                    <input placeholder="Email" type="email" name="email" id="email" value="<?php echo $this->session->flashdata('email'); ?>" maxlength="255" aria-required="true" aria-invalid="false" required="required" />
                                    <i class="fa fa-envelope-o"></i>
                                </div>
                                <div class="form-group text_box">
                                    <label class="f_p text_c f_400">Password</label>
                                    <input placeholder="Password" type="password" name="password" id="password" value="" maxlength="255" aria-required="true" aria-invalid="false" required="required" />
                                    <i class="fa fa-lock"></i>
                                </div>
                                <div class="form-group text_box">
                                    <label class="f_p text_c f_400">Confirm Password</label>
                                    <input placeholder="Confirm Password" type="password" name="confirm_password" id="confirm_password" value="" maxlength="255" aria-required="true" aria-invalid="false" required="required" />
                                    <i class="fa fa-lock"></i>
                                </div>
                                <div class="extra mb_20">
                                    <div class="checkbox remember">
                                        <label>
                                            <input type="checkbox" id="terms" name="terms"> I agree to the <a href="Terms-And-Conditions" target="_blank">terms &amp; conditions</a> of this website
                                        </label>
                                    </div>
                                </div>
                                <div class="d-flex justify-content-between align-items-center">
                                    <button type="submit" class="btn_three" id="submitt">Sign Up</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <?php $this->load->view('footer'); echo "\n"; ?>


    <script>

        function validate()
        {
            if(document.getElementById("terms").checked == false)
            {
                alert('You MUST accept our Terms and Conditions in order to proceed.');
                return false;
            }
            else
            {
                $(':input[type="submit"]').prop('disabled', true);
                $('button[type="submit"]').prop('disabled', true);
                $('#submit').html('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>');
                return true;
            }
        }

    </script>

</body>

</html>



