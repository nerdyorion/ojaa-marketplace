
        <section class="error_area">
            <img class="error_shap" src="<?php echo $this->config->base_url(); ?>assets/img/breadcrumb/banner_bg.png" alt="Image">
            <div class="container flex">
                <div class="error_contain text-center">
                    <div class="b_text">
                        <h1 class="f_p w_color f_700">404</h1>
                    </div>
                    <h2 class="f_p f_400 w_color f_size_40">page not found</h2>
                    <p class="w_color f_400">We’re sorry, the page you have looked for does not exist. Maybe<br> go to our home page</p>
                    <a href="<?php echo $this->config->base_url(); ?>" class="about_btn btn_hover mt_40">Go Back to home Page</a>
                </div>
            </div>
        </section>
    
<?php $this->load->view('footer'); echo "\n"; ?>

</body>

</html>