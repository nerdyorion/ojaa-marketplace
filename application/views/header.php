<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
  <!-- Basic Page Needs -->
  <meta charset="utf-8">
  <!--[if IE]><meta http-equiv="x-ua-compatible" content="IE=9" /><![endif]-->
  <title><?php echo $page_title; ?> | <?php echo $this->config->item('app_name'); ?></title>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="author" content="Ciitmaur" />

  <base href="<?php echo base_url();?>">

  <!-- Mobile Specific Metas -->
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />


  <!-- Open Graph Tags (For Facebook, WhatsApp, etc) -->
  <meta property="og:type" content="website" />
  <meta property="og:title" content="<?php echo $this->config->item('app_name'); ?>" />
  <meta property="og:description" content="<?php echo ellipsize($meta_description, 61); ?>" />
  <meta property="og:url" content="<?php echo $this->config->base_url(); ?>" />
  <meta property="og:site_name" content="<?php echo $this->config->item('app_name'); ?>" />
  <meta property="og:image" content="<?php echo $this->config->base_url(); ?>assets/images/logo-300x200.png" />
  <meta property="og:locale" content="en_US" />
  <meta name="twitter:card" content="summary" />


  <!-- For Google -->
  <meta name="description" content="<?php echo $meta_description; ?>" />
  <meta name="keywords" content="<?php echo $meta_tags; ?>" />
  <meta name="copyright" content="<?php echo $this->config->base_url(); ?>Terms-And-Conditions" />
  <meta name="application-name" content="<?php echo $this->config->item('app_name'); ?>" />

  <!-- For Twitter -->
  <meta name="twitter:card" content="summary" />
  <meta name="twitter:title" content="<?php echo $this->config->item('app_name'); ?>" />
  <meta name="twitter:description" content="<?php echo $meta_description; ?>" />
  <meta name="twitter:image" content="<?php echo $this->config->base_url(); ?>assets/images/logo-300x200.png" />

  
  <!-- Favicon -->
  <link rel="shortcut icon" type="image/x-icon" href="<?php echo $this->config->base_url(); ?>assets/img/favicon.png">
  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>assets/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>assets/vendors/bootstrap-selector/css/bootstrap-select.min.css">
  <!--icon font css-->
  <link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>assets/vendors/themify-icon/themify-icons.css">
  <link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>assets/vendors/flaticon/flaticon.css">
  <link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>assets/vendors/animation/animate.css">
  <link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>assets/vendors/owl-carousel/assets/owl.carousel.min.css">
  <link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>assets/vendors/magnify-pop/magnific-popup.css">
  <link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>assets/vendors/nice-select/nice-select.css">
  <link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>assets/vendors/scroll/jquery.mCustomScrollbar.min.css">
  <link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>assets/vendors/elagent/style.css">
  <link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>assets/css/style.css">
  <link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>assets/css/responsive.css">

  <?php 
  $path = parse_url(current_url(), PHP_URL_PATH);
  $pathFragments = explode('/', $path);
  $end = strtolower(end($pathFragments));
  ?>

  <?php if($end =='profile'): ?>
  <?php endif; ?>
  <?php if($end =='/' || empty($end)): ?>
  <?php endif; ?>
</head>
<body>
    <div id="preloader">
        <div id="ctn-preloader" class="ctn-preloader dark">
            <div class="animation-preloader">
                <div class="spinner"></div>
                <div class="txt-loading">
                    <span data-text-preloader="O" class="letters-loading">
                        O
                    </span>
                    <span data-text-preloader="J" class="letters-loading">
                        J
                    </span>
                    <span data-text-preloader="A" class="letters-loading">
                        A
                    </span>
                    <span data-text-preloader="A" class="letters-loading">
                        A
                    </span>
                </div>
                <p class="text-center">Loading</p>
            </div>
            <div class="loader dark_bg">
                <div class="row">
                    <div class="col-3 loader-section section-left">
                        <div class="bg"></div>
                    </div>
                    <div class="col-3 loader-section section-left">
                        <div class="bg"></div>
                    </div>
                    <div class="col-3 loader-section section-right">
                        <div class="bg"></div>
                    </div>
                    <div class="col-3 loader-section section-right">
                        <div class="bg"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="body_wrapper">
        <header class="header_area">
            <nav class="navbar navbar-expand-lg menu_six">
                <div class="container custom_container p0">
                    <a class="navbar-brand sticky_logo" href="<?php echo $this->config->base_url(); ?>"><img src="<?php echo $this->config->base_url(); ?>assets/img/logo2.png" srcset="<?php echo $this->config->base_url(); ?>assets/img/logo2x-2.png 2x" alt="<?php echo $this->config->item('app_name'); ?>"><img src="<?php echo $this->config->base_url(); ?>assets/img/logo.png" srcset="<?php echo $this->config->base_url(); ?>assets/img/logo2x.png 2x" alt="<?php echo $this->config->item('app_name'); ?>"></a>
                    <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="menu_toggle">
                            <span class="hamburger">
                                <span></span>
                                <span></span>
                                <span></span>
                            </span>
                            <span class="hamburger-cross">
                                <span></span>
                                <span></span>
                            </span>
                        </span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav menu ml-auto mr-auto">
                            <li class="nav-item <?php echo empty($end) || $end =='ojaa' ? 'active' : ''; ?>"><a class="nav-link" title="Home" href="<?php echo $this->config->base_url(); ?>" role="button" aria-expanded="false">Home</a></li>
                            <li class="nav-item <?php echo $end =='features' ? 'active' : ''; ?>"><a title="Features" class="nav-link" role="button" aria-expanded="false" href="#">Features</a></li>
                            <li class="nav-item" <?php echo $end =='pricing' ? 'active' : ''; ?>><a title="Pricing" class="nav-link" role="button" aria-expanded="false" href="#">Pricing</a></li>
                            <li class="nav-item" <?php echo $end =='faqs' ? 'active' : ''; ?>><a title="FAQs" class="nav-link" role="button" aria-expanded="false" href="#">FAQs</a></li>
                            <li class="nav-item" <?php echo $end =='about' ? 'active' : ''; ?>><a title="About" class="nav-link" role="button" aria-expanded="false" href="#">About</a></li>
                            <li class="nav-item" <?php echo $end =='contact' ? 'active' : ''; ?>><a title="Contact" class="nav-link" role="button" aria-expanded="false" href="#">Contact</a></li>
                          </ul>
                          <?php if(is_logged_in()): ?>
                            <a class="btn_get cus_dark" href="dashboard">My Account</a>
                            <?php else: ?>

                              <a class="btn_get cus_dark" href="register?plan=free-trial">Free Trial</a>
                              <a class="btn_get cus_dark" href="login">Sign In</a>
                            <?php endif; ?>
                          </div>
                        </div>
            </nav>
        </header>




















<!-- 

  <section class="gauto-header-top-area">
   <div class="container">
    <div class="row">
     <div class="col-md-6">
      <div class="header-top-left">
       <p>Need Help?: <i class="fa fa-phone"></i> Call: <?php echo $this->config->item("info_phone"); ?></p>
     </div>
   </div>
   <div class="col-md-6">
    <div class="header-top-right">

      <?php if(is_logged_in()): ?>
       <a href="dashboard"><i class="fa fa-home"></i> my account</a>
       <?php else: ?>
         <a href="login"><i class="fa fa-key"></i> login</a>
         <a href="register"><i class="fa fa-user"></i> register</a>
       <?php endif; ?>
                    </div>
                  </div>
                </div>
              </div>
            </section>


            <header class="gauto-main-header-area">
             <div class="container">
              <div class="row">
               <div class="col-md-3">
                <div class="site-logo">
                 <a href="<?php echo $this->config->base_url(); ?>">
                   <img src="<?php echo $this->config->base_url(); ?>assets/img/logo.png" alt="gauto" />
                 </a>
               </div>
             </div>
             <div class="col-lg-6 col-sm-9">
              <div class="header-promo">
               <div class="single-header-promo">
                <div class="header-promo-icon">
                 <img src="<?php echo $this->config->base_url(); ?>assets/img/globe.png" alt="globe" />
               </div>
               <div class="header-promo-info">
                 <h3>Mauritius</h3>
                 <p>Addis Ababa, Ethiopa</p>
               </div>
             </div>
             <div class="single-header-promo">
              <div class="header-promo-icon">
               <img src="<?php echo $this->config->base_url(); ?>assets/img/clock.png" alt="clock" />
             </div>
             <div class="header-promo-info">
               <h3>Monday to Friday</h3>
               <p>9:00am - 6:00pm</p>
             </div>
           </div>
         </div>
       </div>
       <div class="col-lg-3">
        <div class="header-action">
         <a href="contact"><i class="fa fa-phone"></i> Drop a message</a>
       </div>
     </div>
   </div>
 </div>
</header>


<section class="gauto-mainmenu-area">
 <div class="container">
  <div class="row">
   <div class="col-lg-9">
    <div class="mainmenu">
     <nav>
      <ul id="gauto_navigation">
       <li class="<?php echo empty($end) || $end =='martinscar' ? 'active' : ''; ?>"><a href="<?php echo $this->config->base_url(); ?>">home</a></li>
       <li class="<?php echo $end =='about' ? 'active' : ''; ?>"><a href="about">About Us</a></li>
       <li class="<?php echo $end =='cars' ? 'active' : ''; ?>"><a href="cars">Car Listing</a></li>
       <li class="<?php echo $end =='contact' ? 'active' : ''; ?>"><a href="contact">Contact Us</a></li>
       <?php if(is_logged_in()): ?>
        <li class=""><a href="dashboard">My Account</a></li>
        <?php else: ?>
         <li class="<?php echo $end =='login' ? 'active' : ''; ?>"><a href="login">Login</a></li>
         <li class="<?php echo $end =='register' ? 'active' : ''; ?>"><a href="register">Register</a></li>
       <?php endif; ?>
     </ul>
   </nav>
 </div>
</div>
<div class="col-lg-3 col-sm-12">
  <div class="main-search-right">
   <div class="gauto-responsive-menu"></div>

   <div class="search-box">
     <?php echo form_open('cars/index', 'method="get"'); ?>
     <input type="search" name="title" placeholder="Search" value="<?php echo isset($_GET['title']) ? trim($_GET['title']) : ''; ?>">
     <button type="submit"><i class="fa fa-search"></i></button>
   </form>
 </div>

</div>
</div>
</div>
</div>
</section> -->
