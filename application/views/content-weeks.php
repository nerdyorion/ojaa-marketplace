<hr />
<section class="about-page-shortcode section-padding" style="margin-bottom: 100px;">
  <div class="container rowpadding-with-border">
    <div class="row">
      <?php $this->load->view('side-nav-menu'); echo "\n"; ?>
      <div class="col-lg-9">
        <div class="row">
          <div class="col-lg-12">
            <div class="about-content">
              <h2><?php echo $package['type']; ?></h2>
              <hr />

              <nav aria-label="breadcrumb">
                <ol class="breadcrumb" style="margin-left:0;">
                  <li class="breadcrumb-item"><a href="<?php echo $this->config->base_url(); ?>dashboard">Home</a></li>
                  <li class="breadcrumb-item active" aria-current="page" id="bread-current"><?php echo $package['type']; ?></li>
                </ol>
              </nav>

              <?php if($error_code == 0 && !empty($error)): ?>

                <div class="alert alert-success alert-dismissable fade show" role="alert">
                  <strong>Success!</strong> <?php echo $error; ?>
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <?php elseif($error_code == 1 && !empty($error)): ?>
                  <div class="alert alert-danger alert-dismissable fade show" role="alert">
                    <strong>Error!</strong> <?php echo $error; ?>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <?php else: ?>
                  <?php endif; ?>

                  <p>Choose week below:</p>
                  <div class="list-group">
                    <li class="list-group-item bg-primary text-white">Weeks</li>
                    <?php for($i=1; $i <= 12; $i++): ?>
                    <a href="content/index/<?php echo $package['id']; ?>/<?php echo $i; ?>" class="list-group-item list-group-item-action">Week <?php echo $i; ?></a>
                    <?php endfor; ?>
                  </div> 

                </div>
              </div>
            </div>


          </div>
        </div>
      </div>
    </section>
    <!-- end of about section -->

    <?php $this->load->view('footer'); echo "\n"; ?>

  </body>

  </html>