<style type="text/css">
  .nocp {
    -moz-user-select: none;  
-webkit-user-select: none;  
-ms-user-select: none;  
-o-user-select: none;  
user-select: none;
  }
</style>

<hr />
<section class="about-page-shortcode section-padding" style="margin-bottom: 100px;">
  <div class="container rowpadding-with-border">
    <div class="row">
      <?php $this->load->view('side-nav-menu'); echo "\n"; ?>
      <div class="col-lg-9">
        <div class="row">
          <div class="col-lg-12">
            <div class="about-content">
              <h2><?php echo "Week $week_id (" . $package['type'] . ')'; ?></h2>
              <hr />

              <nav aria-label="breadcrumb">
                <ol class="breadcrumb" style="margin-left:0;">
                  <li class="breadcrumb-item"><a href="<?php echo $this->config->base_url(); ?>dashboard">Home</a></li>
                  <li class="breadcrumb-item"><a href="<?php echo $this->config->base_url() . 'package/index/' . $package['id']; ?>"><?php echo $package['type']; ?></a></li>
                  <li class="breadcrumb-item active" aria-current="page" id="bread-current"><?php echo "Week $week_id"; ?></li>
                </ol>
              </nav>

              <?php if($error_code == 0 && !empty($error)): ?>

                <div class="alert alert-success alert-dismissable fade show" role="alert">
                  <strong>Success!</strong> <?php echo $error; ?>
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <?php elseif($error_code == 1 && !empty($error)): ?>
                  <div class="alert alert-danger alert-dismissable fade show" role="alert">
                    <strong>Error!</strong> <?php echo $error; ?>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <?php else: ?>
                  <?php endif; ?>

                  <?php $colors = array("483D8B", "9400D3", "FF1493", "007bff", "6610f2", "6f42c1", "e83e8c", "dc3545", "fd7e14", "ffc107", "28a745", "20c997", "17a2b8", "6c757d", "343a40", "007bff", "6c757d", "28a745", "17a2b8", "ffc107", "dc3545", "f8f9fa", "343a40", );
                  ?>
                  <div class="container nocp">
                    <div class="card-columns">

                      <?php if($contents): ?>
                      <?php foreach($contents as $content): ?>
                        <div class="card">
                          <img class="card-img-top" src="http://via.placeholder.com/200x50/<?php echo $colors[array_rand($colors)]; ?>/ffffff?text=." alt="Image">
                          <div class="card-body">
                            <h5 class="card-title"><?php echo $subjects[getArrayKey($subjects, "id", $content['subject_id'])]['value']; ?></h5>
                            <p class="card-text"><?php echo $content['content']; ?></p>
                          </div>
                        </div>
                      <?php endforeach; ?>

                      <?php else: ?>
                        <p >Contents would be available shortly.</p>
                      <?php endif; ?>

                    </div>

                    <?php if($contents): ?>
                      <ul class="pagination pagination-sm pull-right">
                        <li class="page-item" <?php echo $week_id == 1 ? 'disabled' : ''; ?>><a class="page-link" href="<?php echo $week_id == 1 ? 'javascript:;' : $this->config->base_url() . 'content/index/' . $package['id'] . '/' . ($week_id - 1); ?>">Previous</a></li>
                        <li class="page-item <?php echo $week_id == 12 ? 'disabled' : ''; ?>"><a class="page-link" href="<?php echo $week_id == 12 ? 'javascript:;' : $this->config->base_url() . 'content/index/' . $package['id'] . '/' . ($week_id + 1); ?>">Next</a></li>
                      </ul>
                    <?php endif; ?>
                  </div>

                  <!-- <div class="container">
                    <div class="card-columns">
                      <div class="card">
                        <img class="card-img-top" src="http://via.placeholder.com/1600x900/483D8B/ffffff?text=Card+1" alt="Card image cap">
                        <div class="card-body">
                          <h5 class="card-title">Card title that wraps to a new line</h5>
                          <p class="card-text">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                        </div>
                      </div>
                      <div class="card p-3">
                        <blockquote class="blockquote mb-0 card-body">
                          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                          <footer class="blockquote-footer">
                            <small class="text-muted">
                              Someone famous in <cite title="Source Title">Source Title</cite>
                            </small>
                          </footer>
                        </blockquote>
                      </div>
                      <div class="card">
                        <img class="card-img-top" src="http://via.placeholder.com/1600x450/9400D3/ffffff?text=Card+2" alt="Card image cap">
                        <div class="card-body">
                          <h5 class="card-title">Card title</h5>
                          <p class="card-text">This card has supporting text below as a natural lead-in to additional content.</p>
                          <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                        </div>
                      </div>
                      <div class="card bg-primary text-white text-center p-3">
                        <blockquote class="blockquote mb-0">
                          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat.</p>
                          <footer class="blockquote-footer">
                            <small>
                              Someone famous in <cite title="Source Title">Source Title</cite>
                            </small>
                          </footer>
                        </blockquote>
                      </div>
                      <div class="card text-center">
                        <div class="card-body">
                          <h5 class="card-title">Card title</h5>
                          <p class="card-text">This card has supporting text below as a natural lead-in to additional content.</p>
                          <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                        </div>
                      </div>
                      <div class="card">
                        <img class="card-img" src="http://via.placeholder.com/1600x1600/FF1493/ffffff?text=Card+3" alt="Card image">
                      </div>
                      <div class="card p-3 text-right">
                        <blockquote class="blockquote mb-0">
                          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                          <footer class="blockquote-footer">
                            <small class="text-muted">
                              Someone famous in <cite title="Source Title">Source Title</cite>
                            </small>
                          </footer>
                        </blockquote>
                      </div>
                      <div class="card">
                        <div class="card-body">
                          <h5 class="card-title">Card title</h5>
                          <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This card has even longer content than the first to show that equal height action.</p>
                          <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                        </div>
                      </div>
                    </div>
                  </div> -->

                </div>
              </div>
            </div>


          </div>
        </div>
      </div>
    </section>
    <!-- end of about section -->

    <?php $this->load->view('footer'); echo "\n"; ?>

  </body>

  </html>