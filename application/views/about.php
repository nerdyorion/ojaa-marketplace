

      <!-- Breadcromb Area Start -->
      <section class="gauto-breadcromb-area section_70">
         <div class="container">
            <div class="row">
               <div class="col-md-12">
                  <div class="breadcromb-box">
                     <h3>About Us</h3>
                     <ul>
                        <li><i class="fa fa-home"></i></li>
                        <li><a href="<?php echo $this->config->base_url(); ?>">Home</a></li>
                        <li><i class="fa fa-angle-right"></i></li>
                        <li>About Us</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <!-- Breadcromb Area End -->
       
       
      <!-- About Page Area Start -->
      <section class="about-page-area section_70">
         <div class="container">
            <div class="row">
               <div class="col-lg-6">
                  <div class="about-page-left">
                     <h4>About Us</h4>
                     <h3>We are committed to provide safe Ride solutions</h3>
                     <p><span>since 2003</span>, we are committed to delivering cheap, quality cars to students.</p>
                     <p>We have expanded our reach and capacity to cater for our increasing demands.</p>
                     <div class="about-page-call">
                        <div class="page-call-icon">
                           <i class="fa fa-phone"></i>
                        </div>
                        <div class="call-info">
                           <p>Need any Help?</p>
                           <h4><a href="contact"><?php echo $this->config->item("info_phone"); ?></a></h4>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-lg-6">
                  <div class="about-page-right">
                     <img src="<?php echo $this->config->base_url(); ?>assets/img/about-page.jpg" alt="about page" />
                  </div>
               </div>
            </div>
         </div>
      </section>
      <!-- About Page Area End -->
       
       
      <!-- About Promo Area Start -->
      <section class="gauto-about-promo section_70">
         <div class="container">
            <div class="row">
               <div class="col-md-12">
                  <div class="about-promo-text">
                     <h3>We are proud of our business. <span>Rent Car</span> Now!</h3>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-md-12">
                  <div class="about-promo-image">
                     <img src="<?php echo $this->config->base_url(); ?>assets/img/cars.png" alt="about promo" />
                  </div>
               </div>
            </div>
         </div>
      </section>
      <!-- About Promo Area End -->
       

    <?php $this->load->view('footer'); echo "\n"; ?>

</body>

</html>