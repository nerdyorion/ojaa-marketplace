
        <footer class="footer_area_six sec_pad">
            <div class="footer_top_six">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-4 col-sm-6">
                            <div class="f_widget company_widget">
                                <a href="index.html" class="f-logo"><img src="<?php echo $this->config->base_url(); ?>assets/img/logo3.png" srcset="<?php echo $this->config->base_url(); ?>assets/img/logo-3-2x.png 2x" alt="<?php echo $this->config->item('app_name'); ?>"></a>
                                <p class="mt_40">Copyright <?php echo date('Y'); ?> &copy; <span class="text-secondary"><?php echo $this->config->item('app_name'); ?></span></p>
                            </div>
                        </div>
                        <div class="col-lg-3 col-sm-6">
                            <div class="f_widget about-widget">
                                <h3 class="f-title f_600 w_color f_size_18 mb_40">About Us</h3>
                                <ul class="list-unstyled f_list">
                                    <li><a href="#">Company</a></li>
                                    <li><a href="#">Android App</a></li>
                                    <li><a href="#">ios App</a></li>
                                    <li><a href="#">Desktop</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-3 col-sm-6">
                            <div class="f_widget about-widget">
                                <h3 class="f-title f_600 w_color f_size_18 mb_40">Help?</h3>
                                <ul class="list-unstyled f_list">
                                    <li><a href="#">FAQ</a></li>
                                    <li><a href="#">Privacy</a></li>
                                    <li><a href="#">Term & conditions</a></li>
                                    <li><a href="#">Reporting</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-2 col-sm-6">
                            <div class="f_widget social-widget">
                                <h3 class="f-title f_600 w_color f_size_18 mb_40">Follow Us</h3>
                                <div class="f_social_icon">
                                    <a href="#" class="ti-facebook"></a>
                                    <a href="#" class="ti-twitter-alt"></a>
                                    <a href="#" class="ti-vimeo-alt"></a>
                                    <a href="#" class="ti-pinterest"></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>

    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="<?php echo $this->config->base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>
    <script src="<?php echo $this->config->base_url(); ?>assets/js/propper.js"></script>
    <script src="<?php echo $this->config->base_url(); ?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo $this->config->base_url(); ?>assets/vendors/bootstrap-selector/js/bootstrap-select.min.js"></script>
    <script src="<?php echo $this->config->base_url(); ?>assets/vendors/wow/wow.min.js"></script>
    <script src="<?php echo $this->config->base_url(); ?>assets/vendors/sckroller/jquery.parallax-scroll.js"></script>
    <script src="<?php echo $this->config->base_url(); ?>assets/vendors/owl-carousel/owl.carousel.min.js"></script>
    <script src="<?php echo $this->config->base_url(); ?>assets/vendors/nice-select/jquery.nice-select.min.js"></script>
    <script src="<?php echo $this->config->base_url(); ?>assets/vendors/imagesloaded/imagesloaded.pkgd.min.js"></script>
    <script src="<?php echo $this->config->base_url(); ?>assets/vendors/isotope/isotope-min.js"></script>
    <script src="<?php echo $this->config->base_url(); ?>assets/vendors/magnify-pop/jquery.magnific-popup.min.js"></script>
    <script src="<?php echo $this->config->base_url(); ?>assets/vendors/scroll/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="<?php echo $this->config->base_url(); ?>assets/js/main.js"></script>























<!-- 
      <footer class="gauto-footer-area">
         <div class="footer-top-area">
            <div class="container">
               <div class="row">
                  <div class="col-lg-4">
                     <div class="single-footer">
                        <div class="footer-logo">
                           <a href="<?php echo $this->config->base_url(); ?>">
                           <img src="<?php echo $this->config->base_url(); ?>assets/img/footer-logo.png" alt="footer-logo" />
                           </a>
                        </div>
                        <p>Your one-stop student car rental service.</p>
                     </div>
                  </div>
                  <div class="col-lg-4">
                     <div class="single-footer quick_links">
                        <h3>Quick Links</h3>
                        <ul class="quick-links">
                           <li><a href="<?php echo $this->config->base_url(); ?>">Home</a></li>
                           <li><a href="about">About Us</a></li>
                           <li><a href="cars">Car Listing</a></li>
                           <li><a href="contact">Contact Us</a></li>
                        </ul>
                        <ul class="quick-links">
                           <li><a href="dashboard">Account</a></li>
                           <li><a href="register">Register</a></li>
                           <li><a href="login">Login</a></li>
                        </ul>
                     </div>
                  </div>
                  <div class="col-lg-4">
                     <div class="single-footer">
                        <div class="footer-address">
                           <h3>Head office</h3>
                           <p><?php echo $this->config->item("info_address"); ?></p>
                           <ul>
                              <li>Phone: <?php echo $this->config->item("info_phone"); ?></li>
                              <li>Email: <?php echo $this->config->item("info_email"); ?></li>
                              <li>Office Time: 9AM- 4PM</li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="footer-bottom-area">
            <div class="container">
               <div class="row">
                  <div class="col-md-6">
                     <div class="copyright">
                        <p>Copyright <?php echo date('Y'); ?> &copy; <span class="text-secondary"><?php echo $this->config->item('app_name'); ?></span>. Design With <i class="fa fa-heart"></i> by <a href="#">Martins</a></p>
                     </div>
                  </div>
                  <div class="col-md-6">
                     <div class="footer-social">
                        <ul>
                           <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                           <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                           <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                           <li><a href="#"><i class="fa fa-skype"></i></a></li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </footer>


      <script src="<?php echo $this->config->base_url(); ?>assets/js/jquery.min.js"></script>
      <script src="<?php echo $this->config->base_url(); ?>assets/js/popper.min.js"></script>
      <script src="<?php echo $this->config->base_url(); ?>assets/js/bootstrap.min.js"></script>
      <script src="<?php echo $this->config->base_url(); ?>assets/js/owl.carousel.min.js"></script>
      <script src="<?php echo $this->config->base_url(); ?>assets/js/lightgallery-all.js"></script>
      <script src="<?php echo $this->config->base_url(); ?>assets/js/custom_lightgallery.js"></script>
      <script src="<?php echo $this->config->base_url(); ?>assets/js/jquery.slicknav.min.js"></script>
      <script src="<?php echo $this->config->base_url(); ?>assets/js/jquery.magnific-popup.min.js"></script>
      <script src="<?php echo $this->config->base_url(); ?>assets/js/jquery.nice-select.min.js"></script>
      <script src="<?php echo $this->config->base_url(); ?>assets/js/jquery.datepicker.min.js"></script>
      <script src="<?php echo $this->config->base_url(); ?>assets/js/jquery-clockpicker.min.js"></script>
      <script src="<?php echo $this->config->base_url(); ?>assets/js/main.js"></script> -->