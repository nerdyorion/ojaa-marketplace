
    <!-- BANNER -->
    <div class="section banner-page" data-background="<?php echo $this->config->base_url(); ?>assets/images/bg1-min.jpg">
        <div class="content-wrap pos-relative">
            <div class="container">
                <div class="col-12 col-md-12">
                    <div class="d-flex bd-highlight mb-2">
                        <div class="title-page">Services</div>
                    </div>
                    <nav aria-label="breadcrumb">
                      <ol class="breadcrumb ">
                        <li class="breadcrumb-item"><a href="./">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Services</li>
                      </ol>
                    </nav>
                </div>
            </div>
            
        </div>
    </div>


    <!-- SERVICES -->
    <div class="section">
        <div class="content-wrap">
            <div class="container">
                <div class="row">
                    <!-- Item 1 -->
                    <div class="col-sm-12 col-md-12 col-lg-4">
                        <div class="feature-box-7 shadow">
                            <div class="media-box">
                                <a href="#"><img src="<?php echo $this->config->base_url(); ?>assets/images/electrical.jpg" alt="" class="img-fluid"></a>
                            </div>
                            <div class="body">
                                <div class="info-box">
                                    <div class="text">
                                        <div class="title">Electrical repairs and installations</div>
                                        <p>Electrical repairs and installations </p>
                                        <!--<a href="#" title="Get Detail">Get Detail</a>-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Item 2 -->
                    <div class="col-sm-12 col-md-12 col-lg-4">
                        <div class="feature-box-7 shadow">
                            <div class="media-box">
                                <a href="#"><img src="<?php echo $this->config->base_url(); ?>assets/images/plumbing.jpg" alt="" class="img-fluid"></a>
                            </div>
                            <div class="body">
                                <div class="info-box">
                                    <div class="text">
                                        <div class="title">Plumbing works</div>
                                        <p>Plumbing works </p>
                                        <!--<a href="#" title="Get Detail">Get Detail</a>-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Item 3 -->
                    <div class="col-sm-12 col-md-12 col-lg-4">
                        <div class="feature-box-7 shadow">
                            <div class="media-box">
                                <a href="#"><img src="<?php echo $this->config->base_url(); ?>assets/images/bathroom-shower-floor-tiling.jpg" alt="" class="img-fluid"></a>
                            </div>
                            <div class="body">
                                <div class="info-box">
                                    <div class="text">
                                        <div class="title">Tiling</div>
                                        <p>Tiling </p>
                                        <!--<a href="#" title="Get Detail">Get Detail</a>-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Item 4 -->
                    <div class="col-sm-12 col-md-12 col-lg-4">
                        <div class="feature-box-7 shadow">
                            <div class="media-box">
                                <a href="#"><img src="<?php echo $this->config->base_url(); ?>assets/images/ceramic.jpg" alt="" class="img-fluid"></a>
                            </div>
                            <div class="body">
                                <div class="info-box">
                                    <div class="text">
                                        <div class="title">Roof repairs and installations</div>
                                        <p>Roof repairs and installations </p>
                                        <!--<a href="#" title="Get Detail">Get Detail</a>-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Item 5 -->
                    <div class="col-sm-12 col-md-12 col-lg-4">
                        <div class="feature-box-7 shadow">
                            <div class="media-box">
                                <a href="#"><img src="<?php echo $this->config->base_url(); ?>assets/images/pop.jpg" alt="" class="img-fluid"></a>
                            </div>
                            <div class="body">
                                <div class="info-box">
                                    <div class="text">
                                        <div class="title">P.O.P and wall screeding</div>
                                        <p>P.O.P and wall screeding </p>
                                        <!--<a href="#" title="Get Detail">Get Detail</a>-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Item 6 -->
                    <div class="col-sm-12 col-md-12 col-lg-4">
                        <div class="feature-box-7 shadow">
                            <div class="media-box">
                                <a href="#"><img src="<?php echo $this->config->base_url(); ?>assets/images/painting.jpg" alt="" class="img-fluid"></a>
                            </div>
                            <div class="body">
                                <div class="info-box">
                                    <div class="text">
                                        <div class="title">Internal and external house painting</div>
                                        <p>Internal and external house painting </p>
                                        <!--<a href="#" title="Get Detail">Get Detail</a>-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Item 10 -->
                    <div class="col-sm-12 col-md-12 col-lg-4">
                        <div class="feature-box-7 shadow">
                            <div class="media-box">
                                <a href="#"><img src="<?php echo $this->config->base_url(); ?>assets/images/cctv.jpg" alt="" class="img-fluid"></a>
                            </div>
                            <div class="body">
                                <div class="info-box">
                                    <div class="text">
                                        <div class="title">CCTV camera and access control installations</div>
                                        <p>CCTV camera and access control installations </p>
                                        <!--<a href="#" title="Get Detail">Get Detail</a>-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <a name="rest"></a>
                    <!-- Item 12 -->
                    <div class="col-sm-12 col-md-12 col-lg-4">
                        <div class="feature-box-7 shadow">
                            <div class="media-box">
                                <a href="#"><img src="<?php echo $this->config->base_url(); ?>assets/images/furniture.jpg" alt="" class="img-fluid"></a>
                            </div>
                            <div class="body">
                                <div class="info-box">
                                    <div class="text">
                                        <div class="title">Furniture making</div>
                                        <p>Furniture making </p>
                                        <!--<a href="#" title="Get Detail">Get Detail</a>-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Item 11 -->
                    <div class="col-sm-12 col-md-12 col-lg-4">
                        <div class="feature-box-7 shadow">
                            <div class="media-box">
                                <a href="#"><img src="<?php echo $this->config->base_url(); ?>assets/images/epoxy.jpg" alt="" class="img-fluid"></a>
                            </div>
                            <div class="body">
                                <div class="info-box">
                                    <div class="text">
                                        <div class="title">Epoxy flooring</div>
                                        <p>Epoxy flooring </p>
                                        <!--<a href="#" title="Get Detail">Get Detail</a>-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Item 9 -->
                    <div class="col-sm-12 col-md-12 col-lg-4">
                        <div class="feature-box-7 shadow">
                            <div class="media-box">
                                <a href="#"><img src="<?php echo $this->config->base_url(); ?>assets/images/inverter.jpg" alt="" class="img-fluid"></a>
                            </div>
                            <div class="body">
                                <div class="info-box">
                                    <div class="text">
                                        <div class="title">Inverter and solar power sales installation and maintenance </div>
                                        <p>Inverter and solar power sales installation and maintenance  </p>
                                        <!--<a href="#" title="Get Detail">Get Detail</a>-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Item 13 -->
                    <div class="col-sm-12 col-md-12 col-lg-4">
                        <div class="feature-box-7 shadow">
                            <div class="media-box">
                                <a href="#"><img src="<?php echo $this->config->base_url(); ?>assets/images/electric-fence.jpg" alt="" class="img-fluid"></a>
                            </div>
                            <div class="body">
                                <div class="info-box">
                                    <div class="text">
                                        <div class="title">Electric fence installation & wireless intercom installation</div>
                                        <p>Electric fence installation & wireless intercom installation </p>
                                        <!--<a href="#" title="Get Detail">Get Detail</a>-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Item 7 -->
                    <div class="col-sm-12 col-md-12 col-lg-4">
                        <div class="feature-box-7 shadow">
                            <div class="media-box">
                                <a href="#"><img src="<?php echo $this->config->base_url(); ?>assets/images/wallpaper.jpg" alt="" class="img-fluid"></a>
                            </div>
                            <div class="body">
                                <div class="info-box">
                                    <div class="text">
                                        <div class="title">Wallpaper laying</div>
                                        <p>Wallpaper laying </p>
                                        <!--<a href="#" title="Get Detail">Get Detail</a>-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Item 8 -->
                    <div class="col-sm-12 col-md-12 col-lg-4">
                        <div class="feature-box-7 shadow">
                            <div class="media-box">
                                <a href="#"><img src="<?php echo $this->config->base_url(); ?>assets/images/aluminium.jpg" alt="Image" class="img-fluid"></a>
                            </div>
                            <div class="body">
                                <div class="info-box">
                                    <div class="text">
                                        <div class="title">Aluminum windows installation</div>
                                        <p>Aluminum windows installation </p>
                                        <!--<a href="#" title="Get Detail">Get Detail</a>-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> 

    <?php $this->load->view('footer'); echo "\n"; ?>

</body>

</html>