<section class="about-page-shortcode section-padding mt-3" style="margin-bottom: 100px;">
  <div class="container rowpadding-with-border">
    <div class="row">
      <?php $this->load->view('side-nav-menu'); echo "\n"; ?>
      <div class="col-lg-9">
        <div class="row">
          <div class="col-lg-12">
            <div class="about-content">
              <h2>Change Password</h2>
              <hr />

                            <?php if($error_code == 0 && !empty($error)): ?>

                                <div class="alert alert-success alert-dismissable fade show" role="alert">
                                    <strong>Success!</strong> <?php echo $error; ?>
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <?php elseif($error_code == 1 && !empty($error)): ?>
                                    <div class="alert alert-danger alert-dismissable fade show" role="alert">
                                        <strong>Error!</strong> <?php echo $error; ?>
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <?php else: ?>
                                    <?php endif; ?>
                  <?php echo form_open('/Change-Password', 'class="form-horizontal", onsubmit="return validate();"'); ?>
                  <div class="row list-input">
                    <div class="col-md-12 mr0">
                      <div class="form-group">
                        <label class="mb-10" for="current_password">Current Password* </label>
                        <input id="current_password" name="current_password" class="web form-control" type="password" placeholder="" required="required" value="" />
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group">
                        <label class="mb-10" for="new_password">New Password* </label>
                        <input id="new_password" name="new_password" class="web form-control" type="password" placeholder="" required="required" value="" />
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group">
                        <label class="mb-10" for="confirm_password">Confirm Password* </label>
                        <input id="confirm_password" name="confirm_password" class="web form-control" type="password" placeholder="" required="required" value="" />
                      </div>
                    </div>

                    <div class="col-md-12">
                      <div class="form-group">
                        <button type="submit" name="submit" class="btn btn-default btn-sm" data-loading-text="<i class='fa fa-spinner fa-spin'></i> <span>Update Password</span> <i class='fa fa-check'></i>">Update Password <i class="fa fa-check"></i></button>
                      </div>
                    </div>
                  </div>
                </form>


              </div>
            </div>
          </div>


        </div>
      </div>
    </div>
  </section>
  <!-- end of about section -->

  <?php $this->load->view('footer'); echo "\n"; ?>

  <script type="text/javascript">
    function validate()
    {

      var current_password = document.getElementById("current_password").value;
      var new_password = document.getElementById("new_password").value;
      var confirm_password = document.getElementById("confirm_password").value;
      if(new_password != confirm_password ){
        alert('Passwords do not match.');
        document.getElementById("confirm_password").focus();
        return false;
      }
      var $btn = $('button[type="submit"]').button('loading');
      $(':input[type="submit"]').prop('disabled', true);
      $('button[type="submit"]').prop('disabled', true);
      $('#submit').html('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>');
      return true;
    }
  </script>

</body>

</html>