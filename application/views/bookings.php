<section class="about-page-shortcode section-padding mt-3" style="margin-bottom: 100px;">
  <div class="container rowpadding-with-border">
    <div class="row">
      <?php $this->load->view('side-nav-menu'); echo "\n"; ?>
      <div class="col-lg-9">
        <div class="row">
          <div class="col-lg-12">
            <div class="about-content">
              <h2>Bookings</h2>
              <hr />

                            <?php if($error_code == 0 && !empty($error)): ?>

                                <div class="alert alert-success alert-dismissable fade show" role="alert">
                                    <strong>Success!</strong> <?php echo $error; ?>
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <?php elseif($error_code == 1 && !empty($error)): ?>
                                    <div class="alert alert-danger alert-dismissable fade show" role="alert">
                                        <strong>Error!</strong> <?php echo $error; ?>
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <?php else: ?>
                                    <?php endif; ?>
                  <?php if(empty($rows)): ?>
                    <p>You have not booked any car yet! <br /></p>
                  <?php endif; ?>
                  <p><a href="cars" class="button btn btn-default ">&nbsp;<i class="fa fa-car"></i> Book now&nbsp;</a></p>

                  <?php if(empty($rows)): ?>
                  </p>
                  <?php else: ?>
                    <div class="table-responsive">
                      <table class="table">
                        <thead>
                          <tr>
                            <th>#</th>
                            <th>ID</th>
                            <th>Car</th>
                            <th>Pickup Date</th>
                            <th>Pickup Location</th>
                            <th>Return Date</th>
                            <th>Amount</th>
                            <th>Payment Type</th>
                            <th>Date</th>
                            <th>Status</th>
                            <!-- <th>Actions</th> -->
                          </tr>
                        </thead>
                        <tbody>
                          <?php foreach($rows as $row): ?>
                           <tr>
                            <td><?php echo $sn++; ?></td>
                            <td><?php echo $row['id']; ?></td>
                            <td class="description"><?php echo dashIfEmpty($row['name']); ?></td>
                            <td class="description"><?php echo dashIfEmpty($row['pickup_date']); ?></td>
                            <td class="description"><?php echo dashIfEmpty($row['pickup_location']); ?></td>
                            <td class="description"><?php echo dashIfEmpty($row['return_date']); ?></td>
                            <td class="price">$<?php echo number_format($row['amount'], 2); ?></td>
                            <td class="description"><?php echo dashIfEmpty($row['payment_type']); ?></td>
                          <td class="price"><?php echo dateEmpty($row['date_created']) ? "-" : date('M d, Y h:i A', strtotime($row['date_created'])); ?></td>

                          <td class="price">
                            <b>
                              <?php if($row['approved'] == 0): ?>
                                <mark class="bg-dark text-white">PENDING APPROVAL</mark>
                              <?php elseif($row['approved'] == 1): ?>
                                <mark class="bg-warning text-white">APPROVED</mark>
                              <?php else: ?>
                              <?php endif; ?>
                            </b>
                          </td>

                          <!-- <td class="price">
                            <?php if($row['approved'] == 1): ?>
                              <a href="pay/<?php echo $row['id']; ?>" data-toggle="tooltip" data-original-title="Make Payment" target="_blank"> <i class="fa fa-money text-inverse m-r-10"></i> Make Payment </a> 
                            <?php endif; ?>
                          </td> -->
                        </tr>
                      <?php endforeach; ?>
                    </tbody>
                  </table>
                </div>
              <?php endif; ?>
              <div class=" pull-right">

                <nav aria-label="...">
                  <ul class="pagination">
                    <?php echo $links; ?>
                  </ul>
                </nav>
              </div>


              </div>
            </div>
          </div>


        </div>
      </div>
    </div>
  </section>
  <!-- end of about section -->

  <?php $this->load->view('footer'); echo "\n"; ?>

  <script type="text/javascript">
    function validate()
    {

      var current_password = document.getElementById("current_password").value;
      var new_password = document.getElementById("new_password").value;
      var confirm_password = document.getElementById("confirm_password").value;
      if(new_password != confirm_password ){
        alert('Passwords do not match.');
        document.getElementById("confirm_password").focus();
        return false;
      }
      var $btn = $('button[type="submit"]').button('loading');
      $(':input[type="submit"]').prop('disabled', true);
      $('button[type="submit"]').prop('disabled', true);
      $('#submit').html('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>');
      return true;
    }
  </script>

</body>

</html>