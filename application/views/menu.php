                <div id="Action_bar">
                    <div class="container">
                        <div class="column one">
                            <ul class="contact_details">
                                <li class="slogan">
                                    Have any questions?
                                </li>
                                <li class="phone">
                                    <i class="icon-phone"></i><a href="tel:+123456789012">+123 456 789 012</a>
                                </li>
                                <li class="mail">
                                    <i class="icon-mail-line"></i><a href="mailto:<?php echo $this->config->item('info_email'); ?>"><?php echo $this->config->item('info_email'); ?></a>
                                </li>
                            </ul>
                            <ul class="social">
                                <li class="facebook">
                                    <a href="#" title="Facebook"><i class="icon-facebook"></i></a>
                                </li>
                                <li class="twitter">
                                    <a href="#" title="Twitter"><i class="icon-twitter"></i></a>
                                </li>
                                <li class="youtube">
                                    <a href="#" title="YouTube"><i class="icon-play"></i></a>
                                </li>
                                <!-- <li class="vimeo">
                                    <a href="#" title="Vimeo"><i class="icon-vimeo"></i></a>
                                </li>
                                <li class="googleplus">
                                    <a href="#" title="Google+"><i class="icon-gplus"></i></a>
                                </li>
                                <li class="skype">
                                    <a href="#" title="Skype"><i class="icon-skype"></i></a>
                                </li>
                                <li class="behance">
                                    <a href="#" title="Behance"><i class="icon-behance"></i></a>
                                </li> -->
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="header_placeholder"></div>
                <div id="Top_bar">
                    <div class="container">
                        <div class="column one">
                            <div class="top_bar_left clearfix">
                                <div class="logo">
                                    <a id="logo" href="<?php echo $this->config->base_url(); ?>" title="Need For Democracy"><img class="logo-main scale-with-grid" src="assets/images/logo.png" alt="logo"><img class="logo-sticky scale-with-grid" src="assets/images/logo.png" alt="logo"><img class="logo-mobile scale-with-grid" src="assets/images/logo.png" alt="logo"><img class="logo-mobile-sticky scale-with-grid" src="assets/images/logo.png" alt="logo">
                                    </a>
                                </div>
                                <div class="menu_wrapper">
                                    <nav id="menu" class="menu-main-menu-container">
                                        <ul id="menu-main-menu" class="menu">
                                            <li class="current_page_item">
                                                <a href="<?php echo $this->config->base_url(); ?>"><span>Home</span></a>
                                            </li>
                                            <li>
                                                <a href="about"><span>About</span></a>
                                            </li>
                                            <!-- <li>
                                                <a href="manifesto"><span>Manifesto</span></a>
                                            </li> -->
                                            <li>
                                                <a href="Be-An-Exco"><span>Be An EXCO</span></a>
                                            </li>
                                            <li>
                                                <a href="dues"><span>Dues &amp; Donations</span></a>
                                            </li>
                                            <li>
                                                <a href="contact"><span>Contact</span></a>
                                            </li>
                                        </ul>
                                    </nav><a class="responsive-menu-toggle" href="#"><i class="icon-menu-fine"></i></a>
                                </div>
                                <div class="secondary_menu_wrapper"></div>
                                <div class="banner_wrapper"></div>
                                <!-- <div class="search_wrapper">
                                    <form method="get" id="searchform" action="#">
                                        <i class="icon_search icon-search-fine"></i><a href="#" class="icon_close"><i class="icon-cancel-fine"></i></a>
                                        <input type="text" class="field" name="s" id="s" placeholder="Enter your search">
                                        <input type="submit" class="submit" value="" style="display:none">
                                    </form>
                                </div> -->
                            </div>
                            <div class="top_bar_right">
                                <div class="top_bar_right_wrapper">
                                    <!-- <a id="search_button" href="#">
                                      <i class="icon-search-fine"></i>
                                    </a> -->
                                    <a href="register" class="button button_theme button_js action_button" style="margin-top: -10px; padding-top: 5px;">
                                      <span class="button_label">Register</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>