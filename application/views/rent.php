
      <!-- Breadcromb Area Start -->
      <section class="gauto-breadcromb-area section_70">
         <div class="container">
            <div class="row">
               <div class="col-md-12">
                  <div class="breadcromb-box">
                     <h3>Car Rental</h3>
                     <ul>
                        <li><i class="fa fa-home"></i></li>
                        <li><a href="<?php echo $this->config->base_url(); ?>">Home</a></li>
                        <li><i class="fa fa-angle-right"></i></li>
                        <li><a href="<?php echo $this->config->base_url(); ?>cars">Car Listing</a></li>
                        <li><i class="fa fa-angle-right"></i></li>
                        <li>Rent <?php echo $row['name']; ?></li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <!-- Breadcromb Area End -->
       
      <!-- Booking Form Area Start -->
      <section class="gauto-booking-form section_70">
         <div class="container">

            <div class="row">
               <div class="col-lg-8">
                  <div class="booking-form-left">
                     <div class="single-booking">
                        <h3>Personal Information</h3>
                        <form>
                           <div class="row">
                              <div class="col-md-6">
                                 <p>
                                    <input type="text" placeholder="Your Full Name" value="<?php echo $profile['first_name']; ?>" disabled="disabled" />
                                 </p>
                              </div>
                              <div class="col-md-6">
                                 <p>
                                    <input type="text" placeholder="Your Last Name" value="<?php echo $profile['last_name']; ?>" disabled="disabled" />
                                 </p>
                              </div>
                           </div>
                           <div class="row">
                              <div class="col-md-6">
                                 <p>
                                    <input type="email" placeholder="Your Email Address" value="<?php echo $profile['email']; ?>" disabled="disabled" />
                                 </p>
                              </div>
                              <div class="col-md-6">
                                 <p>
                                    <input type="tel" placeholder="Your Phone Number" value="<?php echo $profile['phone']; ?>" disabled="disabled" />
                                 </p>
                              </div>
                           </div>
                        </form>
                     </div>
                     <div class="single-booking">
                        <h3>Booking Detail</h3>
          <?php if($error_code == 0 && !empty($error)): ?>

            <div class="alert alert-success alert-dismissable fade show" role="alert">
              <strong>Success!</strong>
              <?php echo $error; ?>
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <?php elseif($error_code == 1 && !empty($error)): ?>
              <div class="alert alert-danger alert-dismissable fade show" role="alert">
                <strong>Error!</strong>
                <?php echo $error; ?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <?php else: ?>
              <?php endif; ?>
                        <?php echo form_open('rent/car/' . $id, 'onsubmit="return validate();"'); ?>
                        <input type="hidden" id="payment_type" name="payment_type" value="" />
                           <div class="row">
                              <div class="col-md-6">
                                 <p>
                                    <input type="text" placeholder="Car" value="<?php echo $row['name']; ?>" disabled="disabled" >
                                 </p>
                              </div>
                              <div class="col-md-6">
                                 <p>
                                    <input type="text" placeholder="Pickup Location" name="pickup_location" required="required">
                                 </p>
                              </div>
                           </div>
                           <div class="row">
                              <div class="col-md-6">
                                 <p>
                                    <input id="pickup_date" name="pickup_date" placeholder="Pickup Date" data-select="datepicker" type="text" required="required" autocomplete="off">
                                 </p>
                              </div>
                              <div class="col-md-6">
                                 <p>
                                    <input id="return_date" name="return_date" placeholder="Return Date" data-select="datepicker" type="text" required="required" autocomplete="off">
                                 </p>
                              </div>
                           </div>
                           <div class="row">
                              <div class="col-md-12">
                                 <p>
                                    <textarea placeholder="Comments..." name="comment"></textarea>
                                 </p>
                              </div>
                           </div>
                           <div class="row">
                              <div class="col-md-12">
                                 <div class="action-btn">
                                   <button type="submit" id="submitt" class="gauto-theme-btn">Reserve Now</button>
                                </div>
                             </div>
                          </div>
                        </form>
                     </div>
                  </div>
               </div>
               <div class="col-lg-4">
                  <div class="booking-right">
                     <h3>payment Method</h3>
                     <div class="gauto-payment clearfix">
                        <div class="recent-text">
                           <h2>
                              Total: $<span id="total_price"><?php echo number_format($row['price_per_day']); ?></span>
                           </h2>
                           <br />
                        </div>
                        <div class="payment">
                           <input type="radio" id="ss-option" name="selector" value="Direct Bank Transfer">
                           <label for="ss-option">Direct Bank Transfer</label>
                           <div class="check">
                              <div class="inside"></div>
                           </div>
                           <p>
                              Acct. Name.: MARTINSCAR <br />
                              Acct. No.: 012345678 <br />
                           </p>
                        </div>
                        <div class="payment">
                           <input type="radio" id="f-option" name="selector" value="Cheque Payment">
                           <label for="f-option">Cheque Payment</label>
                           <div class="check">
                              <div class="inside"></div>
                           </div>
                        </div>
                        <div class="payment">
                           <input type="radio" id="s-option" name="selector" value="Credit Card">
                           <label for="s-option">Credit Card</label>
                           <div class="check">
                              <div class="inside"></div>
                           </div>
                           <img src="<?php echo $this->config->base_url(); ?>assets/img/master-card.jpg" alt="credit card">
                        </div>
                        <div class="payment">
                           <input type="radio" id="t-option" name="selector" value="Paypal">
                           <label for="t-option">Paypal</label>
                           <div class="check">
                              <div class="inside"></div>
                           </div>
                           <img src="<?php echo $this->config->base_url(); ?>assets/img/paypal.jpg" alt="credit card">
                        </div>
                     </div>
                  </div>
               </div>
            </div>
               </form>
         </div>
      </section>
      <!-- Booking Form Area End -->
       
       

    <?php $this->load->view('footer'); echo "\n"; ?>
   <script type="text/javascript">
   $(document).ready(function(){
   $("input[name='selector']").on("change", function(){
      $("#payment_type").val($(this).val());
   });


   $("#pickup_date").focusout(function(){
      setTimeout(function(){ getAmount($("#pickup_date").val(), $("#return_date").val()) }, 1000);
   });

   $("#return_date").focusout(function(){
      setTimeout(function(){ getAmount($("#pickup_date").val(), $("#return_date").val()) }, 1000);
   });
   });
  function getAmount(pickup_date_str, return_date_str)
  {
    let pickup_date = pickup_date_str != '' ? new Date(formatDate(pickup_date_str)) : null;
    let return_date = return_date_str != '' ? new Date(formatDate(return_date_str)) : null;
    let amount = <?php echo $row['price_per_day']; ?>;
    console.log(pickup_date);
    console.log(return_date);

    if(return_date > pickup_date) {
      let Difference_In_Time = return_date - pickup_date; 
      // To calculate the no. of days between two dates 
      let datediff = Difference_In_Time / (1000 * 3600 * 24);


      let price = datediff < 1 ? 1 : datediff;
      amount = price * <?php echo $row['price_per_day']; ?>;
      amount = amount.toFixed(2);
      $("#total_price").html(amount);
      console.log(amount);
    }
  }
  function validate()
  {
    let todays_date = new Date();
    let pickup_date = new Date($("#pickup_date").val());
    let return_date = new Date($("#return_date").val());

    let payment_type = $("#payment_type").val();

    if(payment_type == '') {
      alert("Please select payment type");
    } else if(pickup_date == '' || return_date == '' || pickup_date < todays_date) {
      alert("Invalid pickup date");
    } else if(pickup_date > return_date) {
      alert("Invalid pickup date");
    } else if(todays_date < todays_date) {
      alert("Invalid return date");
    } else {
      $(':input[type="submit"]').prop('disabled', true);
      $('button[type="submit"]').prop('disabled', true);
      $('#submit').html('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>');
      return true;
    }

      return false;
  }

   // format from M/D/YYYY to YYYYMMDD

function formatDate(userDate) {
  // split date string at '/' 
  var dateArr = userDate.split('/');
  
  //test results of split
  // console.log(dateArr[0]);
  // console.log(dateArr[1]);
  // console.log(dateArr[2]);

  // check for single number dar or month
  // prepend '0' to single number dar or month
  if(dateArr[0].length == 1){
   dateArr[0] = '0' + dateArr[0];
  } else if (dateArr[1].length == 1){
   dateArr[1] = '0' + dateArr[1];
  } 
  
  // concatenate new values into one string
  userDate = dateArr[2] + '-' + dateArr[1] + '-' + dateArr[0];
  
  // test new string value
  console.log(userDate);
  
  // return value
  return userDate;
}
</script>

</body>

</html>