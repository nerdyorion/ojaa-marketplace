<!-- Custom CSS -->
<!-- <link href="assets/css/admin/bootstrap-datepicker.min.css" rel="stylesheet"> -->

<link href="assets/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />

<!-- Page Content -->
<div id="page-wrapper">
  <div class="container-fluid">
    <div class="row bg-title">
      <div class="col-lg-12">
        <h4 class="page-title">Report</h4>
        <ol class="breadcrumb">
          <li><a href="./">Dashboard</a></li>
          <li class="active">Report</li>
        </ol>
      </div>
      <!-- /.col-lg-12 -->
    </div>
    <!-- row -->
    <div class="row">
      <div class="col-sm-12">
        <div class="white-box">
          <div class="row">
            <div class="col-md-12">
              <?php echo form_open_multipart('/admin123/report', 'class="form-horizontal", method="get", onsubmit="return validate();"'); ?>
              <!--<form class="form-horizontal" action="<?php echo current_url(); ?>" enctype="multipart/form-data" method="post">-->
                <div class="form-group">
                  <label for="current_state" class="col-sm-2 control-label">State:</label>
                  <div class="col-sm-5">
                    <select class="form-control" name="current_state" id="current_state">
                      <option value="0">-- Select --</option>
                      <?php foreach ($states as $state): ?>
                        <option value="<?php echo $state['id']; ?>" <?php $current_state = isset($_GET['current_state']) ? trim($_GET['current_state']) : '-'; echo $current_state == $state['id'] ? 'selected="selected"' : ''; ?>><?php echo $state['name']; ?></option>
                      <?php endforeach; ?>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="name" class="col-sm-2 control-label">Name:</label>
                  <div class="col-sm-5">
                    <input type="text" class="form-control" name="name" value="<?php echo isset($_GET['name']) ? trim($_GET['name']) : ''; ?>">
                  </div>
                </div>
                <div class="form-group">
                  <label for="gender" class="col-sm-2 control-label">Gender:</label>
                  <div class="col-sm-5">
                    <select class="form-control" name="gender" id="gender">
                      <option value="0">-- Select --</option>
                      <option value="Male" <?php $gender = isset($_GET['gender']) ? trim($_GET['gender']) : '-'; echo $gender == "Male" ? 'selected="selected"' : ''; ?>>Male</option>
                      <option value="Female" <?php $gender = isset($_GET['gender']) ? trim($_GET['gender']) : '-'; echo $gender == "Female" ? 'selected="selected"' : ''; ?>>Female</option>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="start" class="col-sm-2 control-label">Date Reg.:</label>
                  <div class="col-sm-5">
                    <div class="input-daterange input-group" id="date-range">
                      <input type="text" class="form-control" name="start" value="<?php echo isset($_GET['start']) ? trim($_GET['start']) : ''; ?>">
                      <span class="input-group-addon bg-info b-0 text-white">to</span>
                      <input type="text" class="form-control" name="end" value="<?php echo isset($_GET['end']) ? trim($_GET['end']) : ''; ?>">
                    </div>
                  </div>
                </div>
                <div class="form-group m-b-0">
                  <div class="col-sm-offset-3 col-sm-9">
                    <button type="submit" class="btn btn-info waves-effect waves-light">Generate</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>

    </div>
    <!-- row -->
    <div class="row">
      <div class="col-sm-12">
        <div class="white-box">
          <h3>Result</h3>
          <?php if(!empty($rows)): ?>
            <p class="text-muted m-b-20"><i class="fa fa-file-excel-o fa-2x" aria-hidden="true"></i> <a href="admin123/report/download_excel?<?php echo $_SERVER['QUERY_STRING']; ?>">Download Excel</a></p>
          <?php endif; ?>
          <div class="table-responsive">
            <table class="table table-bordered">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Name</th>
                  <th>State</th>
                  <th>Contact</th>
                  <th>Gender</th>
                  <th>Date</th>
                  <th class="text-nowrap">Actions</th>
                </tr>
              </thead>
              <tbody>
                <?php if(empty($rows)): ?>
                  <tr>
                    <td colspan="7" align="center">No data returned.</td>
                  </tr>
                  <?php else: ?>
                    <?php $sn = 1; foreach ($rows as $row): ?>
                    <tr>
                      <td><?php echo $sn++; ?></td>
                      <td>
                        <?php echo $row['first_name'] . ' ' . $row['last_name']; ?>
                      </td>
                      <td><?php echo dashIfEmpty($row['state_name']); ?></td>
                      <td>
                        <small>
                          <?php if(!empty($row['phone_voice']) && $row['phone_voice'] != '-'): ?>
                            <i class="fa fa-phone"></i> <?php echo dashIfEmpty($row['phone_voice']); ?><br />
                          <?php endif; ?>
                          <?php if(!empty($row['phone_whatsapp']) && $row['phone_whatsapp'] != '-'): ?>
                            <!-- <i class="fa fa-whatsapp"></i> <?php echo dashIfEmpty($row['phone_whatsapp']); ?><br /> -->
                          <?php endif; ?>
                          <?php if(!empty($row['email']) && $row['email'] != '-'): ?>
                            <i class="fa fa-envelope"></i> <a href="mailto:<?php echo $row['email']; ?>"><?php echo dashIfEmpty($row['email']); ?></a><br />
                          <?php endif; ?>
                          <?php if(!empty($row['address']) && $row['address'] != '-'): ?>
                            <!-- <i class="fa fa-map-marker"></i> <?php echo dashIfEmpty($row['address']) ?> -->
                          <?php endif; ?>
                        </small>
                      </td>
                      <td><?php echo empty($row['gender']) ? '-' : $row['gender']; ?></td>
                      <td><?php echo dateEmpty($row['date_created']) ? "-" : date('M d, Y h:i A', strtotime($row['date_created'])); ?></td>
                    </tr>
                  <?php endforeach; ?>
                <?php endif; ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->

<?php $this->load->view($this->config->item('template_dir_admin') . 'footer', array('error', $error, 'error_code', $error_code)); echo "\n";  // load footer view ?>
<script src="assets/js/jasny-bootstrap.js"></script>
<script src="assets/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript">
 jQuery(document).ready(function() {
  // Date Picker
  // jQuery('.mydatepicker, #datepicker2').datepicker();
  // jQuery('#datepicker-autoclose').datepicker({
  //   autoclose: true,
  //   todayHighlight: true
  // });

  jQuery('#date-range').datepicker({
    toggleActive: true
  });


});

/*
  function validate()
  {
    if((document.getElementById("network_id").value == '0') || (document.getElementById("network_id").value == null)){
      alert('Please select network.');
      return false;
    }
    else {
      return true;
    }
  }
  */
</script>

</body>
</html>
