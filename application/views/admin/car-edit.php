  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-12">
          <h4 class="page-title">Update Car</h4>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url() . "admin123/"; ?>">Dashboard</a></li>
            <li><a href="<?php echo base_url() . "admin123/"; ?>cars">Cars</a></li>
            <li class="active">Edit</li>
          </ol>
        </div>
        <!-- /.col-lg-12 -->
      </div>

      <!-- row -->
      <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
          <div class="white-box">
            <!--<h3>Car</h3>-->
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
              <li role="presentation" class="active"><a href="#update" aria-controls="update" role="tab" data-toggle="tab"><span class="visible-xs"><i class="ti-pencil"></i></span><span class="hidden-xs"> Update</span></a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
              <div role="tabpanel" class="tab-pane active" id="update">
                <div class="col-md-12">
                <?php echo form_open_multipart('admin123/cars/edit/' . $row['id'], 'class="form-horizontal", onsubmit="return validate();"'); ?>
                  <div class="form-group">
                    <label for="brand_id" class="col-sm-2 control-label">Brand: <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                      <select class="form-control" name="brand_id" id="brand_id" required="required">
                        <option value="" selected="selected">-- Select --</option>
                        <?php foreach ($brands as $brand): ?>
                          <option value="<?php echo $brand['id']; ?>" <?php echo $row['brand_id'] == $brand['id'] ? 'selected="selected"' : ''; ?>><?php echo $brand['name']; ?></option>
                        <?php endforeach; ?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">Name: <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" name="name" maxlength="2000" id="name" value="<?php echo $row['name']; ?>" placeholder="" required="required">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="image_url" class="col-sm-2 control-label">Image (650x650 px) <?php echo !empty($row['image_url']) && file_exists($upload_save_path . $row['image_url']) ? '' : '<span class="text-danger">*</span>' ?>: </label>
                    <div class="col-sm-10">
                      <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                        <div class="form-control" data-trigger="fileinput">
                          <i class="glyphicon glyphicon-file fileinput-exists"></i> 
                          <span class="fileinput-filename"></span>
                        </div>
                        <span class="input-group-addon btn btn-default btn-file">
                        <span class="fileinput-new">Select file</span>
                        <span class="fileinput-exists">Change</span>
                        <input type="file" name="image_url" id="image_url" value="<?php echo $row['image_url']; ?>" <?php echo !empty($row['image_url']) && file_exists($upload_save_path . $row['image_url']) ? '' : 'required="required" aria-required="true"' ?> ></span>
                        <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                      </div>

                        <?php if(!empty($row['image_url']) && file_exists($upload_save_path . $row['image_url'])): ?>
                          <strong><a href="<?php echo $upload_save_path . $row['image_url'] ?>" target="_blank">View</a></strong>
                        <?php endif; ?>

                    </div>
                  </div>
                  <div class="form-group">
                    <label for="description" class="col-sm-2 control-label">Description:</label>
                    <div class="col-sm-10">
                      <textarea class="form-control" name="description" id="description" maxlength="8000"><?php echo $row['description']; ?></textarea>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="price_per_day" class="col-sm-2 control-label">Price per day ($): <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                      <input type="number" class="form-control" name="price_per_day" min="1" step="0.1" id="price_per_day" placeholder="" value="<?php echo $row['price_per_day']; ?>" required="required">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="model_year" class="col-sm-2 control-label">Year: <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                      <input type="number" class="form-control" name="model_year" min="1" id="model_year" placeholder="" value="<?php echo $row['model_year']; ?>" required="required">
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="kmph" class="col-sm-2 control-label">Kmph: <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                      <input type="number" class="form-control" name="kmph" min="1" id="kmph" placeholder="" value="<?php echo $row['kmph']; ?>" required="required">
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="drive_setup" class="col-sm-2 control-label">Drive Setup:</label>
                    <div class="col-sm-10">
                      <select class="form-control" name="drive_setup" id="drive_setup">
                        <option value="" selected="selected">-- Select --</option>
                        <?php foreach ($drive_setups as $drive_setup): ?>
                          <option value="<?php echo $drive_setup['id']; ?>" <?php echo $row['drive_setup'] == $drive_setup['id'] ? 'selected="selected"' : ''; ?>><?php echo $drive_setup['name']; ?></option>
                        <?php endforeach; ?>
                      </select>
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="fuel_type" class="col-sm-2 control-label">Fuel Type:</label>
                    <div class="col-sm-10">
                      <select class="form-control" name="fuel_type" id="fuel_type">
                        <option value="" selected="selected">-- Select --</option>
                        <?php foreach ($fuel_types as $fuel_type): ?>
                          <option value="<?php echo $fuel_type['id']; ?>" <?php echo $row['fuel_type'] == $fuel_type['id'] ? 'selected="selected"' : ''; ?>><?php echo $fuel_type['name']; ?></option>
                        <?php endforeach; ?>
                      </select>
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="transmission" class="col-sm-2 control-label">Transmission: <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                      <select class="form-control" name="transmission" id="transmission" required="required">
                        <option value="" selected="selected">-- Select --</option>
                        <?php foreach ($transmissions as $transmission): ?>
                          <option value="<?php echo $transmission['id']; ?>" <?php echo $row['transmission'] == $transmission['id'] ? 'selected="selected"' : ''; ?>><?php echo $transmission['name']; ?></option>
                        <?php endforeach; ?>
                      </select>
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="color" class="col-sm-2 control-label">Color:</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" name="color" maxlength="2000" id="color" value="<?php echo $row['color']; ?>" placeholder="">
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="checkbox" class="control-label"> </label>
                    <div class="col-sm-offset-2 col-sm-10">
                      <div class="form-inline row">
                        <div class="form-group col-sm-3">
                          <div class="checkbox checkbox-primary">
                            <input name="has_ac" id="has_ac" type="checkbox" value="1" <?php echo empty($row['has_ac']) ? '' : 'checked="checked"'; ?>>
                            <label for="has_ac"> Has AC? </label>
                          </div>
                        </div>

                        <div class="form-group col-sm-3">
                          <div class="checkbox checkbox-primary">
                            <input name="has_gps" id="has_gps" type="checkbox" value="1" <?php echo empty($row['has_gps']) ? '' : 'checked="checked"'; ?>>
                            <label for="has_gps"> Has GPS? </label>
                          </div>
                        </div>

                        <div class="form-group col-sm-3">
                          <div class="checkbox checkbox-primary">
                            <input name="has_anti_lock_brakes" id="has_anti_lock_brakes" type="checkbox" value="1" <?php echo empty($row['has_anti_lock_brakes']) ? '' : 'checked="checked"'; ?>>
                            <label for="has_anti_lock_brakes"> Has Anti-Lock Brakes? </label>
                          </div>
                        </div>

                        <div class="form-group col-sm-3">
                          <div class="checkbox checkbox-primary">
                            <input name="has_remote_keyless" id="has_remote_keyless" type="checkbox" value="1" <?php echo empty($row['has_remote_keyless']) ? '' : 'checked="checked"'; ?>>
                            <label for="has_remote_keyless"> Has Remote Keyless? </label>
                          </div>
                        </div>

                      </div>
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="checkbox" class="control-label"> </label>
                    <div class="col-sm-offset-2 col-sm-10">
                      <div class="form-inline row">
                        <div class="form-group col-sm-3">
                          <div class="checkbox checkbox-primary">
                            <input name="has_v6_cylinder" id="has_v6_cylinder" type="checkbox" value="1" <?php echo empty($row['has_v6_cylinder']) ? '' : 'checked="checked"'; ?>>
                            <label for="has_v6_cylinder"> Has v6 Cylinder? </label>
                          </div>
                        </div>

                        <div class="form-group col-sm-3">
                          <div class="checkbox checkbox-primary">
                            <input name="has_rear_seat_dvd" id="has_rear_seat_dvd" type="checkbox" value="1" <?php echo empty($row['has_rear_seat_dvd']) ? '' : 'checked="checked"'; ?>>
                            <label for="has_rear_seat_dvd"> Has Rear Seat DVD? </label>
                          </div>
                        </div>


                      </div>
                    </div>
                  </div>

                  <div class="form-group m-b-0">
                    <div class="col-sm-offset-3 col-sm-9">
                      <button type="submit" class="btn btn-info waves-effect waves-light">Update</button>
                      <button type="button" class="btn btn-info waves-effect waves-light center" onclick="history.go(-1); return false;">&larr; Back</button>
                    </div>
                  </div>
                </form>
                </div>
                <div class="clearfix"></div>
              </div>
            </div>
          </div>
        </div>
      </div> 
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </div>
  <!-- /#page-wrapper -->

<?php $this->load->view($this->config->item('template_dir_admin') . 'footer', array('error', $error, 'error_code', $error_code)); echo "\n";  // load footer view ?>
<script src="assets/admin/js/jasny-bootstrap.js"></script>
<script type="text/javascript">
  function validate()
  {
    
        return true;
    if(document.getElementById("image_url").files.length == 0 ){
      alert('Please upload image.');
      return false;
    }
    else {
      var fileName = $("#image_url").val();
      fileName = fileName.toLowerCase();
      
      if((fileName.lastIndexOf("jpg")===fileName.length-3) || (fileName.lastIndexOf("jpeg")===fileName.length-4) || (fileName.lastIndexOf("png")===fileName.length-3) || (fileName.lastIndexOf("gif")===fileName.length-3))
      {
        //alert("OK");
        return true;
      }
      else
      {
        alert("Please upload valid image file.");
        return false;
      }
    }
  }
</script>

</body>
</html>