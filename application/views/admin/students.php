  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-12">
          <h4 class="page-title">Students</h4>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url() . "admin123/"; ?>">Dashboard</a></li>
            <li class="active">Students</li>
          </ol>
        </div>
        <!-- /.col-lg-12 -->
      </div>

      <!-- row -->
      <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
          <div class="white-box">
            <!--<h3>Student</h3>-->
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
              <li role="presentation" class="active"><a href="#all" aria-controls="all" role="tab" data-toggle="tab"><span class="visible-xs"><i class="ti-view-list"></i></span><span class="hidden-xs"> View All</span></a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
              <div role="tabpanel" class="tab-pane active" id="all"> 
                <div class="col-md-12">
                  <!-- Search Filter -->
                  <div class="row">
                    <?php echo form_open('/admin123/students', 'class="form-inline", method="get", role="form"'); ?>
                    <div class="form-group">
                      <label for="title">Search:</label>
                      <input type="text" class="form-control" name="title" maxlength="2000" id="title" value="<?php echo isset($_GET['title']) ? trim($_GET['title']) : ''; ?>" placeholder="" />
                      </select>
                    </div>
                    <button type="submit" class="btn btn-info waves-effect waves-light"><i class="ti-search"></i> Search</button>
                  </form>
                </div>
                  <div class="table-responsive">
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Name</th>
                          <!-- <th>State</th> -->
                          <th>Contact</th>
                          <th>Gender</th>
                          <th>Documents</th>
                          <th>Verified?</th>
                          <th>Date Registered</th>
                          <th class="text-nowrap">Actions</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php if(empty($rows)): ?>
                        <tr>
                          <td colspan="8" align="center">No data returned.</td>
                        </tr>
                      <?php else: ?>
                      <?php foreach ($rows as $row): ?>
                        <tr>
                          <td><?php echo $sn++; ?></td>
                          <td>
                            <?php echo $row['first_name'] . ' ' . $row['last_name']; ?>
                              <?php if(!empty($row['student_no']) && $row['student_no'] != '-'): ?>
                                <br /><b>Student ID: </b> <?php echo dashIfEmpty($row['student_no']); ?>
                              <?php endif; ?>
                            </td>
                          <td>
                            <small>
                              <?php if(!empty($row['phone']) && $row['phone'] != '-'): ?>
                                <i class="fa fa-phone"></i> <?php echo dashIfEmpty($row['phone']); ?><br />
                              <?php endif; ?>
                              <?php if(!empty($row['email']) && $row['email'] != '-'): ?>
                                <i class="fa fa-envelope"></i> <a href="mailto:<?php echo $row['email']; ?>"><?php echo dashIfEmpty($row['email']); ?></a><br />
                              <?php endif; ?>
                              <?php if(!empty($row['country_name']) && $row['country_name'] != '-'): ?>
                                <i class="fa fa-globe"></i> <?php echo dashIfEmpty($row['country_name']) ?><br />
                              <?php endif; ?>
                              <?php if(!empty($row['address']) && $row['address'] != '-'): ?>
                                <i class="fa fa-map-marker"></i> <?php echo dashIfEmpty($row['address']) ?>
                              <?php endif; ?>
                            </small>
                          </td>
                          <td><?php echo empty($row['gender']) ? '-' : $row['gender']; ?></td>
                          <td>
                            <?php if(!empty($row['id_card_url']) && file_exists($upload_save_path . $row['id_card_url'])): ?>
                              <small><a href="admin123/download/id_card/<?php echo $row['id']; ?>"><i class="fa fa-download text-inverse m-r-5"></i>ID Card</a></small>
                              <br />
                            <?php endif; ?>
                            <?php if(!empty($row['drivers_license_url']) && file_exists($upload_save_path . $row['drivers_license_url'])): ?>
                              <small><a href="admin123/download/drivers_license/<?php echo $row['id']; ?>"><i class="fa fa-download text-inverse m-r-5"></i>Driver's License</a></small>
                              <br />
                            <?php endif; ?>
                            <?php if(!empty($row['proof_of_residence_url']) && file_exists($upload_save_path . $row['proof_of_residence_url'])): ?>
                              <small><a href="admin123/download/proof_of_residence/<?php echo $row['id']; ?>"><i class="fa fa-download text-inverse m-r-5"></i>Proof of Residence</a></small>
                              <br />
                            <?php endif; ?>
                            <?php if(!empty($row['international_passport_url']) && file_exists($upload_save_path . $row['international_passport_url'])): ?>
                              <small><a href="admin123/download/international_passport/<?php echo $row['id']; ?>"><i class="fa fa-download text-inverse m-r-5"></i>Intl. Passport</a></small>
                            <?php endif; ?>
                          </td>
                            <td><?php echo $row['verified'] == 1 ? '<a href="javascript:void(0);" class="bg-success" style="color: #ffffff;">&nbsp;YES&nbsp;</a>' : '<a href="javascript:void(0);" class="bg-danger" style="color: #ffffff;">&nbsp;NO&nbsp;</a>'; ?></td>
                            <!-- <td><?php // echo dateEmpty($row['dob']) ? "-" : date('M d, Y', strtotime($row['dob'])); ?></td> -->
                            <td><?php echo dateEmpty($row['date_created']) ? "-" : date('M d, Y h:i A', strtotime($row['date_created'])); ?></td>
                          <td class="text-nowrap">
                            <!-- <a href="admin123/students/edit/<?php echo $row['id']; ?>" data-toggle="tooltip" data-original-title="Edit"> <i class="fa fa-pencil text-inverse m-r-10"></i> </a>  -->
                            <!-- <a href="admin123/students/delete/<?php echo $row['id']; ?>" data-toggle="tooltip" data-original-title="Delete" onclick="if(confirm('Are you sure you want to proceed?')) return true; else return false;"> <span class="text-danger"><i class="fa fa-close text-danger m-r-10"></i></span> </a>  -->
                            <?php if($row['verified'] == 1): ?>
                            <a href="admin123/students/unverify/<?php echo $row['id']; ?>" data-toggle="tooltip" data-original-title="Unverify" onclick="if(confirm('Are you sure you want to proceed?')) return true; else return false;"> <span class="text-danger"><i class="fa fa-ban text-danger m-r-10"></i></span> </a>
                            <?php else: ?>
                            <a href="admin123/students/verify/<?php echo $row['id']; ?>" data-toggle="tooltip" data-original-title="Verify" onclick="if(confirm('Are you sure you want to verify this student?')) return true; else return false;"> <span class="text-danger"><i class="fa fa-check text-inverse m-r-10"></i></span> </a>
                            <?php endif; ?>

                            <a href="admin123/students/view/<?php echo $row['id']; ?>" data-toggle="tooltip" data-original-title="View"> <i class="fa fa-folder-open text-inverse m-r-10"></i> </a> 
                          </td>
                        </tr>
                      <?php endforeach; ?>
                      <?php endif; ?>
                      </tbody>
                    </table>
                  </div>
                </div>
                <!--
                <div class="col-md-3 pull-right">
                  <p><a href="?page=2">Pagination</a>.</p>
                </div>
                -->
                <div class="clearfix"></div>
              </div>
            </div>
            <div class="col-md-3 pull-right pagination">
              <p><?php echo $links; ?></p>
            </div>
          </div>
        </div>
      </div> 
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </div>
  <!-- /#page-wrapper -->

<?php $this->load->view($this->config->item('template_dir_admin') . 'footer', array('error', $error, 'error_code', $error_code)); echo "\n";  // load footer view ?>
<script src="assets/js/jasny-bootstrap.js"></script>
<script type="text/javascript">
</script>

</body>
</html>