  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-12">
          <h4 class="page-title">Bookings</h4>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url() . "admin123/"; ?>">Dashboard</a></li>
            <li class="active">Bookings</li>
          </ol>
        </div>
        <!-- /.col-lg-12 -->
      </div>

      <!-- row -->
      <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
          <div class="white-box">
            <!--<h3>Student</h3>-->
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
              <li role="presentation" class="active"><a href="#all" aria-controls="all" role="tab" data-toggle="tab"><span class="visible-xs"><i class="ti-view-list"></i></span><span class="hidden-xs"> View All</span></a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
              <div role="tabpanel" class="tab-pane active" id="all"> 
                <div class="col-md-12">
                  <!-- Search Filter -->
                  <div class="row">
                    <?php echo form_open('/admin123/bookings', 'class="form-inline", method="get", role="form"'); ?>
                    <div class="form-group">
                      <label for="title">Search:</label>
                      <input type="text" class="form-control" name="title" maxlength="2000" id="title" value="<?php echo isset($_GET['title']) ? trim($_GET['title']) : ''; ?>" placeholder="" />
                      </select>
                    </div>
                    <button type="submit" class="btn btn-info waves-effect waves-light"><i class="ti-search"></i> Search</button>
                  </form>
                </div>
                  <div class="table-responsive">
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Booking ID</th>
                          <th>Student</th>
                          <th>Car</th>
                          <th>Pickup Date</th>
                          <th>Pickup Location</th>
                          <th>Return Date</th>
                          <th>Amount</th>
                          <th>Payment Type</th>
                          <th>Date</th>
                          <th>Status</th>
                          <th class="text-nowrap">Actions</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php if(empty($rows)): ?>
                        <tr>
                          <td colspan="11" align="center">No data returned.</td>
                        </tr>
                      <?php else: ?>
                      <?php foreach ($rows as $row): ?>
                        <tr>
                          <td><?php echo $sn++; ?></td>
                            <td><?php echo dashIfEmpty($row['id']); ?></td>
                            <td><?php echo dashIfEmpty($row['full_name']); ?></td>
                            <td><?php echo dashIfEmpty($row['name']); ?></td>
                            <td><?php echo dashIfEmpty($row['pickup_date']); ?></td>
                            <td><?php echo dashIfEmpty($row['pickup_location']); ?></td>
                            <td><?php echo dashIfEmpty($row['return_date']); ?></td>
                            <td>$<?php echo number_format($row['amount'], 2); ?></td>
                            <td><?php echo dashIfEmpty($row['payment_type']); ?></td>
                          <td><?php echo dateEmpty($row['date_created']) ? "-" : date('M d, Y h:i A', strtotime($row['date_created'])); ?></td>
                          <td>
                            <?php echo $row['approved'] == 1 ? '<a href="javascript:void(0);" class="bg-success" style="color: #ffffff;">&nbsp;APPROVED&nbsp;</a>' : '<a href="javascript:void(0);" class="bg-danger" style="color: #ffffff;">&nbsp;PENDING APPROVAL&nbsp;</a>'; ?> 
                          </td>
                          <td class="text-nowrap">
                            <?php if($row['approved'] == 0): ?>
                            <a href="admin123/bookings/approve/<?php echo $row['id']; ?>" data-toggle="tooltip" data-original-title="Approve" onclick="if(confirm('Are you sure you want to approve this booking, this action cannot be revoked?')) return true; else return false;"> <span class="text-danger"><i class="fa fa-check text-inverse m-r-10"></i></span> </a>
                            <?php endif; ?>

                            <a href="admin123/bookings/delete/<?php echo $row['id']; ?>" data-toggle="tooltip" data-original-title="Delete" onclick="if(confirm('Are you sure you want to delete this record?')) return true; else return false;"> <span class="text-danger"><i class="fa fa-close text-danger m-r-10"></i></span> </a> 
                          </td>
                        </tr>
                      <?php endforeach; ?>
                      <?php endif; ?>
                      </tbody>
                    </table>
                  </div>
                </div>
                <!--
                <div class="col-md-3 pull-right">
                  <p><a href="?page=2">Pagination</a>.</p>
                </div>
                -->
                <div class="clearfix"></div>
              </div>
            </div>
            <div class="col-md-3 pull-right pagination">
              <p><?php echo $links; ?></p>
            </div>
          </div>
        </div>
      </div> 
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </div>
  <!-- /#page-wrapper -->

<?php $this->load->view($this->config->item('template_dir_admin') . 'footer', array('error', $error, 'error_code', $error_code)); echo "\n";  // load footer view ?>
<script src="assets/js/jasny-bootstrap.js"></script>
<script type="text/javascript">
</script>

</body>
</html>