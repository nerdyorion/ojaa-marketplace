  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-12">
          <h4 class="page-title">Student Details</h4>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url() . "admin123/"; ?>">Dashboard</a></li>
            <li><a href="<?php echo base_url() . "admin123/"; ?>students">Students</a></li>
            <li class="active">Details</li>
          </ol>
        </div>
        <!-- /.col-lg-12 -->
      </div>

      <!-- row -->
      <div class="row">
        <div class="col-md-12">
          <div class="white-box">
            <div class="table-responsive">
              <table class="table table-bordered table-striped">
                <tbody>
                  <tr>
                    <th>Full Name</th>
                    <td><?php echo $row['full_name']; ?></td>
                  </tr>
                  <tr>
                    <th>Gender</th>
                    <td><?php echo dashIfEmpty($row['gender']); ?></td>
                  </tr>
                  <tr>
                    <th>Email</th>
                    <td><a href="mailto:<?php echo $row['email']; ?>"><?php echo $row['email']; ?></a></td>
                  </tr>
                  <tr>
                    <th>Phone</th>
                    <td><?php echo dashIfEmpty($row['phone']); ?></td>
                  </tr>
                  <tr>
                    <th>Date of Birth</th>
                    <td><?php echo empty($row['dob']) ? "-" : $row['dob']; ?></td>
                  </tr>
                  <tr>
                    <th>Country</th>
                    <td><?php echo dashIfEmpty($row['country_name']); ?></td>
                  </tr>
                  <tr>
                    <th>Address</th>
                    <td><?php echo dashIfEmpty($row['address']); ?></td>
                  </tr>
                  <tr>
                    <th>Verified</th>
                    <td><?php echo $row['verified'] == 1 ? '<a href="javascript:void(0);" class="bg-success" style="color: #ffffff;">&nbsp;YES&nbsp;</a>' : '<a href="javascript:void(0);" class="bg-danger" style="color: #ffffff;">&nbsp;NO&nbsp;</a>'; ?> </td>
                  </tr>
                  <tr>
                    <th>Student ID</th>
                    <td><?php echo dashIfEmpty($row['student_no']); ?></td>
                  </tr>
                  <tr>
                  <?php if(!empty($row['id_card_url']) && file_exists($upload_save_path . $row['id_card_url'])): ?>
                  <tr>
                    <th>ID Card</th>
                    <td><strong><a href="admin123/download/id_card/<?php echo $row['id']; ?>">View / Download</a></strong></td>
                  </tr>
                <?php endif; ?>

                  <?php if(!empty($row['drivers_license_url']) && file_exists($upload_save_path . $row['drivers_license_url'])): ?>
                  <tr>
                    <th>Driver's License</th>
                    <td><strong><a href="admin123/download/drivers_license/<?php echo $row['id']; ?>">View / Download</a></strong></td>
                  </tr>
                <?php endif; ?>

                  <?php if(!empty($row['proof_of_residence_url']) && file_exists($upload_save_path . $row['proof_of_residence_url'])): ?>
                  <tr>
                    <th>Proof of Residence</th>
                    <td><strong><a href="admin123/download/proof_of_residence/<?php echo $row['id']; ?>">View / Download</a></strong></td>
                  </tr>
                <?php endif; ?>

                  <?php if(!empty($row['international_passport_url']) && file_exists($upload_save_path . $row['international_passport_url'])): ?>
                  <tr>
                    <th>Intl. Passport</th>
                    <td><strong><a href="admin123/download/international_passport/<?php echo $row['id']; ?>">View / Download</a></strong></td>
                  </tr>
                <?php endif; ?>

                <tr>
                  <th>Date Registered</th>
                  <td><?php echo dateEmpty($row['date_created']) ? "-" : date('M d, Y h:i A', strtotime($row['date_created'])); ?></td>
                </tr>
                <tr>
                  <th>Last Login</th>
                  <td><?php echo dateEmpty($row['last_login']) ? "-" : date('M d, Y h:i A', strtotime($row['last_login'])); ?></td>
                </tr>
              </tbody>
            </table>
          </div>
          <p class="text-center">
            <button type="button" class="btn btn-info waves-effect waves-light center" onclick="history.go(-1); return false;">&larr; Back</button>
          </p>
        </div>
      </div>
    </div>
    <!-- /.row -->
  </div>
  <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->

<?php $this->load->view($this->config->item('template_dir_admin') . 'footer', array('error', $error, 'error_code', $error_code)); echo "\n";  // load footer view ?>
<script src="assets/js/jasny-bootstrap.js"></script>

</body>
</html>
