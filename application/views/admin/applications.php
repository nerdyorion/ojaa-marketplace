  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-12">
          <h4 class="page-title">Applications</h4>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url() . "admin123/"; ?>">Dashboard</a></li>
            <li class="active">Applications</li>
          </ol>
        </div>
        <!-- /.col-lg-12 -->
      </div>

      <!-- row -->
      <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
          <div class="white-box">
            <!--<h3>Student</h3>-->
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
              <li role="presentation" class="active"><a href="#all" aria-controls="all" role="tab" data-toggle="tab"><span class="visible-xs"><i class="ti-view-list"></i></span><span class="hidden-xs"> View All</span></a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
              <div role="tabpanel" class="tab-pane active" id="all"> 
                <div class="col-md-12">
                  <!-- Search Filter -->
                  <div class="row">
                    <?php echo form_open('/admin123/applications', 'class="form-inline", method="get", role="form"'); ?>
                    <div class="form-group">
                      <label for="title">Search:</label>
                      <input type="text" class="form-control" name="title" maxlength="2000" id="title" value="<?php echo isset($_GET['title']) ? trim($_GET['title']) : ''; ?>" placeholder="" />
                      </select>
                    </div>
                    <button type="submit" class="btn btn-info waves-effect waves-light"><i class="ti-search"></i> Search</button>
                  </form>
                </div>
                  <div class="table-responsive">
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Applicant</th>
                          <th>Date</th>
                          <th>Amount</th>
                          <th>Purpose</th>
                          <th>Duration</th>
                          <th>Status</th>
                          <th class="text-nowrap">Actions</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php if(empty($rows)): ?>
                        <tr>
                          <td colspan="8" align="center">No data returned.</td>
                        </tr>
                      <?php else: ?>
                      <?php foreach ($rows as $row): ?>
                        <tr>
                          <td><?php echo $sn++; ?></td>
                          <td>
                            <a href="admin123/customers/view/<?php echo $row['user_id']; ?>"><?php echo $row['applicant']; ?></a>

                            <?php if(!empty($row['stmt_of_acct_url']) && file_exists($upload_save_path . $row['stmt_of_acct_url'])): ?>
                              <br /><strong><a href="admin123/applications/download_statement/<?php echo $row['id']; ?>">View / Download Statement</a></strong>
                            <?php endif; ?>

                            <?php if(!empty($row['emp_letter_url']) && file_exists($upload_save_path . $row['emp_letter_url'])): ?>
                              <br /><strong><a href="admin123/applications/download_emp_cac_assoc_letter/<?php echo $row['id']; ?>">View / Download Emp Letter or CAC</a></strong>
                            <?php endif; ?>
                          </td>
                          <td><?php echo dateEmpty($row['date_created']) ? "-" : date('M d, Y h:i A', strtotime($row['date_created'])); ?></td>
                          <td><?php echo 'NGN' . number_format($row['loan_amount']); ?></td>
                          <td><?php echo $row['loan_purpose']; ?></td>
                          <td><?php echo $row['loan_duration']; ?></td>
                          <td>
                            <?php if($row['passed_review_1'] == 0 && $row['is_rejected'] == 0): ?>
                              <a href="javascript:void(0);" class="bg-default" style="color: #000;">&nbsp;PENDING REVIEWER (L1)&nbsp;</a>
                            <?php elseif($row['passed_review_2'] == 0 && $row['is_rejected'] == 0): ?>
                              <a href="javascript:void(0);" class="bg-default" style="color: #000;">&nbsp;PENDING ENDORSER (L2)&nbsp;</a>
                            <?php elseif($row['passed_review_3'] == 0 && $row['is_rejected'] == 0): ?>
                              <a href="javascript:void(0);" class="bg-default" style="color: #000;">&nbsp;PENDING APPROVER (L3)&nbsp;</a>
                            <?php elseif($row['passed_review_3'] == 1 && $row['is_disbursed'] == 0 && $row['is_rejected'] == 0): ?>
                              <a href="javascript:void(0);" class="bg-warning" style="color: #ffffff;">&nbsp;PENDING DISBUSER (L4)&nbsp;</a>
                            <?php elseif($row['is_disbursed'] == 1): ?>
                              <a href="javascript:void(0);" class="bg-success" style="color: #ffffff;">&nbsp;DISBURSED&nbsp;</a>
                            <?php elseif($row['is_rejected'] == 1): ?>
                              <a href="javascript:void(0);" class="bg-danger" style="color: #ffffff;">&nbsp;REJECTED&nbsp;</a>
                            <?php elseif($row['loan_repayed'] == 1): ?>
                              <a href="javascript:void(0);" class="bg-info" style="color: #ffffff;">&nbsp;REPAYED&nbsp;</a>
                            <?php elseif($row['is_rejected'] == 1): ?>
                              <a href="javascript:void(0);" class="bg-danger" style="color: #ffffff;">&nbsp;REJECTED&nbsp;</a>
                            <?php endif; ?>
                          </td>
                          <td class="text-nowrap">
                            <?php if( (int) $this->session->userdata('user_designation_id') == 1 && $row['is_rejected'] == 0 && $row['passed_review_1'] == 0 ): ?>
                              <a href="admin123/applications/approve_to_endorser/<?php echo $row['id']; ?>" data-toggle="tooltip" data-original-title="Approve to Endorser" onclick="if(confirm('Are you sure you want to proceed?')) return true; else return false;"> <i class="fa fa-check-circle text-inverse m-r-2"></i> Approve to Endorser (L2)</a> &nbsp;&nbsp;

                              <a href="admin123/applications/reject/<?php echo $row['id']; ?>" class="text-danger" data-toggle="tooltip" data-original-title="Reject" onclick="if(confirm('Are you sure you want to reject this application?')) return true; else return false;"><i class="fa fa-ban text-danger m-r-2"></i>  Reject</a>

                            <?php elseif( (int) $this->session->userdata('user_designation_id') == 2 && $row['is_rejected'] == 0 && $row['passed_review_1'] == 1 && $row['passed_review_2'] == 0 ): ?>
                              <a href="admin123/applications/endorse_to_approver/<?php echo $row['id']; ?>" data-toggle="tooltip" data-original-title="Endorse to Approver" onclick="if(confirm('Are you sure you want to proceed?')) return true; else return false;"> <i class="fa fa-check-circle text-inverse m-r-2"></i> Endorse to Approver (L3)</a> 

                              <a href="admin123/applications/reject/<?php echo $row['id']; ?>" class="text-danger"  data-toggle="tooltip" data-original-title="Reject" onclick="if(confirm('Are you sure you want to reject this application?')) return true; else return false;"> <i class="fa fa-ban text-danger m-r-2"></i> Reject</a>

                            <?php elseif( (int) $this->session->userdata('user_designation_id') == 3 && $row['is_rejected'] == 0 && $row['passed_review_1'] == 1 && $row['passed_review_2'] == 1 && $row['passed_review_3'] == 0 ): ?>
                              <a href="admin123/applications/approve_to_disburser/<?php echo $row['id']; ?>" data-toggle="tooltip" data-original-title="Approve to Disburser" onclick="if(confirm('Are you sure you want to proceed?')) return true; else return false;"> <i class="fa fa-check-circle text-inverse m-r-2"></i> Approve to Disburser (L4)</a> 

                              <a href="admin123/applications/reject/<?php echo $row['id']; ?>" class="text-danger"  data-toggle="tooltip" data-original-title="Reject" onclick="if(confirm('Are you sure you want to reject this application?')) return true; else return false;"> <i class="fa fa-ban text-danger m-r-2"></i> Reject</a>

                              <a href="admin123/applications/delete/<?php echo $row['id']; ?>" class="text-danger"  data-toggle="tooltip" data-original-title="Delete" onclick="if(confirm('Are you sure you want to delete this record?')) return true; else return false;"> <span class="text-danger"><i class="fa fa-close text-danger m-r-2"></i></span> Delete</a>

                            <?php elseif( (int) $this->session->userdata('user_designation_id') == 4 && $row['is_rejected'] == 0 && $row['passed_review_1'] == 1 && $row['passed_review_2'] == 1 && $row['passed_review_3'] == 1 && $row['is_disbursed'] == 0 ): ?>
                              <a href="admin123/applications/funds_disbursed/<?php echo $row['id']; ?>" data-toggle="tooltip" data-original-title="Mark Funds Disbursed" onclick="if(confirm('Are you sure you want to proceed?')) return true; else return false;"> <i class="fa fa-check-circle text-inverse m-r-2"></i> Mark Funds Disbursed</a> 

                            <?php elseif( ((int) $this->session->userdata('user_role_id') == 1 || (int) $this->session->userdata('user_role_id') == 2) && (int) $this->session->userdata('user_designation_id') == 0 ): ?>
                                <a href="admin123/applications/delete/<?php echo $row['id']; ?>" class="text-danger"  data-toggle="tooltip" data-original-title="Delete" onclick="if(confirm('Are you sure you want to delete this record?')) return true; else return false;"> <span class="text-danger"><i class="fa fa-close text-danger m-r-2"></i></span> Delete</a>

                            <?php else: ?>
                                <a href="javascript:void(0);" data-toggle="tooltip" data-original-title="-"> <span class="text-primary">-</span> </a>
                            <?php endif; ?>
                          </td>
                        </tr>
                      <?php endforeach; ?>
                      <?php endif; ?>
                      </tbody>
                    </table>
                  </div>
                </div>
                <!--
                <div class="col-md-3 pull-right">
                  <p><a href="?page=2">Pagination</a>.</p>
                </div>
                -->
                <div class="clearfix"></div>
              </div>
            </div>
            <div class="col-md-3 pull-right pagination">
              <p><?php echo $links; ?></p>
            </div>
          </div>
        </div>
      </div> 
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </div>
  <!-- /#page-wrapper -->

<?php $this->load->view($this->config->item('template_dir_admin') . 'footer', array('error', $error, 'error_code', $error_code)); echo "\n";  // load footer view ?>
<script src="assets/js/jasny-bootstrap.js"></script>
<script type="text/javascript">
</script>

</body>
</html>