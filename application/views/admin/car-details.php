  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-12">
          <h4 class="page-title">Car Details</h4>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url() . "admin123/"; ?>">Dashboard</a></li>
            <li><a href="<?php echo base_url() . "admin123/"; ?>cars">Cars</a></li>
            <li class="active">Details</li>
          </ol>
        </div>
        <!-- /.col-lg-12 -->
      </div>

      <!-- row -->
      <div class="row">
        <div class="col-md-12">
          <div class="white-box">
            <div class="table-responsive">
              <table class="table table-bordered table-striped">
                <tbody>
                  <tr>
                    <th>Brand</th>
                    <td><?php echo $row['brand_name']; ?></td>
                  </tr>
                  <tr>
                    <th>Name</th>
                    <td><?php echo $row['name']; ?></td>
                  </tr>
                  <?php if(!empty($row['image_url']) && file_exists($upload_save_path . $row['image_url'])): ?>
                    <tr>
                      <th>Image</th>
                      <td><strong><a href="<?php echo $upload_save_path . $row['image_url'] ?>" target="_blank">View</a></strong></td>
                    </tr>
                  <?php endif; ?>
                  <tr>
                    <th>Description</th>
                    <td><?php echo dashIfEmpty($row['description']); ?></td>
                  </tr>
                  <tr>
                    <th>Price per day</th>
                    <td>$<?php echo number_format($row['price_per_day'], 2); ?></td>
                  </tr>
                  <tr>
                    <th>Year</th>
                    <td><?php echo dashIfEmpty($row['model_year']); ?></td>
                  </tr>
                  <tr>
                    <th>Kmph</th>
                    <td><?php echo number_format($row['kmph']); ?></td>
                  </tr>
                  <tr>
                    <th>Drive Setup</th>
                    <td><?php echo dashIfEmpty($drive_setups[getArrayKey($drive_setups, "id", $row['drive_setup'])]['name']); ?></td>
                  </tr>
                  <tr>
                    <th>Fuel Type</th>
                    <td><?php echo dashIfEmpty($fuel_types[getArrayKey($fuel_types, "id", $row['fuel_type'])]['name']); ?></td>
                  </tr>
                  <tr>
                    <th>Transmission</th>
                    <td><?php echo dashIfEmpty($transmissions[getArrayKey($transmissions, "id", $row['transmission'])]['name']); ?></td>
                  </tr>
                  <tr>
                    <th>Color</th>
                    <td><?php echo dashIfEmpty($row['color']); ?></td>
                  </tr>
                  <?php if(!empty($row['image_url']) && file_exists($upload_save_path . $row['image_url'])): ?>
                  <tr>
                    <th>Image</th>
                    <td><strong><a href="<?php echo $upload_save_path . $row['image_url']; ?>" target="_blank">View</a></strong></td>
                  </tr>
                <?php endif; ?>
                  <tr>
                    <th>Has AC?</th>
                    <td><?php echo $row['has_ac'] == 1 ? '<a href="javascript:void(0);" class="bg-success" style="color: #ffffff;">&nbsp;YES&nbsp;</a>' : '<a href="javascript:void(0);" class="bg-danger" style="color: #ffffff;">&nbsp;NO&nbsp;</a>'; ?> </td>
                  </tr>
                  <tr>
                    <th>Has GPS?</th>
                    <td><?php echo $row['has_gps'] == 1 ? '<a href="javascript:void(0);" class="bg-success" style="color: #ffffff;">&nbsp;YES&nbsp;</a>' : '<a href="javascript:void(0);" class="bg-danger" style="color: #ffffff;">&nbsp;NO&nbsp;</a>'; ?> </td>
                  </tr>
                  <tr>
                    <th>Has Anti-Lock Brakes?</th>
                    <td><?php echo $row['has_anti_lock_brakes'] == 1 ? '<a href="javascript:void(0);" class="bg-success" style="color: #ffffff;">&nbsp;YES&nbsp;</a>' : '<a href="javascript:void(0);" class="bg-danger" style="color: #ffffff;">&nbsp;NO&nbsp;</a>'; ?> </td>
                  </tr>
                  <tr>
                    <th>Has Remote Keyless?</th>
                    <td><?php echo $row['has_remote_keyless'] == 1 ? '<a href="javascript:void(0);" class="bg-success" style="color: #ffffff;">&nbsp;YES&nbsp;</a>' : '<a href="javascript:void(0);" class="bg-danger" style="color: #ffffff;">&nbsp;NO&nbsp;</a>'; ?> </td>
                  </tr>
                  <tr>
                    <th>Has v6 Cylinder?</th>
                    <td><?php echo $row['has_v6_cylinder'] == 1 ? '<a href="javascript:void(0);" class="bg-success" style="color: #ffffff;">&nbsp;YES&nbsp;</a>' : '<a href="javascript:void(0);" class="bg-danger" style="color: #ffffff;">&nbsp;NO&nbsp;</a>'; ?> </td>
                  </tr>
                  <tr>
                    <th>Has Rear Seat DVD?</th>
                    <td><?php echo $row['has_rear_seat_dvd'] == 1 ? '<a href="javascript:void(0);" class="bg-success" style="color: #ffffff;">&nbsp;YES&nbsp;</a>' : '<a href="javascript:void(0);" class="bg-danger" style="color: #ffffff;">&nbsp;NO&nbsp;</a>'; ?> </td>
                  </tr>

                <tr>
                  <th>Date Created</th>
                  <td><?php echo dateEmpty($row['date_created']) ? "-" : date('M d, Y h:i A', strtotime($row['date_created'])); ?></td>
                </tr>
              </tbody>
            </table>
          </div>
          <p class="text-center">
            <button type="button" class="btn btn-info waves-effect waves-light center" onclick="history.go(-1); return false;">&larr; Back</button>
          </p>
        </div>
      </div>
    </div>
    <!-- /.row -->
  </div>
  <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->

<?php $this->load->view($this->config->item('template_dir_admin') . 'footer', array('error', $error, 'error_code', $error_code)); echo "\n";  // load footer view ?>
<script src="assets/js/jasny-bootstrap.js"></script>

</body>
</html>
