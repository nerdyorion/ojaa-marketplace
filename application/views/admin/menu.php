  <div class="navbar-default sidebar nicescroll" role="navigation">
    <div class="sidebar-nav navbar-collapse ">
      <ul class="nav" id="side-menu">
        <li class="nav-small-cap">Menu</li>
        <li class=""> <a href="<?php echo base_url() . "admin123/"; ?>" class="waves-effect"><i class="fa fa-dashboard"></i> Dashboard</a> </li>
        <li> <a href="admin123/templates" class="waves-effect"><i class="ti-tag fa fa-tag" aria-hidden="true"></i> Templates</a> </li>
        
        <?php if(($_SESSION['user_role_id'] == 1) && (strtoupper($_SESSION['user_role_name']) == "SUPER ADMINISTRATOR")): ?>
          <li> <a href="admin123/users" class="waves-effect"><i class="ti-user fa fa-user"></i> Users (Admin)</a> </li>
        <?php endif; ?>

        <li> <a href="admin123/logout" class="waves-effect"><i class="ti-power-off fa fa-power-off" aria-hidden="true"></i> Logout</a> </li>
      </ul>
    </div>
    <!-- /.sidebar-collapse -->
  </div>