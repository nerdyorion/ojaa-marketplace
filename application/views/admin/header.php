<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <base href="<?php echo base_url();?>">
  <meta name="description" content="">
  <meta name="author" content="">
  <!-- <link rel="icon" type="image/png" sizes="16x16" href="assets/admin/images/favicon.ico"> -->
  <link href="assets/img/favicon/favicon-32x32.png" rel="shortcut icon" type="image/png" sizes="32x32" />
  <title><?php echo $page_title; ?> | <?php echo $this->config->item('app_name'); ?></title>
  <!-- Bootstrap Core CSS -->
  <link href="assets/admin/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
  <!-- Menu CSS -->
  <link href="assets/admin/bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">
  <!-- Menu CSS -->
  <!--<link href="assets/bower_components/morrisjs/morris.css" rel="stylesheet">-->
  <!-- Custom CSS -->
  <link href="assets/admin/css/style.css" rel="stylesheet">
  
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="assets/https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="assets/https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body>
  <!-- Preloader -->
  <div class="preloader">
    <div class="cssload-speeding-wheel"></div>
  </div>
  <div id="wrapper">
    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-static-top" style="margin-bottom: 0">
      <div class="navbar-header"> <a class="navbar-toggle hidden-sm hidden-md hidden-lg " href="javascript:void(0)" data-toggle="collapse" data-target=".navbar-collapse"><i class="ti-menu"></i></a>
        <div class="top-left-part"><a class="logo" href="<?php echo base_url() . "admin123/"; ?>"><i class="fa fa-home" style="display: inline; color:#ffffff; "></i>&nbsp;<span class="hidden-xs">
          <!-- <img src="assets/img/logo-white.png" class="img-responsive" style="display: inline; position: relative; width:60%; top: -4px; " /> -->
          <img src="assets/img/logo2.png" class="img-responsive" style="display: inline; position: relative; width:60%; top: -4px; " />
          <!-- <?php echo $this->config->item('app_name'); ?> -->
        </span></a></div>
        <ul class="nav navbar-top-links navbar-left hidden-xs">
          <li><a href="javascript:void(0);" class="open-close hidden-xs waves-effect waves-light"><i class="icon-arrow-left-circle ti-menu"></i></a></li>

          <!-- Notifications -->
          <li class="dropdown">
            <a class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" href="#">
              <i class="icon-bell"></i> 
              <?php $notifications = notifications(); ?>
              <?php $notifications_count = count($notifications); ?>
              <?php if($notifications_count != 0): ?>
                <span class="badge badge-xs badge-warning"><?php echo $notifications_count; ?></span>
              <?php endif; ?>
            </a>
            <ul class="dropdown-menu nicescroll mailbox">
              <li>
                <div class="drop-title">You have <?php echo $notifications_count == 0 ? 'no' : $notifications_count; ?> new message<?php echo $notifications_count == 1 ? '' : 's'; ?></div>
              </li>
              <li>
                <div class="message-center">
                  <?php if($notifications_count == 0): ?>
                  <?php else: ?>
                    <?php foreach($notifications as $notification): ?>
                    <a href="admin123/notifications">
                      <div class="user-img">
                        <img src="assets/admin/images/users/icon.png" alt="user" class="img-circle" />
                        <span class="profile-status"></span>
                      </div>
                      <div class="mail-contnet">
                        <span class="mail-desc"><?php echo strip_tags($notification['message']); ?> </span>
                        <span class="time"><?php echo time_elapsed_string($notification['date_created']); ?></span>
                      </div>
                    </a>
                    <?php endforeach; ?>
                  <?php endif; ?>
                </div>
              </li>
              <li> <a class="text-center" href="admin123/notifications"> <strong>See all notifications</strong> <i class="fa fa-angle-right"></i> </a></li>
            </ul>
            <!-- /.dropdown-messages -->
          </li>

          <!-- /.dropdown -->

          <li class="dropdown"> <a class="dropdown-toggle waves-effect waves-light" href="<?php echo base_url(); ?>" target="_blank"><i class="icon-home"></i> Homepage</a>

          </li>

        </ul>
        <ul class="nav navbar-top-links navbar-right pull-right">
          <li class="dropdown"> <a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="#"> <img src="assets/admin/images/users/icon.png" alt="User" width="36" class="img-circle"><b class="hidden-xs"><?php echo isset($_SESSION['user_full_name']) ? explode(" ", $_SESSION['user_full_name'])[0] : "User"?> <?php echo isset($_SESSION['user_designation_name']) && !empty($_SESSION['user_designation_name']) ? ' :: ' . $_SESSION['user_designation_name'] : ""; ?></b> </a>
            <ul class="dropdown-menu dropdown-user">
              <li><a href="admin123/profile"><i class="ti-user"></i> My Profile</a></li>
              <li role="separator" class="divider"></li>
              <li><a href="admin123/Change-Password"><i class="ti-lock"></i> Change Password</a></li>
              <li role="separator" class="divider"></li>
              <li><a href="admin123/logout"><i class="fa fa-power-off"></i> Logout</a></li>
            </ul>
            <!-- /.dropdown-user -->
          </li>
          <!-- /.dropdown -->
        </ul>
      </div>
      <!-- /.navbar-header -->
      <!-- /.navbar-top-links -->
      <!-- /.navbar-static-side -->
    </nav>
    <div id="success-msg" style="display:none" class="myadmin-alert myadmin-alert-icon myadmin-alert-click alert3 myadmin-alert-top"></div>
    <div id="error-msg" style="display:none" class="myadmin-alert myadmin-alert-icon myadmin-alert-click alert6 myadmin-alert-top"></div>