  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-12">
          <h4 class="page-title">Contents</h4>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url() . "admin123/"; ?>">Dashboard</a></li>
            <li class="active">Contents</li>
          </ol>
        </div>
        <!-- /.col-lg-12 -->
      </div>

      <!-- row -->
      <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
          <div class="white-box">
            <!--<h3>contents</h3>-->
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
              <li role="presentation" class="active"><a href="#all" aria-controls="all" role="tab" data-toggle="tab"><span class="visible-xs"><i class="ti-view-list"></i></span><span class="hidden-xs"> View All</span></a></li>
              <li role="presentation"><a href="#add" aria-controls="add" role="tab" data-toggle="tab"><span class="visible-xs"><i class="ti-plus"></i></span> <span class="hidden-xs">Add New</span></a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
              <div role="tabpanel" class="tab-pane active" id="all"> 
                <div class="col-md-12">
                  <!-- Search Filter -->
                  <div class="row">
                    <?php echo form_open('/admin123/contents', 'class="form-inline", method="get", role="form"'); ?>
                    <div class="form-group">
                      <label for="title">Search:</label>
                      <input type="text" class="form-control" name="title" maxlength="2000" id="title" value="<?php echo isset($_GET['title']) ? trim($_GET['title']) : ''; ?>" placeholder="" />
                      </select>
                    </div>
                    <button type="submit" class="btn btn-info waves-effect waves-light"><i class="ti-search"></i> Search</button>
                  </form>
                </div>
                  <div class="table-responsive">
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Package</th>
                          <th>Subject</th>
                          <th>Week</th>
                          <th>Content</th>
                          <th class="text-nowrap">Actions</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php if(empty($rows)): ?>
                        <tr>
                          <td colspan="6" align="center">No data returned.</td>
                        </tr>
                      <?php else: ?>
                      <?php $sn = 1; foreach ($rows as $row): ?>
                        <tr>
                          <td><?php echo $sn++; ?></td>
                          <td><?php echo $packages[getArrayKey($packages, "id", $row['package_id'])]['type'];; ?></td>
                          <td><?php echo $subjects[getArrayKey($subjects, "id", $row['subject_id'])]['value']; ?></td>
                          <td><?php echo $weeks[getArrayKey($weeks, "id", $row['week_id'])]['value'];; ?></td>
                          <td><?php echo ellipsize($row['content'], 100); ?></td>
                          <td class="text-nowrap">
                            <!-- <a href="admin123/contents/edit/<?php echo $row['id']; ?>" data-toggle="tooltip" data-original-title="Edit"> <i class="fa fa-pencil text-inverse m-r-10"></i> </a>  -->
                            <a href="admin123/contents/delete/<?php echo $row['id']; ?>" data-toggle="tooltip" data-original-title="Delete"> <span class="text-danger"><i class="fa fa-close text-danger m-r-10" onclick="if(confirm('Are you sure you want to delete this record?')) return true; else return false;"></i></span> </a> 
                            <!-- <a href="admin123/contents/view/<?php echo $row['id']; ?>" data-toggle="tooltip" data-original-title="View"> <i class="fa fa-folder-open text-inverse m-r-10"></i> </a>  -->
                          </td>
                        </tr>
                      <?php endforeach; ?>
                      <?php endif; ?>
                      </tbody>
                    </table>
                  </div>
                </div>
                <!--
                <div class="col-md-3 pull-right">
                  <p><a href="?page=2">Pagination</a>.</p>
                </div>
                -->
                <div class="clearfix"></div>
              </div>
              <div role="tabpanel" class="tab-pane" id="add">
                <div class="col-md-12">
                <?php echo form_open('admin123/contents/create', 'class="form-horizontal", onsubmit="return validate();"'); ?>
                  <div class="form-group">
                    <label for="package_id" class="col-sm-2 control-label">Package: <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                      <select class="form-control" name="package_id" id="package_id" required="required">
                        <option value="" selected="selected">-- Select --</option>
                        <?php foreach ($packages as $row): ?>
                          <option value="<?php echo $row['id']; ?>"><?php echo $row['type']; ?></option>
                        <?php endforeach; ?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="subject_id" class="col-sm-2 control-label">Subject: </label>
                    <div class="col-sm-10">
                      <select class="form-control" name="subject_id" id="subject_id">
                        <option value="0" selected="selected">-- Select --</option>
                        <?php foreach ($subjects as $row): ?>
                          <option value="<?php echo $row['id']; ?>"><?php echo $row['value']; ?></option>
                        <?php endforeach; ?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="week_id" class="col-sm-2 control-label">Week: </label>
                    <div class="col-sm-10">
                      <select class="form-control" name="week_id" id="week_id">
                        <option value="0" selected="selected">-- Select --</option>
                        <?php foreach ($weeks as $row): ?>
                          <option value="<?php echo $row['id']; ?>"><?php echo $row['value']; ?></option>
                        <?php endforeach; ?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="content" class="col-sm-2 control-label">Content:</label>
                    <div class="col-sm-10">
                      <textarea class="form-control" name="content" id="content"></textarea>
                    </div>
                  </div>
                  <div class="form-group m-b-0">
                    <div class="col-sm-offset-3 col-sm-9">
                      <button type="submit" class="btn btn-info waves-effect waves-light">Add New</button>
                    </div>
                  </div>
                </form>
                </div>
                <div class="clearfix"></div>
              </div>
            </div>
            <div class="col-md-3 pull-right pagination">
              <p><?php echo $links; ?></p>
            </div>
          </div>
        </div>
      </div> 
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </div>
  <!-- /#page-wrapper -->

<?php $this->load->view($this->config->item('template_dir_admin') . 'footer', array('error', $error, 'error_code', $error_code)); echo "\n";  // load footer view ?>
<script src="assets/js/jasny-bootstrap.js"></script>
<script type="text/javascript">
  function validate()
  {
    var password = document.getElementById("password").value;
    var confirm_password = document.getElementById("confirm_password").value;
    var gender = document.getElementById("gender").value;
    var role_id = document.getElementById("role_id").value;
    if(password != confirm_password ){
      alert('Passwords do not match.');
      document.getElementById("confirm_password").focus();
      return false;
    }
    else if(gender == 0 ){
      alert('Please specify gender.');
      return false;
    }
    else if(role_id == 0 ){
      alert('Please specify role.');
      return false;
    }
    else {
      return true;
    }
  }
</script>

</body>
</html>