  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-12">
          <h4 class="page-title">Cars</h4>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url() . "admin123/"; ?>">Dashboard</a></li>
            <li class="active">Cars</li>
          </ol>
        </div>
        <!-- /.col-lg-12 -->
      </div>

      <!-- row -->
      <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
          <div class="white-box">
            <!--<h3>Car</h3>-->
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
              <li role="presentation" class="active"><a href="#all" aria-controls="all" role="tab" data-toggle="tab"><span class="visible-xs"><i class="ti-view-list"></i></span><span class="hidden-xs"> View All</span></a></li>
              <li role="presentation"><a href="#add" aria-controls="add" role="tab" data-toggle="tab"><span class="visible-xs"><i class="ti-plus"></i></span> <span class="hidden-xs">Add New</span></a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
              <div role="tabpanel" class="tab-pane active" id="all"> 
                <div class="col-md-12">
                  <!-- Search Filter -->
                  <div class="row">
                    <?php echo form_open('/admin123/cars', 'class="form-inline", method="get", role="form"'); ?>
                    <div class="form-group">
                      <label for="title">Search:</label>
                      <input type="text" class="form-control" name="title" maxlength="2000" id="title" value="<?php echo isset($_GET['title']) ? trim($_GET['title']) : ''; ?>" placeholder="" />
                      </select>
                    </div>
                    <button type="submit" class="btn btn-info waves-effect waves-light"><i class="ti-search"></i> Search</button>
                  </form>
                </div>
                  <div class="table-responsive">
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Image</th>
                          <!-- <th>Brand</th> -->
                          <th>Name</th>
                          <!-- <th>State</th> -->
                          <th>Price</th>
                          <th>Year</th>
                          <th>Color</th>
                          <th>No. of Bookings</th>
                          <th class="text-nowrap">Actions</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php if(empty($rows)): ?>
                        <tr>
                          <td colspan="8" align="center">No data returned.</td>
                        </tr>
                      <?php else: ?>
                      <?php foreach ($rows as $row): ?>
                        <tr>
                          <td><?php echo $sn++; ?></td>
                          <td>

                            <?php if(!empty($row['image_url']) && file_exists($upload_save_path . $row['image_url'])): ?>
                              <div class="media">
                                <div class="media-left">
                                  <a href="<?php echo $upload_save_path . $row['image_url']; ?>" target="_blank">
                                    <img class="media-object" src="<?php echo image_thumb($upload_save_path . $row['image_url']); ?>" alt="<?php echo $row['name']; ?>" style="max-width: 50px">
                                  </a>
                                </div>
                              </div>
                            <?php endif; ?>
                          </td>
                          <!-- <td><?php echo dashIfEmpty($row['brand_name']); ?></td> -->
                          <td>
                            <strong><?php echo dashIfEmpty($row['name']); ?></strong>
                            <br />
                            <small><?php echo dashIfEmpty($row['brand_name']); ?></small>
                              <?php echo $row['is_featured'] == 1 ? '<br /><small><a href="javascript:void(0);" class="bg-success" style="color: #ffffff;">&nbsp;FEATURED&nbsp;</a></small>' : ''; ?>
                            </td>
                          <td>$<?php echo number_format($row['price_per_day'], 2); ?></td>
                          <td><?php echo dashIfEmpty($row['model_year']); ?></td>
                          <td><?php echo dashIfEmpty($row['color']); ?></td>
                          <td><?php echo number_format($row['bookings']); ?></td>
                          <td class="text-nowrap">
                            <?php if($row['is_featured'] == 0): ?>
                              <a href="admin123/cars/feature/<?php echo $row['id']; ?>" data-toggle="tooltip" data-original-title="Set as Featured"> <i class="fa fa-trophy text-inverse m-r-10"></i> </a> 
                            <?php else: ?>
                              <a href="admin123/cars/unfeature/<?php echo $row['id']; ?>" data-toggle="tooltip" data-original-title="Remove Featured"> <i class="fa fa-trophy fa-rotate-90 text-inverse m-r-10"></i> </a> 
                            <?php endif; ?>

                            <a href="admin123/cars/edit/<?php echo $row['id']; ?>" data-toggle="tooltip" data-original-title="Edit"> <i class="fa fa-pencil text-inverse m-r-10"></i> </a>

                            <a href="admin123/cars/view/<?php echo $row['id']; ?>" data-toggle="tooltip" data-original-title="View"> <i class="fa fa-folder-open text-inverse m-r-10"></i> </a> 
                            
                            <a href="admin123/cars/delete/<?php echo $row['id']; ?>" data-toggle="tooltip" data-original-title="Delete" onclick="if(confirm('Are you sure you want to proceed?')) return true; else return false;"> <span class="text-danger"><i class="fa fa-close text-danger m-r-10"></i></span> </a>
                          </td>
                        </tr>
                      <?php endforeach; ?>
                      <?php endif; ?>
                      </tbody>
                    </table>
                  </div>
                </div>
                <!--
                <div class="col-md-3 pull-right">
                  <p><a href="?page=2">Pagination</a>.</p>
                </div>
                -->
                <div class="clearfix"></div>
              </div>
              <div role="tabpanel" class="tab-pane" id="add">
                <div class="col-md-12">
                <?php echo form_open_multipart('admin123/cars/create', 'class="form-horizontal", onsubmit="return validate();"'); ?>
                  <div class="form-group">
                    <label for="brand_id" class="col-sm-2 control-label">Brand: <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                      <select class="form-control" name="brand_id" id="brand_id" required="required">
                        <option value="" selected="selected">-- Select --</option>
                        <?php foreach ($brands as $row): ?>
                          <option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
                        <?php endforeach; ?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">Name: <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" name="name" maxlength="2000" id="name" value="" placeholder="" required="required">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="image_url" class="col-sm-2 control-label">Image (650x650 px): <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                      <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                        <div class="form-control" data-trigger="fileinput">
                          <i class="glyphicon glyphicon-file fileinput-exists"></i> 
                          <span class="fileinput-filename"></span>
                        </div>
                        <span class="input-group-addon btn btn-default btn-file">
                        <span class="fileinput-new">Select file</span>
                        <span class="fileinput-exists">Change</span>
                        <input type="file" name="image_url" id="image_url" required="required"></span>
                        <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="description" class="col-sm-2 control-label">Description:</label>
                    <div class="col-sm-10">
                      <textarea class="form-control" name="description" id="description" maxlength="8000"></textarea>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="price_per_day" class="col-sm-2 control-label">Price per day ($): <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                      <input type="number" class="form-control" name="price_per_day" min="1" step="0.1" id="price_per_day" placeholder="" required="required">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="model_year" class="col-sm-2 control-label">Year: <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                      <input type="number" class="form-control" name="model_year" min="1" id="model_year" placeholder="" required="required">
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="kmph" class="col-sm-2 control-label">Kmph: <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                      <input type="number" class="form-control" name="kmph" min="1" id="kmph" placeholder="" required="required">
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="drive_setup" class="col-sm-2 control-label">Drive Setup:</label>
                    <div class="col-sm-10">
                      <select class="form-control" name="drive_setup" id="drive_setup">
                        <option value="" selected="selected">-- Select --</option>
                        <?php foreach ($drive_setups as $row): ?>
                          <option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
                        <?php endforeach; ?>
                      </select>
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="fuel_type" class="col-sm-2 control-label">Fuel Type:</label>
                    <div class="col-sm-10">
                      <select class="form-control" name="fuel_type" id="fuel_type">
                        <option value="" selected="selected">-- Select --</option>
                        <?php foreach ($fuel_types as $row): ?>
                          <option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
                        <?php endforeach; ?>
                      </select>
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="transmission" class="col-sm-2 control-label">Transmission: <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                      <select class="form-control" name="transmission" id="transmission" required="required">
                        <option value="" selected="selected">-- Select --</option>
                        <?php foreach ($transmissions as $row): ?>
                          <option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
                        <?php endforeach; ?>
                      </select>
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="color" class="col-sm-2 control-label">Color:</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" name="color" maxlength="2000" id="color" value="" placeholder="">
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="checkbox" class="control-label"> </label>
                    <div class="col-sm-offset-2 col-sm-10">
                      <div class="form-inline row">
                        <div class="form-group col-sm-3">
                          <div class="checkbox checkbox-primary">
                            <input name="has_ac" id="has_ac" type="checkbox" value="1">
                            <label for="has_ac"> Has AC? </label>
                          </div>
                        </div>

                        <div class="form-group col-sm-3">
                          <div class="checkbox checkbox-primary">
                            <input name="has_gps" id="has_gps" type="checkbox" value="1">
                            <label for="has_gps"> Has GPS? </label>
                          </div>
                        </div>

                        <div class="form-group col-sm-3">
                          <div class="checkbox checkbox-primary">
                            <input name="has_anti_lock_brakes" id="has_anti_lock_brakes" type="checkbox" value="1">
                            <label for="has_anti_lock_brakes"> Has Anti-Lock Brakes? </label>
                          </div>
                        </div>

                        <div class="form-group col-sm-3">
                          <div class="checkbox checkbox-primary">
                            <input name="has_remote_keyless" id="has_remote_keyless" type="checkbox" value="1">
                            <label for="has_remote_keyless"> Has Remote Keyless? </label>
                          </div>
                        </div>

                      </div>
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="checkbox" class="control-label"> </label>
                    <div class="col-sm-offset-2 col-sm-10">
                      <div class="form-inline row">
                        <div class="form-group col-sm-3">
                          <div class="checkbox checkbox-primary">
                            <input name="has_v6_cylinder" id="has_v6_cylinder" type="checkbox" value="1">
                            <label for="has_v6_cylinder"> Has v6 Cylinder? </label>
                          </div>
                        </div>

                        <div class="form-group col-sm-3">
                          <div class="checkbox checkbox-primary">
                            <input name="has_rear_seat_dvd" id="has_rear_seat_dvd" type="checkbox" value="1">
                            <label for="has_rear_seat_dvd"> Has Rear Seat DVD? </label>
                          </div>
                        </div>

                      </div>
                    </div>
                  </div>


                  <div class="form-group m-b-0">
                    <div class="col-sm-offset-3 col-sm-9">
                      <button type="submit" class="btn btn-info waves-effect waves-light">Add New</button>
                    </div>
                  </div>
                </form>
                </div>
                <div class="clearfix"></div>
              </div>
            </div>
            <div class="col-md-3 pull-right pagination">
              <p><?php echo $links; ?></p>
            </div>
          </div>
        </div>
      </div> 
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </div>
  <!-- /#page-wrapper -->

<?php $this->load->view($this->config->item('template_dir_admin') . 'footer', array('error', $error, 'error_code', $error_code)); echo "\n";  // load footer view ?>
<script src="assets/admin/js/jasny-bootstrap.js"></script>
<script type="text/javascript">
  function validate()
  {
    
    if(document.getElementById("image_url").files.length == 0 ){
      alert('Please upload image.');
      return false;
    }
    else {
      var fileName = $("#image_url").val();
      fileName = fileName.toLowerCase();
      
      if((fileName.lastIndexOf("jpg")===fileName.length-3) || (fileName.lastIndexOf("jpeg")===fileName.length-4) || (fileName.lastIndexOf("png")===fileName.length-3) || (fileName.lastIndexOf("gif")===fileName.length-3))
      {
        //alert("OK");
        return true;
      }
      else
      {
        alert("Please upload valid image file.");
        return false;
      }
    }
  }
</script>

</body>
</html>