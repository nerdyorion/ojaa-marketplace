<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    private $upload_save_path = './assets/img/cars/';

    public function __construct()
    {
        parent::__construct();
            $this->load->model('Car_model');
            $this->load->model('Brand_model');
        // $this->load->helper('captcha');
        // $this->load->library('encryption');
    }

    public function index()
    {
        // set default metas
        $header['page_title'] = 'Welcome';
        $header['meta_title'] = $header['page_title'];
        $header['meta_tags'] = $this->config->item('meta_tags_default');
        $header['meta_description'] = $this->config->item('meta_description_default');

        $data['upload_save_path'] = $this->upload_save_path;
        $data['brands'] = $this->Brand_model->getRowsDropDown();
        $data['brands_sidenav'] = $this->Brand_model->getRowsDropDown_ThatHasCars(30);
        $data['featured_sidenav'] = $this->Car_model->getRowsFeatured(12);

        $data['drive_setups'] = drive_setups();
        $data['fuel_types'] = fuel_types();
        $data['transmissions'] = transmissions();

        //---------------------- end meta ---------------------------------

        // $data['captcha_image'] = $this->generateCaptcha();

        // if(empty($data['captcha_image']))
        // {
        //     $data['captcha_image'] = "Captcha not generated. Please reload page.";
        //     // echo "Captcha not generated. Please reload page."; exit;
        // }

        // $data['universities'] = $this->University_model->getRowsDropDown();

        $data['error'] = $this->session->flashdata('error');
        $data['error_code'] = $this->session->flashdata('error_code');
        

        $this->load->view('header', $header);  // load header view
        // $this->load->view('menu');  // load menu view
        $this->load->view('home', $data);  // load content view
    }

    public function csrf()
    {
        echo $this->security->get_csrf_hash();
    }

    private function generateCaptcha($ajax = FALSE)
    {
        $vals = array(
            // 'word'          => 'Random word',
            'img_path'      => './assets/images/captcha/',
            'img_url'       => base_url() . 'assets/images/captcha/',
            'font_path'     => base_url() . 'assets/fonts/raleway/Raleway-ExtraBoldItalic.ttf',
            'img_width'     => '150',
            'img_height'    => 30,
            'expiration'    => 7200,
            'word_length'   => 8,
            'font_size'     => 16,
            'img_id'        => 'captcha_image',
            // 'pool'          => '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',
            // 'pool'          => '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ',
            'pool'          => '123456789ABCDEFGHIJKLMNPQRSTUVWXYZ',

            // White background and border, black text and red grid
            'colors'        => array(
                // 'background' => array(255, 255, 255),
                'background' => array(0, 0, 0),
                'border' => array(255, 255, 255),
                // 'text' => array(0, 0, 0),
                'text' => array(255, 255, 255),
                // 'grid' => array(220, 220, 220)
                'grid' => array(20, 22, 220)
            )
        );

        $cap = create_captcha($vals);

        $data = array(
            'captcha_time'  => $cap['time'],
            'ip_address'    => $this->input->ip_address(),
            'word'          => $cap['word']
        );

        if(!empty($data['word']))
        {
            $query = $this->db->insert_string('captcha', $data);
            $this->db->query($query);
        }

        if($ajax == FALSE)
        {
            return $cap['image'];
        }
        else
        {
            echo $cap['image'];
        }
    }

    private function deleteExpiredCaptcha()
    {
        // First, delete old captchas
        $expiration = time() - 7200; // Two hour limit
        $this->db->where('captcha_time < ', $expiration)
        ->delete('captcha');
    }

    private function validateCaptcha()
    {
        // Then see if a captcha exists:
        $expiration = time() - 7200; // Two hour limit
        $sql = 'SELECT COUNT(*) AS count FROM captcha WHERE word = ? AND ip_address = ? AND captcha_time > ?';
        $binds = array($_POST['captcha'], $this->input->ip_address(), $expiration);
        $query = $this->db->query($sql, $binds);
        $row = $query->row();

        if ($row->count == 0)
        {
            // echo 'You must submit the word that appears in the image.';
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }
}