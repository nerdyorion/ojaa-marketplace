<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    private $ref_pages = array("profile", "cars", "rent");

    public function __construct()
    {
        parent::__construct();
        if(!is_logged_in())  // check if logged in
        {
            //redirect to login
        }
        else
        {
            // student direct redirects only, Admin redirects not available for now
            $this->checkDirectRedirect();

            go_to_dashboard();
        }
        $this->load->model('User_model');
        $this->load->helper('captcha');
        $this->load->library('encrypt');
        // $this->load->helper('url_helper');
    }

    public function index()
    {
        // set default metas
        $header['page_title'] = 'Login';
        $header['meta_title'] = $header['page_title'];
        $header['meta_tags'] = $this->config->item('meta_tags_default');
        $header['meta_description'] = $this->config->item('meta_description_default');

        //---------------------- end meta ---------------------------------

        $data['return'] = '';

        $pages = $this->ref_pages;

        // if(isset($_GET['return']) && !empty($_GET['return']) && in_array($_GET['return'], $pages))
        // {
        //     $data['return'] = trim($_GET['return']);
        // }

        if(isset($_GET['return']) && !empty($_GET['return']) && in_array(explode('/', $_GET['return'])[0], $pages))
        {
            $data['return'] = trim($_GET['return']);
        }

        $data['error'] = $this->session->flashdata('error');
        $data['error_code'] = $this->session->flashdata('error_code');

        $data['page_title'] = 'Login';  // set page title
        $this->load->view('header', $header);  // load header view
        $this->load->view('login', $data);  // load content view

        $this->load->library('form_validation');
        $this->form_validation->set_rules('email', 'Email', 'required|max_length[255]|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'required|max_length[8000]');
    }

    public function secure()
    {
        $data['return'] = '';
        $query = '';

        $pages = $this->ref_pages;

        if(!is_null($this->input->post('return')) && !empty($this->input->post('return')) && in_array(trim(explode('/', $this->input->post('return'))[0]), $pages))
        {
            $data['return'] = trim($this->input->post('return'));
            $query = '?ref=' . $data['return'];
            // var_dump($data['return']); die;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('email', 'Email', 'required|max_length[255]|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'required|max_length[8000]');

        $this->session->set_flashdata('email', $this->input->post('email'));

        if ($this->form_validation->run() === FALSE)
        {
            $errors = str_replace("<p>","", validation_errors());
            $errors = str_replace("</p>","", trim($errors));
            $this->session->set_flashdata('error', $errors);
            $this->session->set_flashdata('error_code', empty($errors) ? 0 : 1);

            redirect('/login' . $query);
        }
        else
        {
            $data['error'] = $this->session->flashdata('error');
            $data['error_code'] = $this->session->flashdata('error_code');
            $data['page_title'] = 'Login';  // set page title
            $data['login'] = $this->User_model->login();  // login
            if(!$data['login'])
            {
                $errors = "Invalid Login!";
                $this->session->set_flashdata('error', $errors);
                $this->session->set_flashdata('error_code', empty($errors) ? 0 : 1);

                redirect('/login' . $query);
            }
            else
            {
                // check if suspended and display message
                // var_dump($data['login']['is_suspended']); die;
                if((int) $data['login']['is_suspended'] == 1)
                {
                    $this->logoutSilent();

                    $errors = "Your account has been suspended. Kindly contact us to resolve.";
                    $this->session->set_flashdata('error', $errors);
                    $this->session->set_flashdata('error_code', empty($errors) ? 0 : 1);

                    redirect('/login' . $query);
                }

                $this->session->set_flashdata('email', '');

                $this->User_model->updateLastLogin($data['login']['id']);  //  update last login

                if(!empty($data['return']))
                {
                    if((int) $this->session->userdata('user_role_id') == 3)
                    {
                        redirect($data['return']);
                    }
                }
                else
                {
                    go_to_dashboard();
                }
            }
        }
    }

    private function checkDirectRedirect()
    {
        if(isset($_GET['ref']) && !empty($_GET['ref']) && in_array(explode('/', $_GET['ref'])[0], $this->ref_pages))
        {
            $go = trim($_GET['ref']);

            if((int) $this->session->userdata('user_role_id') == 3)
            {
                redirect($go);
            }
        }
    }

    private function logoutSilent()
    {
        $this->session->unset_userdata('user_id');
        $this->session->unset_userdata('user_full_name');
        $this->session->unset_userdata('user_email');
        $this->session->unset_userdata('user_role_id');
        $this->session->unset_userdata('user_role_name');
        $this->session->unset_userdata('user_last_login');
        
        $this->session->unset_userdata('user_id_former_logged_in');
        $this->session->unset_userdata('user_full_name_former_logged_in');
        $this->session->unset_userdata('user_email_former_logged_in');
        $this->session->unset_userdata('user_role_id_former_logged_in');
        $this->session->unset_userdata('user_role_name_former_logged_in');
        $this->session->unset_userdata('user_last_login_former_logged_in');
    }

    private function template_verify($to_name, $verify_url)
    {
        // $url = $this->config->base_url() . 'login?ref=profile';
        $url = $verify_url;

        return "<html xmlns=\"http://www.w3.org/1999/xhtml\" xmlns=\"http://www.w3.org/1999/xhtml\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; padding: 0;\">

        <body style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; -webkit-font-smoothing: antialiased; -webkit-text-size-adjust: none; width: 100% !important; height: 100%; line-height: 1.6; background-color: #f6f6f6; margin: 0; padding: 0;\" bgcolor=\"#f6f6f6\">

        <table class=\"body-wrap\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; background-color: #f6f6f6; margin: 0; padding: 0;\" bgcolor=\"#f6f6f6\"><tr style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; padding: 0;\"><td style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0;\" valign=\"top\"></td>
        <td class=\"container\" width=\"600\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; display: block !important; max-width: 600px !important; clear: both !important; width: 100% !important; margin: 0 auto; padding: 0;\" valign=\"top\">
        <div class=\"content\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; max-width: 600px; display: block; margin: 0 auto; padding: 10px;\">
        <table class=\"main\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; border-radius: 3px; background-color: #fff; margin: 0; padding: 0; border: 1px solid #e9e9e9;\" bgcolor=\"#fff\"><tr style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; padding: 0;\"><td class=\"alert alert-warning\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 16px; vertical-align: top; color: #fff; font-weight: 500; text-align: center; border-radius: 3px 3px 0 0; background-color: #4aa3df; margin: 0; padding: 20px;\" align=\"center\" bgcolor=\"#4aa3df\" valign=\"top\">
        " . $this->config->item('app_name') . "
        </td>
        </tr><tr style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; padding: 0;\"><td class=\"content-wrap\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 20px;\" valign=\"top\">
        <table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; padding: 0;\"><tr style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; padding: 0;\"><td class=\"content-block\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;\" valign=\"top\">
        Hello $to_name,
        </td>
        </tr><tr style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; padding: 0;\"><td class=\"content-block\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;\" valign=\"top\">
        Please click the link below to verify your account.
        </td>
        </tr><tr style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; padding: 0;\"><td class=\"content-block\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;\" valign=\"top\">
        <a href=\"$url\" class=\"btn-primary\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; color: #FFF; text-decoration: none; line-height: 2; font-weight: bold; text-align: center; cursor: pointer; display: inline-block; border-radius: 5px; text-transform: capitalize; background-color: #348eda; margin: 0; padding: 0; border-color: #348eda; border-style: solid; border-width: 10px 20px;\">Verify My Account &raquo;</a>
        </td>
        </tr><tr style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; padding: 0;\"><td class=\"content-block\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;\" valign=\"top\">
        Thanks for choosing " . $this->config->item('app_name') . ".
        </td>
        </tr></table></td>
        </tr></table><div class=\"footer\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; clear: both; color: #999; margin: 0; padding: 20px;\">
        <table width=\"100%\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; padding: 0;\"><tr style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; padding: 0;\"><td class=\"aligncenter content-block\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 12px; vertical-align: top; text-align: center; margin: 0; padding: 0 0 20px;\" align=\"center\" valign=\"top\">
        <br />" . $this->config->item('info_email') . "
        <br />&copy; " . date("Y") . " " . $this->config->item('app_name') . ".</td>
        </tr></table></div></div>
        </td>
        <td style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0;\" valign=\"top\"></td>
        </tr></table></body>
        </html>";
    }
}