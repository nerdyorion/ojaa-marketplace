<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller {

    public function __construct()
    {
            parent::__construct();
        $this->load->model('User_model');
        $this->load->helper('captcha');
    }

	public function index()
	{
        redirect('login');

        // set default metas
        $header['page_title'] = 'Register';
        $header['meta_title'] = $header['page_title'];
        $header['meta_tags'] = $this->config->item('meta_tags_default');
        $header['meta_description'] = $this->config->item('meta_description_default');
        //---------------------- end meta ---------------------------------

        $data['error'] = $this->session->flashdata('error');
        $data['error_code'] = $this->session->flashdata('error_code');

        $data['captcha_image'] = $this->generateCaptcha();

        if(empty($data['captcha_image']))
        {
            $data['captcha_image'] = "Captcha not generated. Please reload page.";
            // echo "Captcha not generated. Please reload page."; exit;
        }
        
        $this->load->view('header', $header);  // load header view
        // $this->load->view('menu');  // load menu view
        $this->load->view('register', $data);  // load content view
	}

    function alpha_dash_space($str)
    {
        return ( ! preg_match("/^([-a-z_ ])+$/i", $str)) ? FALSE : TRUE;
    }

    public function secure()
    {
        redirect('login');
        
        $redirect_route = 'register';
        
        $this->load->library('form_validation');
        $this->form_validation->set_rules('first_name', 'First name', 'trim|required|max_length[255]|callback_alpha_dash_space');
        $this->form_validation->set_rules('last_name', 'Last name', 'trim|required|max_length[255]|callback_alpha_dash_space');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|max_length[255]|valid_email');

        $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[6]|max_length[8000]');
        $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'trim|required|max_length[8000]|matches[password]');
        
        // You could set the title in session like above for example
        $this->session->set_flashdata('first_name', $this->input->post('first_name'));
        $this->session->set_flashdata('last_name', $this->input->post('last_name'));
        $this->session->set_flashdata('email', $this->input->post('email'));


        // check captcha
        $this->deleteExpiredCaptcha(); // emailExists($email)
        $email = $this->input->post('email');

        if ($this->form_validation->run() === FALSE)
        {
            $errors = str_replace("<p>","", validation_errors());
            $errors = str_replace("</p>","", trim($errors));
            $this->session->set_flashdata('error', $errors);
            $this->session->set_flashdata('error_code', empty($errors) ? 0 : 1);
            redirect($redirect_route);
        }
        /*elseif($this->validateCaptcha() == FALSE)
        {
            $this->session->set_flashdata('error', 'Please enter the correct captcha.');
            $this->session->set_flashdata('error_code', 1);
            redirect($redirect_route);
        }
        elseif(!passwordStrong(trim($this->input->post('password'))))
        {
            $this->session->set_flashdata('error', 'Password should include at least one special character, one number and one capital letter.');
            $this->session->set_flashdata('error_code', 1);
            redirect($redirect_route);
        }*/
        elseif($this->User_model->emailExists($email))
        {

            $this->session->set_flashdata('first_name', '');
            $this->session->set_flashdata('last_name', '');
            $this->session->set_flashdata('email', '');

            $errors = "You are already registered. <a href='login'>Please login to proceed &raquo;</a>";
            $this->session->set_flashdata('error', $errors);
            $this->session->set_flashdata('error_code', empty($errors) ? 0 : 1);

            redirect($redirect_route);
        }
        else
        {
            // dd('good');

            if(!is_logged_in())
            {
                $this->User_model->addSignup();

                // send welcome email
                $email = trim($this->input->post('email'));

                $to = $email;
                $to_name = ucfirst(trim($this->input->post('first_name'))) . ' ' . ucfirst(trim($this->input->post('last_name')));
                $subject = "Registration Successful!";
                $message = $this->template_reg_successful($to_name);

                $to = filter_var(strtolower($to), FILTER_SANITIZE_EMAIL);
                $to_name = filter_var(ucwords($to_name), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);

                // send welcome mail to student
                sendmail($to, $to_name, $subject, $message);

                // add to list
                // addToList($to, $to_name);

                // add notification to admin dashboard
                $notification_user_id = 0; // for super admin and admin

                $notification_message = 'New student sign-up: <a href="' . base_url() . 'admin123/students">' . $to_name . '</a>';
                add_notification($notification_user_id, $notification_message);
                // --------------------------------------------------


                $errors = "Registration successful. <a href='login'>Please login to proceed&raquo;</a>";
                $this->session->set_flashdata('error', $errors);
                $this->session->set_flashdata('error_code', 0);

                $this->session->set_flashdata('first_name', '');
                $this->session->set_flashdata('last_name', '');
                $this->session->set_flashdata('email', '');

                redirect($redirect_route);
            }
        }
    }

    public function getLgasByStateID($state_id)
    {
        $state_id = (int) $state_id;

        $lgas = $state_id == '' || is_null($state_id) ? array() : $this->Lga_model->getRowsByStateIDDropDown($state_id);

        $output = '<option value="" selected="selected">-- Select --</option>';

        foreach ($lgas as $lga) {
            $output .= '<option value="' . $lga['id'] . '" >' . $lga['name'] . '</option>';
        }

        echo $output;
    }

    public function testemail() {
        $to_name = 'Baba Tee';
        echo $this->template_reg_successful($to_name);
        die;
    }

    private function template_reg_successful($to_name)
    {
        $url = $this->config->base_url() . 'login';

        return "<html xmlns=\"http://www.w3.org/1999/xhtml\" xmlns=\"http://www.w3.org/1999/xhtml\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; padding: 0;\">

                    <body style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; -webkit-font-smoothing: antialiased; -webkit-text-size-adjust: none; width: 100% !important; height: 100%; line-height: 1.6; background-color: #f6f6f6; margin: 0; padding: 0;\" bgcolor=\"#f6f6f6\">

                    <table class=\"body-wrap\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; background-color: #f6f6f6; margin: 0; padding: 0;\" bgcolor=\"#f6f6f6\"><tr style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; padding: 0;\"><td style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0;\" valign=\"top\"></td>
                    <td class=\"container\" width=\"600\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; display: block !important; max-width: 600px !important; clear: both !important; width: 100% !important; margin: 0 auto; padding: 0;\" valign=\"top\">
                    <div class=\"content\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; max-width: 600px; display: block; margin: 0 auto; padding: 10px;\">
                    <table class=\"main\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; border-radius: 3px; background-color: #fff; margin: 0; padding: 0; border: 1px solid #e9e9e9;\" bgcolor=\"#fff\"><tr style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; padding: 0;\"><td class=\"alert alert-warning\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 16px; vertical-align: top; color: #fff; font-weight: 500; text-align: center; border-radius: 3px 3px 0 0; background-color: " . $this->config->item('theme_color') . "; margin: 0; padding: 20px;\" align=\"center\" bgcolor=\"" . $this->config->item('theme_color') . "\" valign=\"top\">
                    " . $this->config->item('app_name') . "
                    </td>
                    </tr><tr style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; padding: 0;\"><td class=\"content-wrap\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 20px;\" valign=\"top\">
                    <table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; padding: 0;\"><tr style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; padding: 0;\"><td class=\"content-block\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;\" valign=\"top\">
                    Hello $to_name,
                                    </td>
                                </tr><tr style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; padding: 0;\"><td class=\"content-block\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;\" valign=\"top\">
                                        Welcome to " . $this->config->item('app_name') . ", your registration was completed successfully.
                                    </td>
                                </tr><tr style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; padding: 0;\"><td class=\"content-block\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;\" valign=\"top\">
                                        Please complete your profile and configuration after logging in to get started.</b>
                    </td>
                    </tr><tr style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; padding: 0;\"><td class=\"content-block\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;\" valign=\"top\">
                    <a href=\"$url\" class=\"btn-primary\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; color: #FFF; text-decoration: none; line-height: 2; font-weight: bold; text-align: center; cursor: pointer; display: inline-block; border-radius: 5px; text-transform: capitalize; background-color: " . $this->config->item('theme_color_deep') . "; margin: 0; padding: 0; border-color: " . $this->config->item('theme_color_deep') . "; border-style: solid; border-width: 10px 20px;\">Login &raquo;</a>
                    </td>
                                </tr><tr style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; padding: 0;\"><td class=\"content-block\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;\" valign=\"top\">
                                        Thanks for choosing " . $this->config->item('app_name') . ".
                                    </td>
                                </tr></table></td>
                    </tr></table><div class=\"footer\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; clear: both; color: #999; margin: 0; padding: 20px;\">
                    <table width=\"100%\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; padding: 0;\"><tr style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; padding: 0;\"><td class=\"aligncenter content-block\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 12px; vertical-align: top; text-align: center; margin: 0; padding: 0 0 20px;\" align=\"center\" valign=\"top\">
                    <br />" . $this->config->item('info_email') . "
                    <br />&copy; " . date("Y") . " " . $this->config->item('app_name') . ".</td>
                    </tr></table></div></div>
                    </td>
                    <td style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0;\" valign=\"top\"></td>
                    </tr></table></body>
                    </html>";
    }


    public function getCaptcha()
    {
        return $this->generateCaptcha(TRUE); // ajax request
    }

    private function generateCaptcha($ajax = FALSE)
    {
        $vals = array(
            // 'word'          => 'Random word',
            'img_path'      => './assets/images/captcha/',
            'img_url'       => base_url() . 'assets/images/captcha/',
            'font_path'     => base_url() . 'assets/fonts/raleway/Raleway-ExtraBoldItalic.ttf',
            'img_width'     => '150',
            'img_height'    => 30,
            'expiration'    => 7200,
            'word_length'   => 8,
            'font_size'     => 16,
            'img_id'        => 'captcha_image',
            // 'pool'          => '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',
            'pool'          => '123456789ABCDEFGHIJKLMNPQRSTUVWXYZ',

            // White background and border, black text and red grid
            'colors'        => array(
                // 'background' => array(255, 255, 255),
                'background' => array(0, 0, 0),
                'border' => array(255, 255, 255),
                // 'text' => array(0, 0, 0),
                'text' => array(255, 255, 255),
                // 'grid' => array(220, 220, 220)
                'grid' => array(20, 22, 220)
            )
        );

        $cap = create_captcha($vals);

        $data = array(
            'captcha_time'  => $cap['time'],
            'ip_address'    => $this->input->ip_address(),
            'word'          => $cap['word']
        );

        if(!empty($data['word']))
        {
            $query = $this->db->insert_string('captcha', $data);
            $this->db->query($query);
        }

        if($ajax == FALSE)
        {
            return $cap['image'];
        }
        else
        {
            echo $cap['image'];
        }
    }

    private function deleteExpiredCaptcha()
    {
        // First, delete old captchas
        $expiration = time() - 7200; // Two hour limit
        $this->db->where('captcha_time < ', $expiration)
        ->delete('captcha');
    }

    private function validateCaptcha()
    {
        // Then see if a captcha exists:
        $expiration = time() - 7200; // Two hour limit
        $sql = 'SELECT COUNT(*) AS count FROM captcha WHERE word = ? AND ip_address = ? AND captcha_time > ?';
        $binds = array($_POST['captcha'], $this->input->ip_address(), $expiration);
        $query = $this->db->query($sql, $binds);
        $row = $query->row();

        if ($row->count == 0)
        {
            // echo 'You must submit the word that appears in the image.';
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }
}
