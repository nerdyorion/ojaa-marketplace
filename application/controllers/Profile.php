<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller {

    private $upload_save_path = '/var/www/html/martinscar-docs/';
    // private $upload_save_path = 'C:/martinscar-docs/';
    // private $upload_save_path = '/home/nubekani/public_html/neegles.com/lab/martinscar-docs/';

    public function __construct()
    {
        parent::__construct();
        if(!is_logged_in())  // check if logged in
        {
            //redirect to login
            redirect('/login');
        }

        $this->load->model('User_model');
        $this->load->model('Country_model');
        $this->load->library('encryption');
        $this->load->library("pagination");
    }

    function alpha_dash_space($str)
    {
        return ( ! preg_match("/^([-a-z_ ])+$/i", $str)) ? FALSE : TRUE;
    } 
    
    public function index()
    {
        // set default metas
        $header['page_title'] = 'My Profile';
        $header['meta_title'] = $header['page_title'];
        $header['meta_tags'] = $this->config->item('meta_tags_default');
        $header['meta_description'] = $this->config->item('meta_description_default');
        //---------------------- end meta ---------------------------------

        $this->load->library('form_validation');
        $this->form_validation->set_rules('first_name', 'First Name', 'required|max_length[255]|callback_alpha_dash_space');
        $this->form_validation->set_rules('last_name', 'Last Name', 'required|max_length[255]|callback_alpha_dash_space');
        $this->form_validation->set_rules('phone', 'Phone', 'numeric|max_length[255]');
        $this->form_validation->set_rules('gender', 'Gender', 'required|max_length[255]');
        $this->form_validation->set_rules('student_no', 'Student ID', 'required|max_length[255]');

        // You could set the title in session like above for example
        // $this->session->set_flashdata('profession', $this->input->post('profession'));
        // $this->session->set_flashdata('work_sector', $this->input->post('work_sector'));
        

        if ($this->form_validation->run() === FALSE)
        {
            $errors = str_replace("<p>","", validation_errors());
            $errors = str_replace("</p>","", trim($errors));

            if(empty($this->session->flashdata('error'))) // not currently updated
            {
                $this->session->set_flashdata('error', $errors);
                $this->session->set_flashdata('error_code', empty($errors) ? 0 : 1);
            }

            $data['error'] = $this->session->flashdata('error');
            $data['error_code'] = $this->session->flashdata('error_code');

            $data['countries'] = $this->Country_model->getRowsDropDown();
            $data['upload_save_path'] = $this->upload_save_path;

            $data['row'] = $this->User_model->getRows(0, 0, $this->session->userdata('user_id'));  // get user profile

            // dd($data['row']['id_card_url']);

            if(empty($data['row']))
                redirect('/logout');

            
            $this->load->view('header', $header);  // load header view
            // $this->load->view('menu');  // load menu view
            $this->load->view('profile', $data);  // load content view
        }
        else
        {
            $data['row'] = $this->User_model->getRows(0, 0, $this->session->userdata('user_id'));  // get user profile
            if(empty($data['row']))
                redirect('/logout');

            $_POST['id_card_url'] = $data['row']['id_card_url'];
            $_POST['drivers_license_url'] = $data['row']['drivers_license_url'];
            $_POST['proof_of_residence_url'] = $data['row']['proof_of_residence_url'];
            $_POST['international_passport_url'] = $data['row']['international_passport_url'];


            // upload files
            if(isset($_FILES["id_card_url"]['name']) && !empty($_FILES["id_card_url"]['name'])) {
                if(!empty($data['row']['id_card_url']) && file_exists($this->upload_save_path . $data['row']['id_card_url'])) {
                    unlink($this->upload_save_path . $data['row']['id_card_url']); // delete former file
                }
                $_POST['id_card_url'] = $this->uploadFile('id_card_url');
            }

            if(isset($_FILES["drivers_license_url"]['name']) && !empty($_FILES["drivers_license_url"]['name'])) {
                if(!empty($data['row']['drivers_license_url']) && file_exists($this->upload_save_path . $data['row']['drivers_license_url'])) {
                    unlink($this->upload_save_path . $data['row']['drivers_license_url']); // delete former file
                }
                $_POST['drivers_license_url'] = $this->uploadFile('drivers_license_url');
            }

            if(isset($_FILES["proof_of_residence_url"]['name']) && !empty($_FILES["proof_of_residence_url"]['name'])) {
                if(!empty($data['row']['proof_of_residence_url']) && file_exists($this->upload_save_path . $data['row']['proof_of_residence_url'])) {
                    unlink($this->upload_save_path . $data['row']['proof_of_residence_url']); // delete former file
                }
                $_POST['proof_of_residence_url'] = $this->uploadFile('proof_of_residence_url');
            }

            if(isset($_FILES["international_passport_url"]['name']) && !empty($_FILES["international_passport_url"]['name'])) {
                if(!empty($data['row']['international_passport_url']) && file_exists($this->upload_save_path . $data['row']['international_passport_url'])) {
                    unlink($this->upload_save_path . $data['row']['international_passport_url']); // delete former file
                }
                $_POST['international_passport_url'] = $this->uploadFile('international_passport_url');
            }

            $this->User_model->updateProfileFrontend($this->session->userdata('user_id'));  // update profile
            //------------------------------------------------------------------------------------

            $this->session->set_flashdata('error_code', 0);
            $this->session->set_flashdata('error', "Profile updated successfully!");
            
            redirect('/profile');
            // redirect('/Profile-Page');
        }
    }


    private function uploadFile($file_field_name)
    {
        // do file url upload now
        $config['upload_path']          = $this->upload_save_path;
        // $config['allowed_types']        = 'csv|txt|zip|rar|gz|doc|docx|xls|xlsx|pdf|pub|epub|ppt|pptx|iso|jpg|jpeg|png|gif';
        $config['allowed_types']        = 'jpg|jpeg|png|gif';
        // $config['allowed_types']        = array('jpg','jpeg','png','gif');
        $config['max_size']             = 1024; // 1MB
        $new_name = "";
        $user_id = (int) $this->session->userdata("user_id");

        $file_url = '';
        $concat_file_category = substr($file_field_name, 0, -4);
        // if($file_field_name == 'id_card_url') {
        //     $concat_file_category = 'id_card';
        // }

        if(isset($_FILES[$file_field_name]['name']) && !empty($_FILES[$file_field_name]['name']))
        {
            $file_name = $_FILES[$file_field_name]['name'];
            $tmp = explode(".", $file_name);
            $file_ext = end($tmp);

            array_pop($tmp);
            $new_name = implode('', $tmp);
            $new_name = preg_replace("/[^a-z0-9\_\-\.]/i", '_', $new_name);
            $fullname = preg_replace("/[^a-z0-9\_\-\.]/i", '_', $this->session->userdata('user_full_name'));

            // $config['file_name'] = $new_name . '_' . $concat_file_category . '_' . date('YmdHis') . "_$user_id" . '.' . $file_ext;
            $config['file_name'] = "$fullname" . "_$user_id" . "_$concat_file_category" . '_' . date('YmdHis') . '.' . $file_ext;
            // $_POST[$file_field_name] = $config['file_name'];

            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            if( !$this->upload->do_upload($file_field_name))
            {
                $errors = str_replace("<p>","[$concat_file_category] ", $this->upload->display_errors());
                $errors = str_replace("</p>","", $errors);
                $this->session->set_flashdata('error', $errors);
                $this->session->set_flashdata('error_code', 1);
                redirect("/profile");
            }
        }


        // dd($config['file_name']);
        return $config['file_name'];
    }

    public function id_card() {
        $id = (int) $this->session->userdata("user_id");

        $data['row'] = $this->User_model->getRows(0, 0, $id);  // get user profile
        if(empty($data['row']))
            redirect('/profile');
            
        $url = $data['row']['id_card_url'];

        $this->download_file($url);
    }

    public function drivers_license() {
        $id = (int) $this->session->userdata("user_id");

        $data['row'] = $this->User_model->getRows(0, 0, $id);  // get user profile
        if(empty($data['row']))
            redirect('/profile');
            
        $url = $data['row']['drivers_license_url'];

        $this->download_file($url);
    }

    public function proof_of_residence() {
        $id = (int) $this->session->userdata("user_id");

        $data['row'] = $this->User_model->getRows(0, 0, $id);  // get user profile
        if(empty($data['row']))
            redirect('/profile');
            
        $url = $data['row']['proof_of_residence_url'];

        $this->download_file($url);
    }

    public function international_passport() {
        $id = (int) $this->session->userdata("user_id");

        $data['row'] = $this->User_model->getRows(0, 0, $id);  // get user profile
        if(empty($data['row']))
            redirect('/profile');
            
        $url = $data['row']['international_passport_url'];

        $this->download_file($url);
    }

    private function download_file($url)
    {
        $filename = $url;
        $file = $this->upload_save_path . $url;

        if (!empty($filename) && file_exists($file) && is_readable($file)) 
        {
            // get the file size and send the http headers
            $size = filesize($file);
            header('Content-Type: application/octet-stream');
            header('Content-Length: '. filesize($file));
            header('Content-Disposition: attachment; filename=' . $filename);
            header('Content-Transfer-Encoding: binary');
            // open the file in binary read-only mode
            // display the error message if file can't be opened
            $file = @ fopen($file, 'rb');
            if ($file) 
            {
                // stream the file and exit the script when complete
                fpassthru($file);
                exit;
            } 
            else 
            {
                $this->session->set_flashdata('error_code', 1);
                $this->session->set_flashdata('error', "Unable to download filestream.");
                redirect('/profile');
            }
        } 
        else
        {
            $this->session->set_flashdata('error_code', 1);
            $this->session->set_flashdata('error', "Invalid file.");
            redirect('/profile');
        }
    }
}