<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MyCustom404Ctrl extends CI_Controller {

    public function __construct()
    {
            parent::__construct();
    }
	public function index()
	{
        $this->output->set_status_header('404');

        // set default metas
        $header['page_title'] = 'Page not found.';
        $header['meta_title'] = $header['page_title'];
        $header['meta_tags'] = $this->config->item('meta_tags_default');
        $header['meta_description'] = $this->config->item('meta_description_default');

        //---------------------- end meta ---------------------------------

        $this->load->view('header', $header);  // load header view
		$this->load->view('404');
	}
}
