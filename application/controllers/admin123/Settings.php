<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Settings extends Admin_Controller {

    public function __construct()
    {
        parent::__construct();
        if(!is_logged_in())  // check if logged in
        {
            //redirect to login
            redirect('/admin123/login');
        }
        $this->load->model('Setting_model');
    }

    public function index()
    {
        $data['error'] = $this->session->flashdata('error');
        $data['error_code'] = $this->session->flashdata('error_code');

        $header['page_title'] = 'Settings';  // set page title
        $data['row'] = $this->Setting_model->getRow();
        
        // dd($data['row']);

        $this->load->view($this->config->item('template_dir_admin') . 'header', $header);  // load header view
        $this->load->view($this->config->item('template_dir_admin') . 'menu');  // load menu view
        $this->load->view($this->config->item('template_dir_admin') . 'settings', $data);  // load content view
    }

    public function update()
    {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('interest_rate', 'Interest Rate', 'trim|required|numeric');

        if ($this->form_validation->run() === FALSE)
        {
            $errors = str_replace("<p>","", validation_errors());
            $errors = str_replace("</p>","", trim($errors));
            
            $this->session->set_flashdata('error', $errors);
            $this->session->set_flashdata('error_code', empty($errors) ? 0 : 1);

            redirect("/admin123/settings");
        }
        else
        {
            $this->Setting_model->update();

            $this->session->set_flashdata('error_code', 0);
            $this->session->set_flashdata('error', "Record updated successfully!");

            redirect("/admin123/settings");
        }
    }
}
