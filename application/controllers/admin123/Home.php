<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends Admin_Controller {

    public function __construct()
    {
        // var_dump("hello"); die;
        parent::__construct();
        // $this->load->library('session');
        if(!is_logged_in())  // check if logged in
        {
            //redirect to login
            redirect('/admin123/login');
        }
        $this->load->model('User_model');
    }

	public function index()
	{
        $header['page_title'] = 'Home';
        $data['sn'] = 1;
        $data['applications_today'] = 0; // (int) $this->Booking_model->record_count_today();
        $data['applications_this_week'] = 0; // (int) $this->Booking_model->record_count_this_week();
        $data['applications_total'] = 0; // (int) $this->Booking_model->record_count();
        $data['students_total'] = 0; // (int) $this->User_model->record_count("student");

        $data['last_login'] = $this->session->userdata("user_last_login");
        // $data["rows"] = $this->Customer_model->getRowsAllUnpaid(5, 0);
        $data["rows"] = array();
        //$data['rows'] = 'rows';
        $this->load->view($this->config->item('template_dir_admin') . 'header', $header);  // load header view
        $this->load->view($this->config->item('template_dir_admin') . 'menu');  // load menu view
        $this->load->view($this->config->item('template_dir_admin') . 'home', $data);  // load content view
	}
}