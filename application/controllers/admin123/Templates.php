<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Templates extends Admin_Controller {

    public function __construct()
    {
            parent::__construct();
            if(!is_logged_in())  // check if logged in
            {
                //redirect to login
                redirect('/admin123/login');
            }
    }

    public function index()
    {
        $data['page_title'] = 'Page Builder';
        // $this->load->view($this->config->item('template_dir_admin') . 'header', $header);
        // $this->load->view($this->config->item('template_dir_admin') . 'menu');
        $this->load->view($this->config->item('template_dir_admin') . 'builder', $data);
    }
}
